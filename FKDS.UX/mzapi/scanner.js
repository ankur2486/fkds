/**
 * Interface to the simple scanner services provided by the platform
 * @namespace scanner
 * @memberof mzero.create
 */
mzero.create.scanner = (function() {
	
	var service = mzero.create.MAPEnv.createService("License");
	
	var reYYYY_MM_DD = /(\d\d\d\d)-(\d\d)-(\d\d)/;
	
	function fromYYYY_MM_DD(s)
	{
		var result = reYYYY_MM_DD.exec(s);
		if (result == null) {
			return null;
		}
		var yyyy = parseInt(result[1]);
		var mm = parseInt(result[2]);
		var dd = parseInt(result[3]);
		return new Date(yyyy, mm - 1, dd);
	}
	
	function prepareScanLicenseResult(data) {
		if(!data) return null;
		var entries = data["LicenseEntries"];
		if(!entries) return null;

		var dict = {};
		
		entries.forEach(function(elem, index, array) {
			var key/*String*/ = elem["Key"];
			var value/*String*/ = elem["Value"];
			dict[key] = value;
		});
		var result/*LicenseScanResult*/ = { };
		result.RawEntries = dict;
		result.City = dict.city;
		result.Country = dict.country;
		result.DateOfBirth = fromYYYY_MM_DD(dict.dob);
		result.ExpirationDate = fromYYYY_MM_DD(dict.expires);
		result.FirstName = dict.first;
		result.Gender = dict.sex;
		result.IssuedDate = fromYYYY_MM_DD(dict.issued);
		result.LastName = dict.last;
		result.LicenseNumber = dict.id;
		result.MiddleName = dict.middle;
		result.PostalCode = dict.postal;
		result.StateCode = dict.st;
		result.StateName = dict.state;
		result.StreetAddress = dict.street;
		result.StreetAddress2 = dict.street2;
		return result;
	};
	
	/** @lends mzero.create.scanner */
	var exports = {
		/**
		 * Represents a Request to read a symbol
		 * @name mzero.create.scanner.ScanSymbolRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.scanner.scanSymbol
		 * @see mzero.create.scanner.ScanSymbolResult
		 */
		
		/**
		 * Data obtained on successful completion of {@link mzero.create.scanner.ScanSymbolRequest}
		 *
		 * @class mzero.create.scanner.ScanSymbolResult
		 * @property {String} QRCode - Decoded contents of the scanned symbol which, in fact, might not have been a QR symbol (barcodes can be scanned as well)
		 * @see mzero.create.scanner.ScanSymbolRequest
		 */
		 
		/**
		 * Start the scan symbol request, providing the callback handlers to be used.
		 *
		 * @param eventHandlers {Object} - Event handlers
		 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. In case of error, the <tt>err</tt> value will indicate the reason (such as <tt>Timeout</tt> when the user does not present a symbol or <tt>Cancelled</tt> if the request was cancelled).
		 * @method
		 * @name mzero.create.scanner.ScanSymbolRequest#start
		 */
		
		/**
		 * Call <tt>end()</tt> to indicate that the request should finish as soon as possible, interrupting the operation of the device. This
		 * hurries the end of the request and, if successful, you will get a <tt>Cancelled</tt> error on the <tt>Done</tt> handler.
		 *
		 * <p>Note that, because the requests run asynchronously, after calling this function your request might still succeed or fail with
		 * a reason other than <tt>Cancelled</tt>: the operation might have actually completed by the time you called <tt>end()</tt>.</p>
		 *
		 * @method
		 * @name mzero.create.scanner.ScanSymbolRequest#end
		 */
		 
		/**
		 * Represents a Request to scan and decode a driver's license. While the operation is ongoing, it will fire back
		 * some progress events
		 * @name mzero.create.scanner.LicenseScanRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.scanner.scanLicense
		 * @see mzero.create.scanner.LicenseScanProgress
		 * @see mzero.create.scanner.LicenseScanResult
		 */
		
		/**
		 * Data provided by a progress event during a {@link mzero.create.scanner.LicenseScanRequest}, which is specially useful for slow scanning devices.
		 *
		 * @class mzero.create.scanner.LicenseScanProgress
		 * @property {String} Stage - Indicates the current phase of the scanning process:
		 * <dl>
		 * <dt>WaitingForScan</dt> <dd>there is no document in front of the device</dd>
		 * <dt>Scanning</dt> <dd>the document is being scanned</dd>
		 * <dt>Analyzing</dt> <dd>the document is being decoded</dd>
		 * <dt>Complete</dt> <dd>the process has finished and the result will follow</dd>
		 * @property {Number} PercentageComplete - A lose estimate of the progress been made so far during the process
		 *
		 * @see mzero.create.scanner.LicenseScanRequest
		 * @see mzero.create.scanner.LicenseScanResult
		 */
		
		/**
		 * Data obtained on successful completion of {@link mzero.create.scanner.LicenseScanRequest}
		 *
		 * @class mzero.create.scanner.LicenseScanResult
		 * @property {Object.<String, String>} RawEntries - A dictionary containing the standard fields present in the driver's license and their string values. You may want to refer to the other properties in the result object since they simplify their use
		 * @property {String} City -
		 * @property {String} Country -
		 * @property {Date} DateOfBirth -
		 * @property {Date} ExpirationDate -
		 * @property {String} FirstName -
		 * @property {String} Gender -
		 * @property {Date} IssuedDate -
		 * @property {String} LastName -
		 * @property {String} LicenseNumber -
		 * @property {String} MiddleName -
		 * @property {String} PostalCode -
		 * @property {String} StateCode -
		 * @property {String} StateName - Warning: if not available, it might be the same as the StateCode
		 * @property {String} StreetAddress -
		 * @property {String} StreetAddress2 -
		 * @see mzero.create.scanner.LicenseScanRequest
		 * @see mzero.create.scanner.LicenseScanProgress
		 */
		 
		/**
		 * Start the driver's license scan request, providing the callback handlers to be used.
		 *
		 * @param eventHandlers {Object} - Event handlers
		 * @param eventHandlers.Progress {mzero.create.EventCallback} - Progress notifications on the scan process. The <tt>data</tt> value is of type {@link mzero.create.scanner.LicenseScanProgress}.
		 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. In case of error, the <tt>err</tt> value will indicate the reason (such as <tt>Timeout</tt> when the user does not present a symbol or <tt>Cancelled</tt> if the request was cancelled).
		 * @method
		 * @name mzero.create.scanner.LicenseScanRequest#start
		 */
		
		/**
		 * Call <tt>end()</tt> to indicate that the request should finish as soon as possible, interrupting the operation of the device. This
		 * hurries the end of the request and, if successful, you will get a <tt>Cancelled</tt> error on the <tt>Done</tt> handler.
		 *
		 * <p>Note that, because the requests run asynchronously, after calling this function your request might still succeed or fail with
		 * a reason other than <tt>Cancelled</tt>: the operation might have actually completed by the time you called <tt>end()</tt>.</p>
		 *
		 * @method
		 * @name mzero.create.scanner.LicenseScanRequest#end
		 */
		
		/**
		 * Activates the scanner device until the user presents a symbol or a timeout occurs.
		 *
		 * @return {mzero.create.scanner.ScanSymbolRequest} Request ready to be started
		 * @see mzero.create.scanner.ScanSymbolResult
		 */
		scanSymbol: function() {
			var code = "CA";
			var request = service.createRequest("ScanQRCode", { "Country" : code });
			request.withSignal("end", function() {
				request._sendSignal(["CancelScanQRCode", { "Country" : code }]);
			}, 0)
			.succeedsOn("ScanQRCodeSucceeded")
			;
			return request;
		},
		
		/**
		 * Activates the license scanner device until the user presents a document or a timeout occurs.
		 *
		 * @return {mzero.create.scanner.LicenseScanRequest} Request ready to be started
		 * @see mzero.create.scanner.LicenseScanResult
		 */
		scanLicense: function() {
			var code = "US";
			var request = service.createRequest("ScanLicense", { "Country" : code });
			request.withSignal("end", function() {
				request._sendSignal(["CancelScan", { "Country" : code }]);
			}, 0)
			.firingEvents({ "ScanLicenseEvent" : "Progress" })
			.succeedsOn("ScanLicenseSucceeded")
			.transformResultUsing(prepareScanLicenseResult)
			;
			return request;
		}
	};
	return exports;
})();
