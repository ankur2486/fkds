/**
 * Interface to the Printer service provided by the platform
 *
 * @namespace printer
 * @memberof mzero.create
 */
mzero.create.printer = (function() {
	var webprinter = mzero.create.MAPEnv.createService("WebPrint");
	/**
	 * @lends mzero.create.printer
	 */
	var exports = {
		/**
		 * Parameters to the <code>printXMLString()</code> function.
		 *
		 * <p>
		 * Other than the data string having to reference the XSL file that will render it at print time, the XML format
		 * is freely defined by the application.
		 * </p>
		 * <p>
		 * For more details, please refer to the SDK demo and the external resource files in the <code>PrintXML-Files</code> directory.
		 * </p>
		 *
		 * @name mzero.create.printer.PrintXMLStringParams
		 * @class
		 * @property {string} XMLString - String containing XML data and a reference to the XSL file to be used for printing
		 * @property {int} NumberOfCopies - Number of copies to print out; if the value is not specified or it is invalid, it defaults to 1
		 * @see mzero.create.printer.printXMLString
		 * @example <caption>Example printable XML data string</caption>
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="giftcard-activation.xsl"?>
<activation>
  <store number="843" phone="123-456-7890"/>
  <where lane="001" cashier="987654321"/>
  <transaction>
    <card>Sports Gift Card</card>
    <value>25</value>
    <acct>*************987654</acct>
    <approval-code>345678</approval-code>
    <date>07-31-2013</date>
    <time>15:59:47</time>
  </transaction>
</activation>
		 */
		
		/**
		 * Represents a Request to print an XML data string, which will be rendered into a printable format (HTML) using
		 * an XSL transformation.
		 *
		 * @name mzero.create.printer.PrintXMLStringRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.printer.printXMLString
		 */
		/**
		 * Creates a Request object that controls the process of printing an XML data string through an XSL transformation.
		 * @param {mzero.create.printer.PrintXMLStringParams} params - Print parameters
		 * @return {mzero.create.printer.PrintXMLStringRequest} Request ready to be started
		 */
		printXMLString: function(params) {
			var self = this;
			var numCopies = params.NumberOfCopies;
			if(!numCopies || numCopies < 1) numCopies = 1;
			var request = webprinter.createRequest("PrintXml", { "Xml" : params.XMLString, "NumberOfCopies" : numCopies })
				.succeedsOn("PrintXmlSucceeded")
			;
			return request;
		},
	};
	return exports;
})();
