/**
 * Initializes the aws service using the given configuration information
 */
$.getJSON("AWSAuth.json", function( data ) {
    AWS.config.update(data);
    AWS.config.region = 'us-west-1';
});