/**
 * Admin Return Key component to manually return a key to a customer
 */
angular.
module('adminReturnKey', ['ngCookies']).
component('adminreturnkey', {
    templateUrl: 'components/admin-return-key/admin-return-key.html',
    controllerAs: "adminReturnKeyCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter','MyService',
    function AdminReturnKeyController($cookies, InitIdle, $location, $http, $filter,MyService) {
        InitIdle.init();
        var self = this;
        self.master = {};
        self.page_name = "admin_return_key";
        self.location_path = $location.path();
        self.process_confirmed = false;

        //prepopulate the customer form fields if a customer exists in the database
        if ( $cookies.key_drop_appointment_id )
        {
            self.session_id = $cookies.key_drop_appointment_id;
            console.log(self.session_id);
            $http.get('http://localhost:5000/appointments/'+self.session_id).then(function(response) {
                self.appointment = response['data'];
                self.session_id = response['data']['_id'];
                self.key_slot = response['data']['key_slot'];
                self.key_slot_id = response['data']['key_slot_id'];

                // mzapi call for retrive key
                self.KeyRetrieve(self.key_slot);
            }).catch(function(err) {
                throw err;
            });
        }

        self.KeyRetrieve = function (lockerId) {
            if (MyService.devMode)
                return false;

            var eventHandlers = {
                Retrieving: function (err, data) {
                    MyService.logInfoNError("info", "Inside Retrieving event " + data.Locker);
                },
                Done: function (err, data) {
                    if (err) {
                        MyService.logInfoNError("error", "In key retrieve done error " + err);                       
                    }
                    else if (data) {
                        MyService.logInfoNError("info", "In done event " + data);                      
                    }
                }
            };
            var params = { Locker: lockerId };
            var request = mzero.create.locker.retrieve(params).start(eventHandlers);
            MyService.logInfoNError("info", "Retrieve from locker");           
        };


        self.finalize = function()
        {
            $http.patch('http://localhost:5000/key_table/'+self.key_slot_id, {
                appointment_id: null,
                is_occupied:false
            }).then(function(response){
                //success
            }).catch(function(err) {
                MyService.logInfoNError("error", "Error while updating appointment_id and is_occupied" + err);
               
                throw err;
            });

            $http.patch('http://localhost:5000/appointments/'+self.session_id, {
                key_slot_id: null,
                key_slot: null,
                key_status: 'picked up',
                appt_status: 'completed'
            }).then(function(response){
                //success
            }).catch(function(err) {
                MyService.logInfoNError("error", "Error while updating key_status and appt_status" + err);              
                throw err;
            });

            self.process_confirmed = true;

        };

        self.change_page = function () {
            MyService.reportScreen("DashboardSummary", "User clicked on CONFIRM button.");
            $location.path('/dashboard-summary');
        };
    }]
});
