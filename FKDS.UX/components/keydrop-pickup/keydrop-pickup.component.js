/**
 * Keydrop Pickup component to load keydrop pickup page
 */
angular.
module('keydropPickup', ['ngCookies']).
component('keydroppickup', {
    templateUrl: 'components/keydrop-pickup/keydrop-pickup.html',
    controllerAs: "keydropPickupCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
        function KeydropPickupController($cookies, InitIdle, $location, $http, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "keydrop_pickup";
            self.location_path = $location.path();

            //get the key slot to drop for this appointment customer
            if ($cookies.session_id) {
                session_id = $cookies.session_id;
                $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                    appointment = response['data'];
                    self.key_slot = appointment['key_slot'];
                    self.key_slot_id = appointment['key_slot_id'];

                    // mzapi call for retrive key
                    self.KeyRetrieve(self.key_slot);

                }).catch(function (err) {
                    throw err;
                });
            }

            MyService.reportScreen("KeydropPickupPage", "Retriving key is in progress");

            $('body').removeClass('app-body-inner-bg');
            
            // mzapi call for retrive key
            self.KeyRetrieve = function (lockerId) {
                if (MyService.devMode)
                    return false;

                var eventHandlers = {
                    Retrieving: function (err, data) {
                        MyService.logInfoNError("info", "In Key retrive function" + data.Locker);                        
                    },
                    Done: function (err, data) {
                        if (err) {
                            MyService.logInfoNError("error", "In done event of Key retrive function" + err);
                        }
                        else if (data) {
                            MyService.logInfoNError("info", "In done event of Key retrive function" + data);                          
                        }
                    }
                };
                var params = { Locker: lockerId };
                var request = mzero.create.locker.retrieve(params).start(eventHandlers);
                MyService.logInfoNError("info", "Retrieving from locker" );                
            }

            self.change_page = function () {
                MyService.addPageTimer();
                MyService.clearSession("HomePage", "Session ended, User clicked on home button.");
               
                //log the page history to the appointment
                if ($cookies.session_id) {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                        logging_array.push({
                            "access_date": current_datetime,
                            "page_name": self.page_name,
                            "url_path": self.location_path
                        });

                        $http.patch('http://localhost:5000/appointments/' + session_id, {
                            logging_list: logging_array,
                            key_status: 'picked up',
                            appt_status: 'completed',
                            key_slot_id: null,
                            key_slot: null
                        }).then(function (response) {
                            //success
                        }).catch(function (err) {
                            MyService.logInfoNError("error", "Error while saving key_status & appt_status");
                            throw err;
                        });

                    }).catch(function (err) {
                        throw err;
                    });
                }

                //update keyslot to reset it
                $http.patch('http://localhost:5000/key_table/' + self.key_slot_id, {
                    appointment_id: null,
                    is_occupied: false,
                }).then(function (response) {
                    //success
                }).catch(function (err) {
                    MyService.logInfoNError("error", "Error while saving appointment_id & is_occupied");                  
                    throw err;
                });
            }
        }]
});
