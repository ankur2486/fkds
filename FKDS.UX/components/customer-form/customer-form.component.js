/**
 * Customer Form component to load customer form
 */
angular.
module('customerForm', ['ngCookies']).
component('customerform', {
    templateUrl: 'components/customer-form/customer-form.html',
    controllerAs: "customerFormCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
    function CustomerFormController($cookies, InitIdle, $location, $http, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.master = {};
        self.page_name = "customer_form";
        self.location_path = $location.path();

        self.cust_form = {};

        //prepopulate the customer form fields if a customer exists in the database
        if ( $cookies.customer_dictionary )
        {
            self.customerDict = $cookies.customer_dictionary;

            self.cust_form.first_name = self.customerDict['first_name'];
            self.cust_form.last_name = self.customerDict['last_name'];
            self.cust_form.street = self.customerDict['street'];
            self.cust_form.city = self.customerDict['city'];
            self.cust_form.state = self.customerDict['state'];
            self.cust_form.zipcode = self.customerDict['zipcode'];
            //self.cust_form.pin = parseInt(self.customerDict['pin'], 10);
        }
        else {
            $cookies.customer_dictionary = {};
        }

        self.backTohome = function () {
            MyService.reportScreen("license-or-manual", "User clicked on back button on the customer form page.");
        };

        self.update = function (isValid, form) {
            if (isValid) {
                MyService.addPageTimer();
                self.master = angular.copy(form);
               
                // $cookies.customer_dictionary = self.master;
                $cookies.customer_dictionary['first_name'] = self.cust_form.first_name;
                $cookies.customer_dictionary['last_name'] = self.cust_form.last_name;
                $cookies.customer_dictionary['street'] = self.cust_form.street;
                $cookies.customer_dictionary['city'] = self.cust_form.city;
                $cookies.customer_dictionary['state'] = self.cust_form.state;
                $cookies.customer_dictionary['zipcode'] = self.cust_form.zipcode;

                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push(
                           {"access_date": current_datetime,
                           "page_name": self.page_name,
                           "url_path": self.location_path});

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array,
                           customer: self.cust_form
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           MyService.logInfoNError("error", "Error while saving customer data " + err);                          
                           throw err;
                       });
                   }).catch(function(err) {
                       throw err;
                   });
                }

                if($cookies.admin_add_customer)
                {
                    var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                    current_datetime.toString();
                    current_datetime += ' GMT';

                    $http.post('http://localhost:5000/appointments', {
                        logging_list: [
                            {
                                "access_date": current_datetime,
                                "page_name": self.page_name,
                                "url_path": self.location_path
                            }
                        ],
                        customer: self.cust_form,
                        admin_created: true
                    }).then(function(response){
                        //success
                        if (response.data['_id']) {
                            $cookies.session_id = response.data['_id'];
                        }
                    }).catch(function(err) {
                        MyService.logInfoNError("error", "Error while saving customer data " + err);                       
                        throw err;
                    });
                }

                MyService.reportScreen("manual-more-info", "User clicked on submit button on the customer form page.");
                $location.path('/manual-more-info');
            }
        };
    }]
});
