/**
 * Pin Page component to load pin page
 */
angular.
module('serviceCreationPinPage', ['ngCookies']).
component('servicecreationpinpage', {
    templateUrl: 'components/service-creation-pin-page/service-creation-pin-page.html',
    controllerAs: "serviceCreationPinPageCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter','MyService',
        function ServiceCreationPinPageController($cookies, InitIdle, $location, $http, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "service_creation_pin_page";
        self.location_path = $location.path();
        self.pin = "";
        self.message = "";

        MyService.enterSessionScreen("ServiceCreationPinPage", "User clicked on admin pin page.")
        MyService.reportScreen("ServiceCreationPinPage", "User clicked on admin pin page.");

        self.update = function(isValid, pin) {
            MyService.addPageTimer();
            if (isValid) {
                if (pin == '9969')
                {
                    MyService.reportScreen("DashboardSummary", "Admin select Dashboard summary");
                    //$location.path('/service-creation');
                    $location.path('/dashboard-summary');
                }
                else
                {
                    self.message = "Wrong Pin.";
                }
            }
        };
    }]
});
