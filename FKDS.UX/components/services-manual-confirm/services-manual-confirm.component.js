/**
 * Services Manual Confirm component to echo chosen manual services
 */
angular.
module('servicesManualConfirm', ['ngCookies', 'ngMaterial']).
component('servicesmanualconfirm', {
    templateUrl: 'components/services-manual-confirm/services-manual-confirm.html',
    controllerAs: "servicesManualConfirmCtrl",
    controller: ['$http', '$cookies', '$mdDialog', 'initIdle', '$location', '$filter', 'MyService',
        function ServicesManualConfirmController($http, $cookies, $mdDialog, InitIdle, $location, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "services_manual_confirm";
            self.location_path = $location.path();

            $('body').addClass('app-body-inner-bg');

            //scrollbar configuration
            self.config = {
                autoHideScrollbar: false,
                theme: 'light',
                advanced: {
                    updateOnContentResize: true
                },
                scrollInertia: 0,
                axis: 'y',
            };

            //pull the chosen services if any are stored
            if ($cookies.manual_services_comments) {
                self.complete_manual_services_list = $cookies.manual_services_comments;
            }

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
            }

            if ($cookies.vehicle != null) {
                var vehicle = $cookies.vehicle;
                self.vehicle_name = vehicle.year + " " + vehicle.make + " " + vehicle.model;
                self.vin = vehicle.vin;
                self.mileage = vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //pop-up dialog definition
            self.showDialog = function (ev, comment) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Service Details')
                        .textContent(comment)
                        .ariaLabel('Service Details')
                        .ok('Ok')
                        .targetEvent(ev)
                );
            };

            self.backToService = function () {
                MyService.addPageTimer();
                MyService.reportScreen("services-manual-describe", "User clicked on back button on the services manual confirm page.");
                $location.path('/services-manual-describe');
            };
            self.change_page = function () {
                MyService.addPageTimer();
                MyService.reportScreen("ServicesSignaturePage", "User clicked on next button on the services manual confirm page.");
                //log the page history to the appointment
            if ($cookies.session_id)
            {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                   logging_array.push({"access_date": current_datetime,
                            "page_name": self.page_name,
                       "url_path": self.location_path});

                        $http.patch('http://localhost:5000/appointments/' + session_id, {
                            logging_list: logging_array
                        }).then(function (response) {
                            //success
                        }).catch(function (err) {
                            console.log(err);
                            throw err;
                        });

                    }).catch(function (err) {
                        throw err;
                    });
                }
                $location.path('/services-signature')
            }
        }]
});
