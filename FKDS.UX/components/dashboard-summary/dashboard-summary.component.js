/**
 * Dashboard Summary component to load dashboard summary
 */
angular.
module('dashboardSummary', ['ngCookies', 'ngMaterial']).
component('dashboardsummary', {
    templateUrl: 'components/dashboard-summary/dashboard-summary.html',
    controllerAs: "dashboardSummaryCtrl",
    controller: ['$http', '$cookies', '$mdDialog', 'initIdle', '$location', '$filter','MyService',
        function DashboardSummaryController($http, $cookies, $mdDialog, InitIdle, $location, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "dashboard_summary";
        self.location_path = $location.path();
        self.appointment_object_list = [];

        //delete all cookies
        delete $cookies['customer_dictionary'];
        delete $cookies['appointment_comments'];
        delete $cookies['vehicle'];
        delete $cookies['session_id'];
        delete $cookies['dummyLicense'];
        delete $cookies['services_chosen'];
        delete $cookies['manual_services_chosen'];
        delete $cookies['manual_services_comments'];
        delete $cookies['services_chosen'];
        delete $cookies['upsell_services_chosen'];
        delete $cookies['key_drop_appointment_id'];
        delete $cookies['admin_add_customer'];

        //scrollbar configuration
        self.config = {
            autoHideScrollbar: false,
            theme: 'light',
            advanced: {
                updateOnContentResize: true
            },
            setHeight: 1200,
            scrollInertia: 0,
            axis: 'y',
        };

        // self.dms_service_dict = {};
        // $http.get('http://localhost:5000/services/?max_results=60').then(function(response) {
        //     for ( var i = 0; i < response['data']['_items'].length; i++ )
        //     {
        //         self.dms_service_dict[response['data']['_items'][i]['_id']] = response['data']['_items'][i];
        //     }
        // }).catch(function(err) {
        //     throw err;
        // });
        //
        // self.upsell_service_dict = {};
        // $http.get('http://localhost:5000/upsell_services/?max_results=60').then(function(response) {
        //     for ( var i = 0; i < response['data']['_items'].length; i++ )
        //     {
        //         self.upsell_service_dict[response['data']['_items'][i]['_id']] = response['data']['_items'][i];
        //     }
        // }).catch(function(err) {
        //     throw err;
        // });

        $http.get('http://localhost:5000/appointments/?where={"appt_status": "not scheduled"}&max_results=2000').then(function(response) {
            self.appointment_object_list = response['data']['_items'];

            $http.get('http://localhost:5000/appointments/?where={"appt_status": "scheduled"}&max_results=2000').then(function(response) {
                self.appointment_object_list = self.appointment_object_list.concat(response['data']['_items']);
        }).catch(function(err) {
            throw err;
        });
        }).catch(function(err) {
            throw err;
        });


            self.change_page = function (data, page) {
             MyService.addPageTimer();

                if (page == "RETURNKEY")
                    MyService.reportScreen("admin-keydrop-form", "User clicked on return key now button on the dashboard summery page.");
                else if (page == "MANUALLYREMOVED")
                    MyService.reportScreen("admin-return-key", "User clicked on KEY Manually Removed button on the dashboard summery page.");
                else
                    MyService.reportScreen("dashboard-customer-profile", "User clicked on Customer profile on the dashboard summery page.");

            $cookies.key_drop_appointment_id = data;
        };

        self.add_customer_page = function () {
             MyService.addPageTimer();
                MyService.reportScreen("customer-form", "User clicked on CREATE NEW KEY DROP button on the dashboard summery page.");
            $cookies.admin_add_customer = true;
        };

        //change the list of customers shown
        self.change_view = function (view) {

                if (view == 'active') {
                $http.get('http://localhost:5000/appointments/?where={"appt_status": "not scheduled"}&max_results=2000').then(function(response) {
                    self.appointment_object_list = response['data']['_items'];

                    $http.get('http://localhost:5000/appointments/?where={"appt_status": "scheduled"}&max_results=2000').then(function(response) {
                        self.appointment_object_list = self.appointment_object_list.concat(response['data']['_items']);
                    }).catch(function(err) {
                        throw err;
                    });
                }).catch(function(err) {
                    throw err;
                });


            }

                if (view == 'archived') {
                $http.get('http://localhost:5000/appointments/?where={"appt_status" : "completed"}&max_results=2000').then(function(response) {
                    self.appointment_object_list = response['data']['_items'];
                }).catch(function(err) {
                    throw err;
                });
            }
        };

    }]
});
