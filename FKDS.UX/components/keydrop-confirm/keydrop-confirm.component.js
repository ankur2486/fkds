/**
 * Keydrop Confirm component to load keydrop confirmation page
 */
angular.
module('keydropConfirm', ['ngCookies', 'ngMaterial']).
component('keydropconfirm', {
    templateUrl: 'components/keydrop-confirm/keydrop-confirm.html',
    controllerAs: "keydropConfirmCtrl",
    controller: ['$cookies', '$mdDialog', 'initIdle', '$location', '$http', '$filter', 'MyService', 'Utils',
        function KeydropConfirmController($cookies, $mdDialog, InitIdle, $location, $http, $filter, MyService, $Utils) {
        InitIdle.init();
        var self = this;
        self.page_name = "keydrop_confirm";
        self.location_path = $location.path();
        MyService.reportScreen("KeydropConfirmPage", "Confirming key");
        
        $('body').addClass('app-body-inner-bg');

        if ($cookies.customer_dictionary)
        {
            customer_dict = $cookies.customer_dictionary;

            if ('email' in customer_dict)
            {
                customer_email = customer_dict['email'];
                $Utils.send_service_email(self, customer_email);
            }
        }
        else
        {
            customer_email = "";
        }

        self.showPrompt = function (ev) {
          
            //Grab email from cookies if it exists
            MyService.reportScreen("KeydropConfirmPage", "User clicked on resend button on the keydrop confirm page.");
            if ($cookies.customer_dictionary)
            {
                customer_dict = $cookies.customer_dictionary;

                if ('email' in customer_dict)
                {
                    customer_email = customer_dict['email'];
                }
            }
            else
            {
                customer_email = "";
            }

            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.prompt()
                .title('Please review your email.')
                .textContent('Stored email prepopulated.')
                .placeholder('Email')
                .ariaLabel('Email')
                .initialValue(customer_email)
                .targetEvent(ev)
                .ok('Confirm')
                .cancel('Cancel');

            // self.send_receipt_email = function send_receipt_email(result){
            $mdDialog.show(confirm).then(function (result) {
                var to = result;
                $Utils.send_service_email(self, to);
            });
            };

        self.change_page = function () {
            //log the page history to the appointment
            if ($cookies.session_id)
            {
                session_id = $cookies.session_id;
                $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                    logging_array = response.data['logging_list'];

                    var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                    current_datetime.toString();
                    current_datetime += ' GMT';

                    logging_array.push({"access_date": current_datetime,
                        "page_name": self.page_name,
                        "url_path": self.location_path});

                    $http.patch('http://localhost:5000/appointments/'+session_id, {
                        logging_list: logging_array,
                        appt_status: 'scheduled'
                    }).then(function(response){
                        //success
                    }).catch(function(err) {
                        MyService.logInfoNError("error", "Error while saving appt_status and logging data " + err);                      
                        throw err;
                    });

                }).catch(function(err) {
                    throw err;
                });
            }
             MyService.addPageTimer();
            MyService.reportScreen("HomePage", "User clicked on finish button on the key drop confirm page");
            MyService.clearSession("HomePage", "Session ended");
        };
    }]
});
