/**
 * Services Manual component to load available manual services
 */
angular.
module('servicesManual', ['ngCookies']).
component('servicesmanual', {
    templateUrl: 'components/services-manual/services-manual.html',
    controllerAs: "servicesManualCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
        function ServicesManualController($http, $cookies, InitIdle, $location, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "services_manual";
            self.location_path = $location.path();
            self.manual_services_list = [];

            //scrollbar configuration
            self.config = {
                autoHideScrollbar: false,
                theme: 'light',
                advanced: {
                    updateOnContentResize: true
                },
                setHeight: 900,
                scrollInertia: 0,
                axis: 'y',
            };

            //manual service array
            self.service_array = ['Air Conditioner', 'Air Filtration', 'Alignment', 'Battery', 'Break Service', 'Cooling System', 'Drivetrain', 'Engine',
                'Fuel System', 'Inspection', 'Oil Change', 'Suspension', 'Tires', 'Transmission', 'Windshield', 'Other Service'];

            //pull the chosen services if any are stored
            if ($cookies.manual_services_chosen) {
                self.manual_services_list = $cookies.manual_services_chosen;
            }

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
            }

            if ($cookies.vehicle != null) {
                var vehicle = $cookies.vehicle;
                self.vehicle_name = vehicle.year + " " + vehicle.make + " " + vehicle.model;
                self.vin = vehicle.vin;
                self.mileage = vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //checkbox function to see if a service is in the chosen list
            self.checkboxStatus = function (service) {
                return (self.manual_services_list.indexOf(service) !== -1);
            };

            //add service to the list or remove it if it is already present
            self.update_choices = function (service) {
                MyService.addPageTimer();
                if (self.manual_services_list.includes(service)) {
                    var index = self.manual_services_list.indexOf(service);
                    self.manual_services_list.splice(index, 1);
                }
                else {
                    self.manual_services_list.push(service);
                }
            };

            self.save_choices = function () {
            var manual_services_name_list = [];
           
             MyService.addPageTimer();
            MyService.reportScreen("ServicesManualDescribePage", "User clicked on next button on the services manual page.");
            for (var i = 0; i < self.manual_services_list.length; i++) {
                manual_services_name_list.push(self.manual_services_list[i]);
            }

                $cookies.manual_services_chosen = manual_services_name_list;

            //log the page history to the appointment
            if ($cookies.session_id)
            {
               session_id = $cookies.session_id;
               $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                   logging_array = response.data['logging_list'];

                   var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                   current_datetime.toString();
                   current_datetime += ' GMT';

                   logging_array.push({"access_date": current_datetime,
                       "page_name": self.page_name,
                       "url_path": self.location_path});

                   $http.patch('http://localhost:5000/appointments/'+session_id, {
                       logging_list: logging_array
                   }).then(function(response){
                       //success
                   }).catch(function(err) {
                       console.log(err);
                       throw err;
                   });

               }).catch(function(err) {
                   throw err;
               });
            }
        };
    }]
});
