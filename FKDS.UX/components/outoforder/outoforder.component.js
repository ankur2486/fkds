/**
 * Appointment Comment component to load field for comments
 */
angular.
module('outOfOrder', ['ngCookies']).
component('outoforder', {
    templateUrl: 'components/outoforder/outoforderPage.html',
    controllerAs: "OutOfOrderCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
    function OutOfOrderController($http, $cookies, InitIdle, $location, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "outoforder";
        self.location_path = $location.path();


       
    }]
});
