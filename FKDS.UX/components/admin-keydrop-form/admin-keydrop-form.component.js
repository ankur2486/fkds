/**
 * Admin Keydrop Form component to load service total and key drop information
 */
angular.
module('adminKeydropForm', ['ngCookies']).
component('adminkeydropform', {
    templateUrl: 'components/admin-keydrop-form/admin-keydrop-form.html',
    controllerAs: "adminKeydropFormCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter','MyService', 'Utils', '$scope',
    function AdminKeydropFormController($cookies, InitIdle, $location, $http, $filter,MyService, $Utils, $scope) {
        InitIdle.init();
        var self = this;
        self.master = {};
        self.page_name = "admin_keydrop_form";
        self.location_path = $location.path();
        self.price_given = false;
        self.swap_div = false;
        self.process_confirmed = false;
        self.keydrop_form = {};
        var keyStoreRequest = null;
        self.LockerStatusArray = null;

        //prepopulate the customer form fields if a customer exists in the database
        if ( $cookies.key_drop_appointment_id )
        {
            self.session_id = $cookies.key_drop_appointment_id;
            $http.get('http://localhost:5000/appointments/'+self.session_id).then(function(response) {
                self.appointment = response['data'];
            }).catch(function(err) {
                throw err;
            });
        }


        self.appointment_payment_update = function(payment_status) {

            //update appointment payment status
            if ( $cookies.key_drop_appointment_id )
            {
                self.session_id = $cookies.key_drop_appointment_id;
                $http.patch('http://localhost:5000/appointments/'+self.session_id, {
                    payment_status:payment_status
                }).then(function(response){
                    //success
                }).catch(function(err) {
                    MyService.logInfoNError("error", "Error while updating appointment payment status" + err);
                    throw err;
                });
            }

            if (payment_status == 'paid')
            {
                self.price_given = true;

                if ( $cookies.key_drop_appointment_id )
                {
                    self.session_id = $cookies.key_drop_appointment_id;
                    $http.patch('http://localhost:5000/appointments/'+self.session_id, {
                        service_total:0
                    }).then(function(response){
                        //success
                    }).catch(function(err) {
                        MyService.logInfoNError("error", "Error while updating appointment service_total" + err);
                        throw err;
                    });
                }

                //generate key drop slot
                $http.get('http://localhost:5000/key_table/?where={"is_occupied": false}').then(function(response) {
                    //console.log(response['data'][0]['key_slot']);
                    //self.key_slot = response['data']
                    self.key_slot = response['data']['_items'][0]['key_slot'];
                    self.key_slot_id = response['data']['_items'][0]['_id'];
                   

                    //light up slot for self.key_slot
                    //mzero API call
                    //check cup availablity using mzapi
                    var available = self.CheckCupAvailable(self.key_slot);
                    if (available)
                        self.KeyStore(self.key_slot);
                    //ToDo else go to next cup 
                   
                }).catch(function(err) {
                    MyService.logInfoNError("error", "Error while generating key drop slot" + err);
                    throw err;
                });

                
            }

            if (payment_status == 'not paid')
            {               
                self.swap_div = true;
            }
        };       

        self.KeyStore = function (lockerId) {  
            if (MyService.devMode)
                return false;

            var eventHandlers = {
                Storing: function (err, data) {
                    console.log("We are storing into locker id " + data.Locker);                   
                },
                Done: function (err, data) {
                    keyStoreRequest = null;
                    if (err) {
                        console.log("In done error " + err);
                    }
                    else if (data) {
                        console.log("In done event " + data);
                    }
                }
            };
            var params = { Locker: lockerId };
            var keyStoreRequest = mzero.create.locker.store(params).start(eventHandlers);
            console.log("Storing in locker");
           
        }

        self.CheckCupAvailable = function (lockerId) {
            MyService.logInfoNError("info", "Checking Locker availability for locker id" + lockerId);
            if (self.LockerStatusArray == null)
                return false;
            else {
                var object = self.LockerStatusArray.find(function (d) {
                    return d.Id === lockerId;
                });

                if (object && object.Status == "Available")
                    return true;
                else
                    return false;
            }

        };

        self.CheckLockers = function () {
            if (MyService.devMode) {
                self.LockerStatusArray = [{ 'Id': 1, 'Status': 'Available' }, { 'Id': 2, 'Status': 'Unknown' }];
                return false;
            }

            var eventHandlers = {
                Done: function (err, data) {
                    if (err) {
                        MyService.logInfoNError("error", "Error in check locker function" + err);
                    }
                    else if (data) {
                        self.LockerStatusArray = data;
                        MyService.logInfoNError("info", "In check locker done event" + data);
                    }
                }
            };
            var request = mzero.create.locker.checkLockers().start(eventHandlers);
            MyService.logInfoNError("info", "Checking Lockers status");
        };

        $scope.$on('$destroy', function () {
            self.KeyStoreEnd();
        });

        self.KeyStoreEnd = function () {

            if (!MyService.devMode) {
                if (keyStoreRequest) {                   
                    keyStoreRequest.end();
                    MyService.logInfoNError("info", "Key store stop function");
                }
            }
        };

        self.update = function (isValid, form) {
           
            if (isValid) {
                MyService.addPageTimer();
                self.master = angular.copy(form);
                MyService.reportScreen("VehicleInfoPage", "User clicked on submit button on the customer form page.");

                //log the service total to the appointment
                if (self.session_id)
                {
                    $http.patch('http://localhost:5000/appointments/'+self.session_id, {
                        service_total:self.master.service_total
                    }).then(function(response){
                        //success
                    }).catch(function(err) {
                        MyService.logInfoNError("error", "Error while updating service_total" + err);
                        throw err;
                    });
                }

                self.price_given = true;

                //generate key drop slot
                $http.get('http://localhost:5000/key_table/?where={"is_occupied": false}').then(function(response) {
                    self.key_slot = response['data']['_items'][0]['key_slot'];
                    self.key_slot_id = response['data']['_items'][0]['_id'];
                    
                    //light up slot for self.key_slot
                    //mzero API call
                    //check cup availablity 
                    var available = self.CheckCupAvailable(self.key_slot);
                    if (available)
                    self.KeyStore(self.key_slot);

                }).catch(function(err) {
                    throw err;
                });
            }
        };

        self.finalize = function()
        {
            self.KeyStoreEnd();

            $http.patch('http://localhost:5000/key_table/'+self.key_slot_id, {
                appointment_id: self.session_id,
                is_occupied:true
            }).then(function(response){
                //success
            }).catch(function(err) {
                MyService.logInfoNError("error", "Error while updating appointment_id and is_occupied in key_table" + err);
                throw err;
            });

            $http.patch('http://localhost:5000/appointments/'+self.session_id, {
                key_slot_id: self.key_slot_id,
                key_slot: self.key_slot,
                key_status: 'slot assigned'
            }).then(function(response){
                //success
            }).catch(function(err) {
                MyService.logInfoNError("error", "Error while updating key_slot_id and key_slot in appointments " + err);
                throw err;
            });

            self.process_confirmed = true;

        };

        self.change_page = function () {
            MyService.reportScreen("DashboardSummary", "User clicked on CONFIRM button.");
            if ($cookies.customer_dictionary && $cookies.customer_dictionary.email) {
                var customer_email = $cookies.customer_dictionary.email;
                $Utils.send_service_email(self, customer_email);
            }
            $location.path('/dashboard-summary');
        };

        self.CheckLockers();
    }]
});
