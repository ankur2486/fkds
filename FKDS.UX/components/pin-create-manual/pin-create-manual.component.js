/**
 * Pin Create Manual component to load create pin manual page
 */
angular.
module('pinCreateManual', ['ngCookies']).
component('pincreatemanual', {
    templateUrl: 'components/pin-create-manual/pin-create-manual.html',
    controllerAs: "pinCreateManualCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
        function PinCreateManualController($cookies, InitIdle, $location, $http, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "pin_create";
            self.location_path = $location.path();
            self.master = {};
            self.pin1 = "";
            self.pin2 = "";
            self.IsMatch = true;

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'];
            }

            self.checkMatch = function () {
                if (self.pin1 != self.pin2) {
                    self.IsMatch = false;
                    return false;
                }
                self.IsMatch = true;
            };

            self.update = function (isValid) {
                MyService.addPageTimer();
               
                self.checkMatch();
                if (isValid && self.IsMatch) {
                    var pin = self.pin1;

                    //save email and phone field to the record
                    if ($cookies.customer_dictionary) {
                        $cookies.customer_dictionary['pin'] = pin;
                    }
                    else {
                        $cookies.customer_dictionary = {
                            "pin": pin,
                        }
                    }

                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push({"access_date": current_datetime,
                           "page_name": self.page_name,
                           "url_path": self.location_path});

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array,
                           customer: {
                               pin: pin,
                           }
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           MyService.logInfoNError("error", "Error while saving customer data " + err);
                           throw err;
                       });

                   }).catch(function(err) {
                       throw err;
                   });
                }

                MyService.reportScreen("drilldown", "User clicked on confirm button on the Pin Create Manual page.");
                $location.path('/drilldown');
            }
        };
    }]
});
