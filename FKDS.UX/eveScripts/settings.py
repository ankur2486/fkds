MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DBNAME = 'gomotokeydrop'

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']
DATE_FORMAT = "%a, %d %b %Y %H:%M:%S GMT"
IF_MATCH = False
PAGINATION = False

DEBUG = True

services = {
	'schema': {
	    'image_path': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 1500,
	    },
	    'name': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 1500,
		'required': True,
	    },
	    'description': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 15000,
	    },
	    'price': {
		'type': 'integer',
	    },
	    'price_details': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 15000,
	    },
	    'name_filter': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 1500,
        },
	}
}

upsell_services = {
	'schema': {
	    'image_path': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 1500,
	    },
	    'name': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 1500,
		'required': True,
	    },
	    'description': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 15000,
	    },
	    'price': {
		'type': 'integer',
	    },
	    'price_details': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 15000,
	    },
	    'name_filter': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 1500,
        },
	}
}

appointments = {
	'schema': {
		'appt_datetime': {
			'type': 'datetime',
		},
		'admin_created': {
            'type': 'boolean',
            'default': False,
        },
		'appt_comment': {
		    'type': 'string',
		    'maxlength': 5000,
		},
		'qr_code': {
		    'type': 'string',
		    'minlength': 1,
		    'maxlength': 1500,
		},
		'qr_status': {
		    'type': 'string',
		    'minlength': 1,
		    'maxlength': 1500,
		},
		'license_status': {
		    'type': 'string',
		    'minlength': 1,
		    'maxlength': 1500,
		    'default': 'not scanned',
		},
		'appt_status': {
		    'type': 'string',
		    'minlength': 1,
		    'maxlength': 1500,
		    'default': 'not scheduled',
		},
		'services_confirmed': {
			'type': 'list',
			'schema': {
				'type': 'objectid'
			}
		},
		'services_recommended': {
			'type': 'list',
			'schema': {
				'type': 'string'
			}
		},
		'services_finalized': {
			'type': 'list',
			'schema': {
				'type': 'objectid'
			}
		},

		'service_total': {
            'type': 'float',
        },

		'manual_services': {
		    'type': 'list',
		    'schema': {
			    'type': 'dict',
			    'schema': {
				    'name': {'type': 'string', 'maxlength': 100},
				    'description': {'type': 'string', 'maxlength': 5000}
			    }
		    }
		},
        'payment_status': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 1500,
            'default': 'not paid',
        },
        'key_status': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 1500,
            'default': 'none',
        },
        'key_slot': {
        'type': 'integer',
        'nullable': True
        },
        'key_slot_id': {
        'type': 'objectid',
        'nullable': True
        },
		'customer': {
			'type': 'dict',
			'schema': {
				'first_name': {'type': 'string', 'maxlength': 1500},
				'last_name': {'type': 'string', 'maxlength': 1500},
				'email': {'type': 'string', 'maxlength': 1500},
				'phone': {'type': 'string', 'maxlength': 1500},
				'contact_preference': {'type': 'string', 'maxlength': 1500},
				'ford_pass_id': {'type': 'string', 'maxlength': 1500},
				'street': {'type': 'string', 'maxlength': 1500},
				'city': {'type': 'string', 'maxlength': 1500},
				'state': {'type': 'string', 'maxlength': 1500},
				'zipcode': {'type': 'string', 'maxlength': 1500},
				'pin': {'type': 'integer'},
			}
		},
		'vehicle': {
			'type': 'dict',
			'schema': {
				'image': {'type': 'string', 'maxlength': 1500},
				'year': {'type': 'string'},
				'make': {'type': 'string'},
				'model': {'type': 'string'},
				'series': {'type': 'string'},
				'style': {'type': 'string'},
				'vin': {'type': 'string', 'maxlength': 1500},
				'mileage': {'type': 'integer'},
			}
		},
		'logging_list': {
		    'type': 'list',
		    'schema': {
			    'type': 'dict',
			    'schema': {
				    'access_date': {'type': 'datetime'},
			        'page_name': {'type': 'string'},
			        'url_path': {'type': 'string'},
			    }
		    }
		},
	}
}

key_table = {
	'schema': {
	    'appointment_id': {
		'type': 'objectid',
		'nullable': True
	    },
	    'is_occupied': {
		'type': 'boolean',
		'default': False,
	    },
	    'key_slot': {
        'type': 'integer',
        },
	}
}

DOMAIN = {
	'services': services,
	'upsell_services': upsell_services,
	'appointments': appointments,
	'key_table': key_table,
}
