/**
 * Initializes the idle timeout functionality
 */

angular.module('keyDropApp').factory('initIdle', ['$rootScope', '$location', 'Idle', function ($rootScope, $location, Idle) {
    return {
        init: function () {
            $rootScope.$on('IdleTimeout', function () {
                //TODO Save any session data to the database as a partially completed appointment
                $location.path("/home");
                $rootScope.$apply();
                Idle.watch();
            });
        }
    };
}]);

