$('.goKeyName').keyboard({
    layout: 'custom',
    autoAccept: 'false',
    maxLength: 255,
    stickyShift: false,
    customLayout: {
        'default': [
            'q w e r t y u i o p \\ {bksp}',
            '- a s d f g h j k l ; _',
            '{shift} z x c v b n m , . / {shift}',
            '{accept} {space} {cancel} {clear}'
            ],
        'shift': [
            'Q W E R T Y U I O P \\ {bksp}',
            '- A S D F G H J K L ; _',
            '{shift} Z X C V B N M , . / {shift}',
            '{accept} {space} {cancel} {clear}'
            ]
    },
    usePreview: false,
    beforeClose: function (e, keyboard, el, accepted) {
        $('input.current').removeClass('current');
        $("body").css('padding-bottom', '0px');
    }
});


$('.goKeyNumber').keyboard({
    layout: 'custom',
    autoAccept: 'false',
    maxLength: 255,
    customLayout: {
        'default': [
            '7 8 9',
            '4 5 6',
            '1 2 3',
            '. 0 {bksp}'
            ]
    },
    usePreview: false,
    beforeClose: function (e, keyboard, el, accepted) {
        $('input.current').removeClass('current');
        $("body").css('padding-bottom', '0px');
    }
});

$('.goKeyMail').keyboard({
    layout: 'custom',
    autoAccept: 'false',
    maxLength: 255,
    stickyShift: false,
    customLayout: {
        'default': [
            '! 1 2 3 4 5 6 7 8 9 0 \ {bksp}',
            'q w e r t y u i o p [ ]',
            '- a s d f g h j k l ; _',
            '{shift} z x c v b n m , . / {shift}',
            '@gmail.com @hotmail.com @yahoo.com @aol.com @comcast.net @ .com',
            '{accept} {space} {cancel} {clear}'],
        'shift': [
            '! 1 2 3 4 5 6 7 8 9 0 \ {bksp}',
            'Q W E R T Y U I O P [ ]',
            '- A S D F G H J K L ; _',
            '{shift} Z X C V B N M , . / {shift}',
            '@gmail.com @hotmail.com @yahoo.com @aol.com @comcast.net @ .com',
            '{accept} {space} {cancel} {clear}']
    },
    usePreview: false,
    beforeClose: function (e, keyboard, el, accepted) {
        $('input.current').removeClass('current');
        $("body").css('padding-bottom', '0px');
    }
});
