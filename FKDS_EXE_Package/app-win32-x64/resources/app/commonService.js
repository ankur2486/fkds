﻿angular
    .module('keyDropApp')
    .service('MyService', ['$rootScope', '$location', '$mdDialog', '$http', function ($rootScope, $location, $mdDialog, $http) {
        var myService = this;
        myService.devMode = false;
        var mainDisplayTracker = null;
        var logger = null;
        myService.pageTimerHandler = null;
        myService.areYouThereTimerHandler = null;
        myService.pageName;
        myService.progressMessage;
        myService.tempPageName;
        myService.mapServiceActivated = false;
        myService.isScannerActive = false;
        myService.scannerTimeout = false;
        myService.cardReaderServiceEnabled = false;
        myService.pagesTimeOut = {};
        myService.customReportData = {};
        myService.errorTimerHandler = null;
        myService.isShown = false;

        myService.addPageTimer = function () {

            if (myService.pageTimerHandler)
                clearTimeout(myService.pageTimerHandler);

            if (myService.areYouThereTimerHandler)
                clearTimeout(myService.areYouThereTimerHandler);

            $("#sessionTimeOutPopup").modal("hide");
            
            if ($location.url() == "/home")
                return;

            myService.areYouThereTimerHandler = setTimeout(function () {
                myService.showPopup("#sessionTimeOutPopup", parseInt(myService.pagesTimeOut.fkds_pagetimeout.areYouTherePopupTimeout.text));
            }, Number(myService.pagesTimeOut.fkds_pagetimeout.areYouStillTherePopup.text));

            myService.pageTimerHandler = setTimeout(function () {
                $mdDialog.hide();
                $('.modal').modal('hide');
                $('.ui-keyboard-keyset.ui-keyboard-keyset-normal').hide();
                $('.ui-keyboard.ui-widget-content.ui-widget.ui-corner-all.ui-helper-clearfix.ui-keyboard-has-focus').hide();

                myService.clearSession("HomePage", "Session timeout");

            }, Number(myService.pagesTimeOut.fkds_pagetimeout.pageInactivity.text));
        }

        myService.disableTimer = function () {

            if (myService.pageTimerHandler)
                clearTimeout(myService.pageTimerHandler);

            if (myService.areYouThereTimerHandler)
                clearTimeout(myService.areYouThereTimerHandler);

            myService.pageTimerHandler = null;
            myService.areYouThereTimerHandler = null;
        }

        myService.hideWaiting = function () {
            $("#ProgressPopup").modal('hide');//todo create ProgressPopup
        }

        myService.showWaiting = function (errorMessage, debugMessage) {
            var message = 'Please Wait...';
            if (!debugMessage)
                message = (errorMessage ? errorMessage + " " : "") + message;

            myService.progressMessage = message;
            myService.logInfoNError("info", "In show waiting function with message: " + message);
            $("#ProgressPopup").modal('show');//todo create ProgressPopup

        };

        myService.clearSession = function (page, message) {
         
            myService.isShown = false;
            myService.reportScreen(page, message);
            myService.exitSessionScreen(page, message);
            $mdDialog.hide();
            $('.modal').modal('hide');
            $('.ui-keyboard-keyset.ui-keyboard-keyset-normal').hide();
            $('.ui-keyboard.ui-widget-content.ui-widget.ui-corner-all.ui-helper-clearfix.ui-keyboard-has-focus').hide();
            $('.modal-backdrop').remove();
            $location.url('/home');
            if (!$rootScope.$$phase) $rootScope.$apply();
        }

        myService.showPopup = function (popupName, time) {
            $(popupName).modal('show');
            setTimeout(function () {
                $(popupName).modal("hide");
            }, time);
        };

        myService.ReloadAfterMapServiceFails = function () {
            myService.errorTimerHandler = setTimeout(function () {
                document.location.reload(true);
            }, 10000);
        }

        myService.CallInitialiseAfterInterval = function () {
            myService.errorTimerHandler = setTimeout(function () {
                myService.initialize();
            }, 10000);
        }

        this.checkSystem = function (callback) {
            if (myService.devMode) {
                callback();
                return;
            }

            var eventHandlers = {
                Done: function (err, data) {
                    if (err) {
                        if (err.toLowerCase().indexOf('serviceinuse') > -1) {
                            callback();
                        } else {
                            myService.reportScreen('outoforder', 'Error While Report service start.' + err.toString());
                            $location.path('/outoforder');
                            if (!$rootScope.$$phase) $rootScope.$apply();
                            myService.CallInitialiseAfterInterval();
                        }
                    }
                    else {

                        var licenseServiceStat = myService.getServiceState("License", data);
                        var cardReaderServiceStat = myService.getServiceState("MzeroPay", data);

                        if (cardReaderServiceStat == "Available")
                            myService.cardReaderServiceEnabled = true;
                        else
                            myService.cardReaderServiceEnabled = false;

                        if (licenseServiceStat == "Available")
                            myService.isScannerActive = true;
                        else
                            myService.isScannerActive = false;

                        callback();
                    }
                }
            };
            var request = mzero.create.system.report().start(eventHandlers);
        }

        myService.getServiceState = function (key, data) {
            var items = $.grep(data.Services, function (obj) {
                return obj.MAPName == key;
            });

            if (items.length > 0)
                return items[0].State;
        }

        myService.getScreenTrackerSingleton = function () {
            if (mainDisplayTracker)
                return mainDisplayTracker;

            var options = {
                display: "main"
            };

            mainDisplayTracker = mzero.create.components.screentracker.create(options);
            return mainDisplayTracker;
        }

        myService.reportScreen = function (screenName, reason) {

            if (myService.devMode) {
                console.log(screenName + " " + reason);
                return;
            }

            myService.getScreenTrackerSingleton().enter(screenName, reason);
        }

        myService.enterSessionScreen = function (screenName, reason) {

            if (myService.devMode) {
                console.log(screenName + " " + reason);
                return;
            }

            myService.getScreenTrackerSingleton().enterSession("Session_" + Date.now(), screenName, reason);
        }

        myService.exitSessionScreen = function (screenName, reason) {

            if (myService.devMode) {
                console.log(screenName, reason);
                return;
            }

            myService.getScreenTrackerSingleton().exitSession(screenName, reason);
        }

        myService.getConfiguration = function () {

            if (myService.devMode) {
                myService.isScannerActive = true;
                myService.cardReaderServiceEnabled = true;
            }

            myService.logInfoNError("info", "Version: 1.0.9 ");
            
            $http.get('fkds_pagetimeout.json').then(function (response) {
               
                myService.pagesTimeOut = response.data;

            }).catch(function (err) {
                myService.logInfoNError("error", 'Unable to Find file fkds_pageTimeout.json.' + err);
                myService.reportScreen("outoforder", 'Unable to Find file fkds_pageTimeout.json.' + err);
                $location.path('/outoforder');
                if (!$rootScope.$$phase) $rootScope.$apply();
                myService.CallInitialiseAfterInterval();
                throw err;
            });
         

            if (myService.tempPageName == 'drop') {
                myService.enterSessionScreen("license-or-manual", "User wants to drop off key.")
                myService.reportScreen("license-or-manual", "User wants to drop off key.");
                $location.path('/license-or-manual');
                if (!$rootScope.$$phase) $rootScope.$apply();
                myService.addPageTimer();

            }
            else if (myService.tempPageName == 'pickup') {
                myService.enterSessionScreen("QRScanPage", "User wants to pick up key.");
                $location.path('/qr-scan');
                if (!$rootScope.$$phase) $rootScope.$apply();
                myService.addPageTimer();
            }
            else {
                myService.reportScreen("HomePage", "Home page after Initialization.");               
            }
        }

        myService.initialize = function () {

            if (myService.devMode) {
                myService.getConfiguration();
                return;
            }

            var appData = { // optional application data
                MachineId1: "",
                SoftwareId: ""
            };
            var options = {
                Logging: {
                    Console: true
                },
                Connection: {
                    Url: "ws://127.0.0.1:61623",
                    //Url: "ws://10.1.1.185:61623",
                    Login: "mapkiosk",
                    Passcode: "oY8tKFDPCQG4KG"
                },
                AppData: appData
            };
            var eventHandlers = {
                Done: myService.onMAPConnectionDone,
                Disconnected: myService.onMAPDisconnected
            };

            if (!mzero.create.MAPEnv.isInitialized()) {
                mzero.create.MAPEnv.initialize(options, eventHandlers);
                logger = mzero.create.MAPEnv.getLogger('FKDS');
                myService.logInfoNError("info", "Platform connection started");
            }
            else {
                myService.logInfoNError("MapService already activated.");
                myService.mapServiceActivated = true;

                myService.checkSystem(function () {
                    myService.getConfiguration();
                });
            }
        }

        myService.onMAPConnectionDone = function (err, data) {
            // Initialize UI according to connection result
            if (err) {
                myService.logInfoNError("error", "Error initializing API: " + err);
                myService.reportScreen('outoforder', 'Error initializing MapService' + err);
                $location.path('/outoforder');
                if (!$rootScope.$$phase) $rootScope.$apply();
                myService.ReloadAfterMapServiceFails();

            } else {
                myService.logInfoNError("info", "Platform connection ready");
                myService.mapServiceActivated = true;
                myService.checkSystem(function () {
                    myService.getConfiguration();
                });
            }
        }

        myService.onMAPDisconnected = function (err, data) {
            if (err) {
                myService.logInfoNError("error", "Platform disconnected, err=" + err);
                myService.ReloadAfterMapServiceFails();
            }
        }

        myService.logInfoNError = function (type, message) {
            console.log(type + " " + message);
            if (!myService.devMode) {
                if (type == "error")
                    logger.error(message);
                else
                    logger.info(message);
            }
        }

        myService.initialize();
    }]);