/**
 * Creates a directive to use the slidetoggle jquery plugin when price details are to be shown
 */

angular.
module('services').
directive("matchHeight", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            (function() {

                $(function() {
                    // apply your matchHeight on DOM ready (they will be automatically re-applied on load or resize)

                    // get test settings
                    var byRow = $('div').hasClass('test-rows');

                    // apply matchHeight to each item container's items
                    $('.items-container').each(function() {
                        $(this).children('.service-item-block').matchHeight({
                            byRow: byRow
                        });
                    });

                });

            })();
        }
    }
});