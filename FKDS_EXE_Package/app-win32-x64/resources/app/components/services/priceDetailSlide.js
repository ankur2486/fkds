/**
 * Creates a directive to use the slidetoggle jquery plugin when price details are to be shown
 */
angular.
module('services').
directive("priceDetails", ["$interval", function($interval) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            //On click
            $(elem).click(function() {
                $(attrs.priceDetails).slideToggle("slow");
            });
        }
    }
}]);