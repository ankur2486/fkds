/**
 * Services component to load available services
 */
angular.
module('services', ['ngCookies']).
component('services', {
    templateUrl: 'components/services/services.html',
    controllerAs: "servicesCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
        function ServicesController($http, $cookies, InitIdle, $location, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "services";
            self.location_path = $location.path();
            self.services_list = [];

        //pull the customer name if one is stored
        self.customer_name = "Visitor";
        self.customer_first_name = "";
            if ($cookies.customer_dictionary) {
            self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
            self.customer_first_name = $cookies.customer_dictionary['first_name'];
        }

            if (self.customer_first_name != "") {
            //pull the service choices from the database
            $http.get('http://localhost:5000/services?where={"name_filter": "'+self.customer_first_name+'"}').then(function(response) {
                self.services = response.data['_items'];

                    for (var i = 0; i < self.services.length; i++) {
                    self.services_list.push(self.services[i]['_id']);
                }

                    if (self.services.length == 0) {
                    //pull the service choices from the database
                    $http.get('http://localhost:5000/services').then(function(response) {
                        self.services = response.data['_items'];
                        setTimeout(function() {
                            $.fn.matchHeight._update();
                        }, 1);
                    }).catch(function(err) {
                        throw err;
                    });
                }
                else {
                    setTimeout(function() {
                        $.fn.matchHeight._update();
                    }, 1);
                }

            }).catch(function(err) {
                throw err;
            });
        }

        if($cookies.vehicle != null) {
            self.vehicle = $cookies.vehicle;
            self.vehicle_name = self.vehicle.year + " " + self.vehicle.make + " " + self.vehicle.model;
            self.vin = self.vehicle.vin;
            self.mileage = self.vehicle.mileage;
        }
        else {
            self.vehicle_name = "No Vehicle Found"
        }

            $('body').addClass('app-body-inner-bg');

            //pull the service choices from the database
            $http.get('http://localhost:5000/services').then(function (response) {
                self.services = response.data['_items'];
            }).catch(function (err) {
                throw err;
            });

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
            }

            //pull the chosen services if any are stored
            if ($cookies.services_chosen) {
                self.services_list = $cookies.services_chosen;
            }

            //checkbox function to see if a service is in the chosen list
            self.checkboxStatus = function (service) {
                return (self.services_list.indexOf(service) !== -1);
            };

            //add service to the list or remove it if it is already present
            self.update_choices = function (id) {
                if (self.services_list.includes(id)) {
                    var index = self.services_list.indexOf(id);
                    self.services_list.splice(index, 1);
                }
                else {
                    self.services_list.push(id);
                }
            };

        self.save_choices = function () {
            MyService.addPageTimer();
            //$cookies.services_chosen = self.services_list;
            MyService.reportScreen("AddRecommandedServicesPage", "User clicked on next button on the confirm requested services page.");

            //log the page history to the appointment
                if ($cookies.session_id) {
               session_id = $cookies.session_id;
               $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                   logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                        logging_array.push({
                            "access_date": current_datetime,
                       "page_name": self.page_name,
                            "url_path": self.location_path
                        });
                    $http.patch('http://localhost:5000/appointments/'+session_id, {
                        logging_list: logging_array,
                        services_confirmed: self.services_list
                    }).then(function(response){
                        //success
                    }).catch(function(err) {
                        throw err;
                    });
               }).catch(function(err) {
                   throw err;
               });
            }
        };
    }]
});
