/**
 * License Confirm component to load confirmation page
 */
angular.
module('licenseConfirm', ['ngCookies']).
component('licenseconfirm', {
    templateUrl: 'components/license-confirm/license-confirm.html',
    controllerAs: "licenseConfirmCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter','MyService',
        function LicenseConfirmController($cookies, InitIdle, $location, $http, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "license_confirm";
        self.location_path = $location.path();
      
        self.dummyCustomer = $cookies.dummyLicense;

        MyService.reportScreen("LicenseConfirmPage", "License scanned successfully.");

        self.change_page = function (scope) {
            $cookies.customer_dictionary = self.dummyCustomer;
            
            if (scope == 'licenseConfirm')
                MyService.reportScreen("more-info", "User clicked on correct button on the license confirm page.");
            else if (scope == 'licenseScan')
                MyService.reportScreen("license-scan", "User clicked on rescan button on the license confirm page.");

            //log the page history to the appointment
            if ($cookies.session_id)
            {
               session_id = $cookies.session_id;
               $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                   logging_array = response.data['logging_list'];

                   var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                   current_datetime.toString();
                   current_datetime += ' GMT';

                   logging_array.push({"access_date": current_datetime,
                       "page_name": self.page_name,
                       "url_path": self.location_path});
                    $http.patch('http://localhost:5000/appointments/'+session_id, {
                        logging_list: logging_array,
                        customer: self.dummyCustomer,
                        license_status: "scanned"
                    }).then(function(response){
                        //success
                    }).catch(function(err) {
                        MyService.logInfoNError("error", "Error while saving customer & license_status");                       
                        throw err;
                    });
               }).catch(function(err) {
                   throw err;
               });
            }
        }
    }]
});
