/**
 * Payment Summary component to load payment summary
 */
angular.
module('paymentSummary', ['ngCookies', 'ngMaterial']).
component('paymentsummary', {
    templateUrl: 'components/payment-summary/payment-summary.html',
    controllerAs: "paymentSummaryCtrl",
    controller: ['$http', '$cookies', '$mdDialog', 'initIdle', '$location', '$filter', 'MyService',
        function PaymentSummaryController($http, $cookies, $mdDialog, InitIdle, $location, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "payment_summary";
            self.location_path = $location.path();
            self.iscardReaderServiceAvailable = MyService.cardReaderServiceEnabled;

            $('body').addClass('app-body-inner-bg');

            if (!self.iscardReaderServiceAvailable)
                MyService.showPopup("#errorPaymentServicePopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));

            //Data initialization is temporary til integration with hardware
            self.customerDict = {
                "first_name": "Test",
                "last_name": "Customer",
                "phone": "123-123-1234",
                "email": "test@gomoto.com",
                "street": "123 Street Road",
                "city": "Nowhere",
                "state": "WY",
                "zipcode": "12345",
                "pin": "1234"
            };

            //pull the customer if one is stored
            self.customer_email = "";
            if ($cookies.customer_dictionary) {
                self.customerDict = $cookies.customer_dictionary;

                if (self.customerDict['email'])
                {
                    self.customer_email = self.customerDict['email'];
                }
            }

            //pull the chosen services if any are stored
            self.services_list = [];
            if ($cookies.services_chosen) {
                self.services_list = $cookies.services_chosen;
            }

            //pull the chosen manual services if any are stored
            self.manual_services_list = [];
            if ($cookies.manual_services_chosen) {
                self.manual_services_list = $cookies.manual_services_chosen;
            }

            if($cookies.vehicle != null) {
                self.vehicle = $cookies.vehicle;
                self.vehicle_name = self.vehicle.year + " " + self.vehicle.make + " " + self.vehicle.model;
                self.vin = self.vehicle.vin;
                self.mileage = self.vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            self.send_receipt_email = function send_receipt_email(result){
                var to = result;
                var from = 'Contact <keydropalert@gomoto.org>';
                var subject = "Payment Summary";
                var html_message = "This is your generic keydrop email.";

                //grab the session information from the database
                if ($cookies.session_id)
                {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                        logging_array = response.data['logging_list'];

                        var current_date = $filter('date')(new Date(), 'EEE, dd MMM yyyy');
                        var current_time = $filter('date')(new Date(), 'h:mm:ss a');
                        // console.log(current_date);
                        // console.log(current_time);

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                        logging_array.push({"access_date": current_datetime,
                            "page_name": self.page_name,
                            "url_path": self.location_path});

                        $http.patch('http://localhost:5000/appointments/'+session_id, {
                            logging_list: logging_array,
                            appt_comment: self.appointment_comment
                        }).then(function(response){
                            //success
                        }).catch(function(err) {
                            console.log(err);
                            throw err;
                        });

                        //customer object
                        var customer = response.data['customer'];
                        var customer_name = "No Customer Found";
                        var customer_first_name = "";
                        if ( customer )
                        {
                            customer_name = customer['first_name'] + " " + customer['last_name'];
                            customer_first_name = customer['first_name'];
                        }

                        //vehicle object
                        var vehicle = response.data['vehicle'];
                        var vehicle_string = "No Vehicle Found";
                        if (vehicle)
                        {
                            vehicle_string = vehicle['year'] + ' ' + vehicle['make'] + ' ' + vehicle['model'];
                            if (vehicle['series'])
                            {
                                vehicle_string += ' ' + vehicle['series'];
                            }

                        }

                        //send the email
                        $http.get('components/email/payment_email.html').then(function(response) {

                            var template = _.template(response.data)(
                                {name:customer_name,
                                first_name:customer_first_name,
                                vehicle:vehicle_string,
                                date:current_date,
                                time:current_time});

                            html_message = template;
                            sendMail(to, from, subject, html_message);

                        }).catch(function(err) {
                            throw err;
                        });


                    }).catch(function(err) {
                        throw err;
                    });
                }
            };

            //self.customer_email = "mjones@gomoto.com";
            //send receipt email for this session
            if (self.customer_email != "")
            {
                //self.send_receipt_email(self.customer_email);
            }

            self.services = [];
            self.service_total = 0;//default set

            //loop through the chosen services and calculate the total price for the services
            for (var i = 0; i < self.services_list.length; i++) {
                $http.get('http://localhost:5000/services/' + self.services_list[i]).then(function (response) {
                    self.services.push(response.data);
                    self.service_total += response.data['price'];
                }).catch(function (err) {
                    throw err;
                });
            }

            //get the assigned service_total from the database
            $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                self.service_total = response['data']['service_total'];
            }).catch(function(err) {
                throw err;
            });

            self.change_page = function () {
                 MyService.addPageTimer();
                MyService.reportScreen("PaymentSummaryPage", "User clicked on pay now button on the payment summary page.");
                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push({"access_date": current_datetime,
                           "page_name": self.page_name,
                           "url_path": self.location_path});

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           console.log(err);
                           throw err;
                       });

                   }).catch(function(err) {
                       throw err;
                   });
                }
            }
        }]
});
