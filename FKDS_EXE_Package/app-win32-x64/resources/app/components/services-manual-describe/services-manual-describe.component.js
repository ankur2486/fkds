/**
 * Services Manual Describe component to load chosen manual services for comments
 */
angular.
module('servicesManualDescribe', ['ngCookies']).
component('servicesmanualdescribe', {
    templateUrl: 'components/services-manual-describe/services-manual-describe.html',
    controllerAs: "servicesManualDescribeCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
        function ServicesManualDescribeController($http, $cookies, InitIdle, $location, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "services_manual_describe";
            self.location_path = $location.path();

            //scrollbar configuration
            self.config = {
                autoHideScrollbar: false,
                theme: 'light',
                advanced: {
                    updateOnContentResize: true
                },
                setHeight: 400,
                scrollInertia: 0,
                axis: 'y',
            };

            //pull the chosen services if any are stored
            if ($cookies.manual_services_chosen) {
                self.chosen_manual_services_list = $cookies.manual_services_chosen;
            }

            self.manual_service_comment_list = {};

            //pre-fill the comment dictionary
            for (var i = 0; i < self.chosen_manual_services_list.length; i++) {
                self.manual_service_comment_list[self.chosen_manual_services_list[i]] = "";
            }

            //pull the chosen service comments if any are stored
            if ($cookies.manual_services_comments) {
                self.manual_service_comment_list = $cookies.manual_services_comments;
            }

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
            }

            if ($cookies.vehicle != null) {
                var vehicle = $cookies.vehicle;
                self.vehicle_name = vehicle.year + " " + vehicle.make + " " + vehicle.model;
                self.vin = vehicle.vin;
                self.mileage = vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //save the comment for the specific service
            self.update_choices = function (name, text) {
                self.manual_service_comment_list[name] = text;
            };
            self.backToService = function () {
                MyService.addPageTimer();
                MyService.reportScreen("services-manual", "User clicked on back button on the services manual describe page.");
                $location.path('/services-manual')
            };
            self.save_choices = function () {
                $cookies.manual_services_comments = self.manual_service_comment_list;
                MyService.addPageTimer();
                MyService.reportScreen("ServicesManualConfirmPage", "User clicked on next button on the services manual describe page.");
                var manual_services = [];
                for (var key in self.manual_service_comment_list) {
                    if (self.manual_service_comment_list.hasOwnProperty(key)) {
                        manual_services.push({
                            name: key,
                            description: self.manual_service_comment_list[key]
                        });
                    }

                }
                $location.path('/services-manual-confirm')
                //log the page history to the appointment
            if ($cookies.session_id)
            {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                   logging_array.push({"access_date": current_datetime,
                            "page_name": self.page_name,
                       "url_path": self.location_path});

                        $http.patch('http://localhost:5000/appointments/' + session_id, {
                            logging_list: logging_array,
                            manual_services: manual_services
                        }).then(function (response) {
                            //success
                        }).catch(function (err) {
                            console.log(err);
                            throw err;
                        });

                    }).catch(function (err) {
                        throw err;
                    });
                }
            };

        }]
});
