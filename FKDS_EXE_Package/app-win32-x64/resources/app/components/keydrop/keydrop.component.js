/**
 * Keydrop component to load keydrop page
 */
angular.
module('keydrop', ['ngCookies']).
component('keydrop', {
    templateUrl: 'components/keydrop/keydrop.html',
    controllerAs: "keydropCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService', '$scope',
        function KeydropController($cookies, InitIdle, $location, $http, $filter, MyService, $scope) {
            InitIdle.init();
            var self = this;
            self.page_name = "keydrop";
            self.location_path = $location.path();
            var keyStoreRequest = null;
            self.LockerStatusArray = null;
            self.KeyDropped = false;

            $('body').removeClass('app-body-inner-bg');

            self.change_page = function () {
                MyService.addPageTimer();
                MyService.reportScreen("keydrop-confirm", "User clicked on next button on the keydrop page.");
               
                //log the page history to the appointment
                if ($cookies.session_id) {
                    session_id = $cookies.session_id;
                        $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                            logging_array = response.data['logging_list'];

                            var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                            current_datetime.toString();
                            current_datetime += ' GMT';

                            logging_array.push({
                                "access_date": current_datetime,
                                "page_name": self.page_name,
                                "url_path": self.location_path
                            });

                            $http.patch('http://localhost:5000/appointments/' + session_id, {
                                logging_list: logging_array                      
                            }).then(function (response) {
                                //success
                            }).catch(function (err) {
                            MyService.logInfoNError("error", "Error while saving logging data " + err);                            
                                throw err;
                            });

                        }).catch(function (err) {
                            throw err;
                        });
                    }
                };
        }]
});
