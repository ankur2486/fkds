/**
 * Dashboard Customer Profile component to load customer profile
 */
angular.
module('dashboardCustomerProfile', ['ngCookies', 'ngMaterial']).
component('dashboardcustomerprofile', {
    templateUrl: 'components/dashboard-customer-profile/dashboard-customer-profile.html',
    controllerAs: "dashboardCustomerProfileCtrl",
    controller: ['$http', '$cookies', '$mdDialog', 'initIdle', '$location', '$filter','MyService',
        function DashboardCustomerProfileController($http, $cookies, $mdDialog, InitIdle, $location, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "dashboard_customer_profile";
        self.location_path = $location.path();

        //scrollbar configuration
        self.config = {
            autoHideScrollbar: false,
            theme: 'light',
            advanced: {
                updateOnContentResize: true
            },
            setHeight: 1200,
            scrollInertia: 0,
            axis: 'y',
        };

            if ($cookies.key_drop_appointment_id) {
            self.session_id = $cookies.key_drop_appointment_id;
        }

        if (self.session_id) {
            // self.dms_service_dict = {};
            // $http.get('http://localhost:5000/services/?max_results=60').then(function (response) {
            //     for (var i = 0; i < response['data']['_items'].length; i++) {
            //         self.dms_service_dict[response['data']['_items'][i]['_id']] = response['data']['_items'][i];
            //     }
            // }).catch(function (err) {
            //     throw err;
            // });
            //
            // self.upsell_service_dict = {};
            // $http.get('http://localhost:5000/upsell_services/?max_results=60').then(function (response) {
            //     for (var i = 0; i < response['data']['_items'].length; i++) {
            //         self.upsell_service_dict[response['data']['_items'][i]['_id']] = response['data']['_items'][i];
            //     }
            // }).catch(function (err) {
            //     throw err;
            // });

            $http.get('http://localhost:5000/appointments/'+self.session_id).then(function (response) {
                self.appointment_object = response['data'];
            }).catch(function (err) {
                throw err;
            });
        }

        self.goBack = function () {
                MyService.addPageTimer();
            $location.path('/dashboard-summary');
                MyService.reportScreen("dashboard-summary", "User clicked on back button on dashboard page.");
        };

            self.change_page = function (data, page) {
                MyService.addPageTimer();
                if (page == "RETURNKEY")
                    MyService.reportScreen("admin-keydrop-form", "User clicked on  return key now button on the dashboard customer profile page.");
                else
                    MyService.reportScreen("admin-return-key", "User clicked on EY Manually Removed button on the dashboard customer profile page.");

            $cookies.key_drop_appointment_id = data;
        };

        self.add_customer_page = function () {
                MyService.addPageTimer();
                MyService.reportScreen("customer-form", "User clicked on CREATE NEW KEY DROP button on the dashboard page.");
            $cookies.admin_add_customer = true;
        };

    }]
});
