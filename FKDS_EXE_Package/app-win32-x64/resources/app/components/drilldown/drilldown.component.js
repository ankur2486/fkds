/**
 * Services component to load available services
 */
var _ = require("lodash");
angular.
module('drilldown', ['ngCookies', 'ngSanitize', 'ui.select']).
component('drilldown', {
    templateUrl: 'components/drilldown/drilldown.html',
    controllerAs: "drilldownCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
        function DrilldownController($http, $cookies, InitIdle, $location, $filter, MyService) {
        InitIdle.init();
        var self = this;
        var initialLoad = true;
        self.page_name = "drilldown";
        self.location_path = $location.path();
        if($cookies.vehicle != null) {
            var vehicle = $cookies.vehicle;
            self.vin = vehicle.vin;
            self.mileage = vehicle.mileage;
        }

        $http.get('vehicle_dropdown.json').then(function(response) {
            self.years = response.data;

            if($cookies.vehicle != null) {
                    var vehicle = $cookies.vehicle;
                    var index = findIndex(self.years, vehicle.year);

                    if (index >= 0) {
                        self.year = {};
                        self.year.selected = self.years[index];
                        self.updateMakes(self.year.selected);
                    }
                }
                initialLoad = false;
            }).catch(function (err) {
                throw err;
            });

            this.updateMakes = function (year) {
                self.makes = year.make_list;

                self.make = {};
                self.model = {};
                self.series = {};
                if ($cookies.vehicle != null && initialLoad) {
                    var vehicle = $cookies.vehicle;
                    var index = findIndex(self.makes, vehicle.make);

                    if (index >= 0) {
                        self.make = {};
                        self.make.selected = self.makes[index];
                        self.updateModels(self.make.selected);
                    }
                }
            };

            this.updateModels = function (make) {
                self.models = make.model_list;

                self.model = {};
                self.series = {};
                if ($cookies.vehicle != null && initialLoad) {
                    var vehicle = $cookies.vehicle;
                    var index = findIndex(self.models, vehicle.model);

                    if (index >= 0) {
                        self.model = {};
                        self.model.selected = self.models[index];
                        self.updateSeries(self.model.selected);
                    }
                }
            };

            this.updateSeries = function (model) {
                self.seriesArray = model.series_list;

                self.series = {};
                if ($cookies.vehicle != null && initialLoad) {
                    var vehicle = $cookies.vehicle;
                    var index = -1;
                    if (vehicle.series != null) {
                        index = findIndex(self.seriesArray, vehicle.series);
                    }

                    if (index >= 0) {
                        self.series = {};
                        self.series.selected = self.seriesArray[index];
                    }
                }
            };

            this.backTohome = function () {
                MyService.addPageTimer();
                MyService.clearSession("HomePage", "Session ended,User clicked on back button on the Vehicle information page.");
            }

            this.submit = function () {
                MyService.addPageTimer();
                var data = {
                    year: self.year.selected.name,
                    make: self.make.selected.name,
                    model: self.model.selected.name,
                    vin: self.vin,
                    mileage: self.mileage
                };
                if (self.series != null && self.series.selected != null) {
                    data['series'] = self.series.selected.name;
                }
                $cookies.vehicle = data;
               
                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push(
                           {"access_date": current_datetime,
                               "page_name": self.page_name,
                               "url_path": self.location_path});

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array,
                           vehicle: data
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           MyService.logInfoNError("error", "Error while saving vehical data " + err);                          
                           throw err;
                       });

                   }).catch(function(err) {
                       throw err;
                   });
                }

                if ($cookies.admin_add_customer)
                {
                    MyService.reportScreen("admin-keydrop-form", "User clicked on submit button on the Vehicle information page.");
                    $cookies.key_drop_appointment_id = $cookies.session_id;
                    $location.path('/admin-keydrop-form');
                }
                else
                {
                    MyService.reportScreen("services-manual", "User clicked on submit button on the Vehicle information page.");
                    $location.path('/services-manual');
                }
            };

            function findIndex(arrayToSearch, searchedName) {
                var index = -1;
                _.find(arrayToSearch, function (item, labelIdx) {
                    if (item.name == searchedName) {
                        index = labelIdx;
                        return true;
                    }
                });
                return index;
            }
        }]
});
