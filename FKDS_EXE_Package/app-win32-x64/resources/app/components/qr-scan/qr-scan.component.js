/**
 * QR Scan component to load QR scan page
 */
angular.
module('qrScan', ['ngCookies']).
component('qrscan', {
    templateUrl: 'components/qr-scan/qr-scan.html',
    controllerAs: "qrScanCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService', '$scope',
        function QRScanController($cookies, InitIdle, $location, $http, $filter, MyService, $scope) {
            InitIdle.init();
            var self = this;
            self.page_name = "qr_scan";
            self.location_path = $location.path();
            self.scannedBarcode = "";
            self.isScanned = true;
            var mzRequestScanCode = null;
            self.devMode = MyService.devMode;
            self.mzBarcodeEndInProgress = false;
            self.popupMessage = '';

            $('body').removeClass('app-body-inner-bg');
            MyService.reportScreen("QRScanPage", "User wants to pick up key.");
            MyService.addPageTimer();

            self.goBack = function () {
                self.mzeroBarcodeReadEnd();
                MyService.clearSession("HomePage", "Session ended, User clicked on back button on qr scan page.");
            };

            self.change_page = function () {
                MyService.addPageTimer();
                self.mzeroBarcodeReadEnd();

                ////check if qrcode matches to any customers
                $http.get('http://localhost:5000/appointments/' + self.scannedBarcode).then(function (response) {
                    $cookies.session_id = self.scannedBarcode;
                    $cookies.customer_dictionary = response['data']['customer'];
                    $cookies.vehicle = response['data']['vehicle'];
                    $cookies.manual_services_chosen = response['data']['manual_services'];

                    //this appointment is completed, go home
                    if (response['data']['appt_status'] == 'completed') {
                        // show popup and redirect to home after popup close.
                        self.popupMessage = "Your appointment is completed."
                        MyService.showPopup("#messagePopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                        return;
                    }

                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                        logging_array.push({
                            "access_date": current_datetime,
                            "page_name": self.page_name,
                            "url_path": self.location_path
                        });

                    $http.patch('http://localhost:5000/appointments/' + self.scannedBarcode, {
                        	qr_status: "scanned",
                            logging_list: logging_array
                        }).then(function (response) {
                            //success
                        }).catch(function (err) {
                        MyService.logInfoNError("error", "Error while saving qr_status " + err);
                            throw err;
                        });

                    if (response.data['admin_created']) {
                        if (response.data['payment_status'] == 'paid') {
                            MyService.reportScreen("keydrop-pickup", "Admin created appointment with paid status.");
                            $location.path('/keydrop-pickup');
                            return;
                        }
                        else {
                            MyService.reportScreen("payment-scan", "Admin created appointment with unpaid status.");
                            $location.path('/payment-scan');
                            return;
                        }
                    }
                    else {
                        MyService.reportScreen("pin-page", "Scanned barcode.");
                        $location.path('/pin-page');
                    }
                    }).catch(function (err) {
                    //no qr match found // show popup
                    MyService.logInfoNError("info", "There is no appointment associated with this QR code " + err);
                    self.popupMessage = "There is no appointment associated with this QR code."
                    MyService.showPopup("#messagePopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                        throw err;
                    });
                //END REMOVE


            };

            $("#sessionPopup").on('hidden.bs.modal', function () {
                MyService.clearSession("HomePage", "QR Scan timeout.");
            });

            $("#qrScanErrorPopup").on('hidden.bs.modal', function () {
                MyService.clearSession("HomePage", "QR Scan timeout.");
            });

            $("#qrScanProgressPopup").on('hidden.bs.modal', function () {               
                self.change_page();
            });

            $("#messagePopup").on('hidden.bs.modal', function () {
                MyService.clearSession("HomePage", self.popupMessage);
            });

            self.mzeroBarcodeReadStart = function () {

                if (!MyService.devMode) {
                    mzRequestScanCode = mzero.create.scanner.scanSymbol();
                    var eventHandlers = {
                        Done: function (err, data) {
                            self.mzBarcodeEndInProgress = false;
                            mzRequestScanCode = null;
                            MyService.addPageTimer();
                            MyService.hideWaiting();
                            if (err) {
                                MyService.logInfoNError("error", "Error in mzeroBarcodeReadStart done " + err);
                                if (err.toLowerCase().indexOf('timeout') > -1) {
                                    MyService.showPopup("#sessionPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));

                                }
                                else if (err.toLowerCase().indexOf('cancelled') > -1) {
                                }
                                else if (err.toLowerCase().indexOf('serviceinuse') > -1) {
                                    MyService.showPopup("#qrScanErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                                }
                                else {
                                    console.log("Error activating barcode scanner");
                                    console.log(err);
                                    MyService.showPopup("#qrScanErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                                }
                            } else if (data) {
                                MyService.logInfoNError("info", " Done Event of mzeroBarcodeReadStart" + data);
                                self.isScanned = false;
                                $('.manual-service-action a').removeAttr("disabled");
                                MyService.showPopup("#qrScanProgressPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                                self.scannedBarcode = data.QRCode;
                                //Not need to write any update query. Added inside change_page function and called after  qrScanProgressPopup popup closed. Please refer     $("#qrScanProgressPopup").on('hidden.bs.modal', function () {                     
                            }
                        }
                    };

                    mzRequestScanCode.start(eventHandlers);
                    MyService.logInfoNError("info", " Starting barcode scanner.");
                    MyService.reportScreen("Scanner", "Scanner started");
                }
            };

            self.mzeroBarcodeReadEnd = function () {

                if (!MyService.devMode) {

                    if (self.mzBarcodeEndInProgress)
                        return;

                    if (mzRequestScanCode) {
                        self.mzBarcodeEndInProgress = true;
                        mzRequestScanCode.end();
                        MyService.logInfoNError("info", "Barcode Scanner Stopped.");
                        console.log('Scanner Stopped');
                        MyService.reportScreen("Scanner", "Scanner Stopped");
                    }
                }
            };

            if (!MyService.devMode)
                self.mzeroBarcodeReadStart();

            $scope.$on('$destroy', function () {
                self.mzeroBarcodeReadEnd();
            });


            // DevMode 
            self.errorQRScan = function () {
                MyService.showPopup("#qrScanErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
            }

            self.scanQR = function () {
                self.isScanned = false;
                $('.manual-service-action a').removeAttr("disabled");
                MyService.addPageTimer();
                MyService.showPopup("#qrScanProgressPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                self.scannedBarcode = "57ee68addf9db01e985a2366";
            }

        }]
});
