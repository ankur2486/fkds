/**
 * Appointment Comment component to load field for comments
 */
angular.
module('appointmentComment', ['ngCookies']).
component('appointmentcomment', {
    templateUrl: 'components/appointment-comment/appointment-comment.html',
    controllerAs: "appointmentCommentCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
    function AppointmentCommentController($http, $cookies, InitIdle, $location, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "appointment_comment";
        self.location_path = $location.path();
        self.appointment_comment = "";
        //pull the chosen appointment comments if any are stored
        if ($cookies.appointment_comments)
        {
            self.appointment_comment = $cookies.appointment_comments;
        }

        //pull the customer name if one is stored
        self.customer_name = "Visitor";
        if ($cookies.customer_dictionary)
        {
            self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
        }

        if($cookies.vehicle != null) {
            self.vehicle = $cookies.vehicle;
            self.vehicle_name = self.vehicle.year + " " + self.vehicle.make + " " + self.vehicle.model;
            self.vin = self.vehicle.vin;
            self.mileage = self.vehicle.mileage;
        }
        else {
            self.vehicle_name = "No Vehicle Found"
        }

        self.backToService = function () {
            MyService.reportScreen("services-upsell", "User clicked on back button on the appointment comment page.");
            $location.path('/services-upsell');
        };

        self.save_choices = function () {
            MyService.addPageTimer();
            $cookies.appointment_comments = self.appointment_comment;           

            //log the page history to the appointment
            if ($cookies.session_id) {
                session_id = $cookies.session_id;
                $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                    logging_array = response.data['logging_list'];

                    var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                    current_datetime.toString();
                    current_datetime += ' GMT';

                    logging_array.push({
                        "access_date": current_datetime,
                        "page_name": self.page_name,
                        "url_path": self.location_path
                    });

                    $http.patch('http://localhost:5000/appointments/' + session_id, {
                        logging_list: logging_array,
                        appt_comment: self.appointment_comment
                    }).then(function (response) {
                        //success
                    }).catch(function (err) {
                        MyService.logInfoNError("error", "Error while saving appointment_comment " + err);                       
                        throw err;
                    });

                }).catch(function (err) {
                    throw err;
                });
            }
            MyService.reportScreen("services-tally", "User clicked on next button on the appointment comment page.");
            $location.path('/services-tally')
        };
    }]
});
