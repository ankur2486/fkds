/**
 * Ford Log In component to load log in page
 */
angular.
module('licenseOrManual', ['ngCookies']).
component('licenseormanual', {
    templateUrl: 'components/license-or-manual/license-or-manual.html',
    controllerAs: "licenseOrManualCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
        function LicenseOrManualController($cookies, InitIdle, $location, $http, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "license_or_manual";
        self.location_path = $location.path();
        self.isScannerActive = MyService.isScannerActive;

        if (!self.isScannerActive && !MyService.isShown) {
            MyService.showPopup("#errorLicenseServicePopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
            MyService.isShown = true;
        }

        self.backTohome = function () {
                MyService.addPageTimer();
            MyService.reportScreen("HomePage", "User clicked on back button on the license or manual page.");
        };
        self.change_page = function (data) {
             MyService.addPageTimer();
            if (data == 'scanLicense')
                MyService.reportScreen("LicenseScanPage", "User clicked on scan license link on the license or manual page.");
            else if (data == 'manualEntry')
                MyService.reportScreen("CustomerFormPage", "User clicked on manual entry link on the license or manual page.");

            ////log the page history to the appointment
                if ($cookies.session_id) {
               session_id = $cookies.session_id;
               $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                   logging_array = response.data['logging_list'];

                   var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                   current_datetime.toString();
                   current_datetime += ' GMT';

                        logging_array.push({
                            "access_date": current_datetime,
                                    "page_name": self.page_name,
                            "url_path": self.location_path
                        });

                   $http.patch('http://localhost:5000/appointments/'+session_id, {
                       logging_list: logging_array
                   }).then(function(response){
                       //success
                   }).catch(function(err) {
                       console.log(err);
                       throw err;
                   });

               }).catch(function(err) {
                   throw err;
               });
            }
        }
    }]
});
