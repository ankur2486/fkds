
var nodemailer = require('nodemailer');
// var smtpTransport = require('nodemailer/node_modules/nodemailer-smtp-transport');

var transporter;

function setTransporter() {
    $.getJSON("EmailAuth.json", function (data) {
        // grabs credentials from EmailAuth.json to set up the mail transporter
        transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: data.user,
                pass: data.pass
            }
        });

    });
}

function sendMail(to, from, subject, html) {
    transporter.sendMail({
        from: from,
        to: to,
        subject: subject,
        html: html
        
    }, function (error, response) {
        //Email not sent
        if (error) {
            console.log(error);
            //res.end("Email send Falied");
        }
            //email send sucessfully
        else {
            console.log(response);
        }
    });
}

function sendMailWithAttachment(to, from, subject, html, imageData, ImageName) {
   
    transporter.sendMail({
        from: from,
        to: to,
        subject: subject,
        html: html,
        attachments: [
            {   // encoded string as an attachment
                filename: ImageName,
                content: imageData,
                encoding: 'base64',
                cid: 'myimagecid'
            }
        ]
    }, function (error, response) {
        //Email not sent
        if (error) {
            console.log(error);
            //res.end("Email send Falied");
        }
            //email send sucessfully
        else {
            console.log(response);
        }
    });
}