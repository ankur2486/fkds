/**
 * GomotoKeydrop
 * Acts as the interface for several other technologies such as mzero to build a car rental kiosk where one can retrieve
 * and deposit the keys of rental cars.
 */
const electron = require('electron');
// Module to control application life.
const {app} = electron;
// Module to create native browser window.
const {BrowserWindow} = electron;
// Keep a global reference of the window object to avoid the window getting garbage collected
let win;
// Boot up the error logging emailer
require("./errorLogging");
const DEBUG = false;

/**
 * Creates the main window of the application, loading the first page
 */
function createWindow() {
    win = new BrowserWindow({width: 1080, height: 1920});
    win.loadURL(`file://${__dirname}/index.html`);
    win.setMenuBarVisibility(false);

    if(DEBUG) {
        win.webContents.openDevTools();
    }
    else {
        win.setFullScreen(true);
    }

    win.on('closed', () => {
        win = null;
});
}

// Create the main window when electron is done initializing
app.on('ready', createWindow);

app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
}
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow();
}
});