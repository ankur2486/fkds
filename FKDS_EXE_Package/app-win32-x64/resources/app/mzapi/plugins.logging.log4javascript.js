// mzero.plugins.logging.log4javascript.js
if(!mzero.plugins) mzero.plugins = { };
mzero.plugins.logging = (function() {
	function mixProperties(defaultVals, newVals) {
		var result = {};
		if(defaultVals) Object.getOwnPropertyNames(defaultVals).forEach(function(elem, index, array) {
			result[elem] = defaultVals[elem];
		});
		if(newVals) Object.getOwnPropertyNames(newVals).forEach(function(elem, index, array) {
			result[elem] = newVals[elem];
		});
		return result;
	}

	/**
	 * Simple configurable access to the logging functionality provided by the logging plugin
	 * @name mzero.plugins.logging.LoggingOptions
	 * @property {boolean|mzero.plugins.logging.AjaxLoggingOptions} Ajax - Optional: settings for configuring an Ajax appender. If <code>true</code> or <code>null</code> are used, the plugin default values are accepted; if an object is passed, its properties override the plugin defaults
	 * @property {boolean|mzero.plugins.logging.ConsoleLoggingOptions} Console - Optional: settings for configuring an appender that uses the browser console. If <code>true</code> or <code>null</code> are used, the plugin default values are accepted; if an object is passed, its properties override the plugin defaults
	 * @property {boolean|mzero.plugins.logging.PopUpLoggingOptions} PopUp - Optional: settings for configuring an appender that uses a pop up window. If <code>true</code> or <code>null</code> are used, the plugin default values are accepted; if an object is passed, its properties override the plugin defaults
	 * @class
	 * @see mzero.create.InitializationOptions
	 */

	/**
	 * Console logging uses the browser console
	 * @name mzero.plugins.logging.ConsoleLoggingOptions
	 * @class
	 * @see mzero.plugins.logging.LoggingOptions
	 */
	var ConsoleDefaults = {
	};

	/**
	 * Ajax logging forwards logged messages to a platform service
	 * @name mzero.plugins.logging.AjaxLoggingOptions
	 * @property {string} URL - Address of the logging service
	 * @property {boolean} WithCredentials - Optional: enable this option when accessing the URL requires authentication to be forwarded during the Ajax call
	 * @property {boolean} SendAllOnUnload - Optional: apparently this option is obtrusive on WebKit browsers
	 * @property {boolean} WaitForResponse - Optional
	 * @class
	 * @see mzero.plugins.logging.LoggingOptions
	 */
	var AjaxDefaults = {
		URL: "logging/log4javascript",
		WithCredentials: false, // enable it when authentication needs to be forwarded
		SendAllOnUnload: true, // beware: apparently it's obtrusive on webkit browsers
		WaitForResponse: false
	};
	/**
	 * PopUp logging displays logged messages into a separate browser window
	 * @name mzero.plugins.logging.PopUpLoggingOptions
	 * @property {boolean} LazyInit - Optional: when enabled, the window will not be created until something is logged
	 * @property {boolean} InitiallyMinimized - Optional: when enabled, the window will be created minimized
	 * @property {boolean} UseDocumentWrite - 
	 * @property {int} Width - Pop up width
	 * @property {int} Height - Pop up height
	 * @class
	 * @see mzero.plugins.logging.LoggingOptions
	 */
	var PopUpDefaults = {
		LazyInit: false,
		InitiallyMinimized: false,
		UseDocumentWrite: true,
		Width: 600,
		Height: 400
	};

	function initialize(nullLogging, options) {
		log4javascript.logLog.setQuietMode(true);
		log4javascript.logLog.setAlertAllErrors(false);
	}
	/**
	 * Configure the specified log4javscript logger using the platform configuration options
	 * @param {Object} - Logger; if it is log4javascript's null logger, it will not be modified
	 * @param {mzero.plugins.logging.LoggingOptions} - Logging options to apply; if null, no configuration will take place and a null logger will be used
	 */
	function configureLogger(logger, options) {
		if( (logger === log4javascript.getNullLogger()) || !options) {
			return;
		}
		_addAjaxAppender(logger, options.Ajax, true);
		_addPopUpAppender(logger, options.PopUp, false);
		_addConsoleAppender(logger, options.Console, true);
	}
	var _avoidAjaxConsoleErrors = true;
	var _ajaxURLStatus = {};
	function _addAjaxAppender(logger, opt, enableByDefault) {
		var isOptBoolean = (typeof opt == "boolean");
		if(isOptBoolean && !opt) {
			return;
		} else if(!opt && !enableByDefault) {
			return;
		}
		var options = mixProperties(AjaxDefaults, isOptBoolean? null : opt);
		if(_ajaxURLStatus[options.URL] == "checked" || !_avoidAjaxConsoleErrors) {
			_finishAddingAjaxAppender(logger, options);
		} else {
			var xhr = new XMLHttpRequest();
			xhr.onload = function(e) {
				if(xhr.status >= 200 && xhr.status < 300) {
					_ajaxURLStatus[options.URL] = "checked";
					_finishAddingAjaxAppender(logger, options);
				} else {
					console.warn("Ajax appender is disabled because no backend was found at this time");
				}
			};
			xhr.open("GET", options.URL);
			xhr.send();
		}
	}
	function _finishAddingAjaxAppender(logger, options) {
		var appender = new log4javascript.AjaxAppender(options.URL, options.WithCredentials);
		appender.setLayout(new log4javascript.JsonLayout());
		appender.addHeader("Content-Type", "application/json");
		appender.setWaitForResponse(options.WaitForResponse);
		//appender.setRequestSuccessCallback(requestCallback);
		appender.setFailCallback(function(message) { console.log("log4javascript ajax error: " + message); });
		appender.setSendAllOnUnload(options.SendAllOnUnload);
		logger.addAppender(appender);
	}
	function _addPopUpAppender(logger, opt, enableByDefault) {
		var isOptBoolean = (typeof opt == "boolean");
		if(isOptBoolean && !opt) {
			return;
		} else if(!opt && !enableByDefault) {
			return;
		}
		var options = mixProperties(PopUpDefaults, isOptBoolean? null : opt);
		var appender = new log4javascript.PopUpAppender(options.LazyInit, options.InitiallyMinimized, options.UseDocumentWrite, options.Width, options.Height);
		logger.addAppender(appender);
	}
	function _addConsoleAppender(logger, opt, enableByDefault) {
		var isOptBoolean = (typeof opt == "boolean");
		if(isOptBoolean && !opt) {
			return;
		} else if(!opt && !enableByDefault) {
			return;
		}
		var options = mixProperties(ConsoleDefaults, isOptBoolean? null : opt);
		var appender = new log4javascript.BrowserConsoleAppender();
		logger.addAppender(appender);
	}

	return {
		initialize: initialize
		, configureLogger: configureLogger
		, getNullLogger: log4javascript.getNullLogger
		, getNamedLogger: log4javascript.getLogger
	};
}());
