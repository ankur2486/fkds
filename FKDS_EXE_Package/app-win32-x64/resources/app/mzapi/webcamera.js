/**
 * Interface to the WebCamera service provided by the platform
 *
 * @namespace webcamera
 * @memberof mzero.create
 */
mzero.create.webcamera = (function() {
	var service = mzero.create.MAPEnv.createService("WebCamera");
	/**
	 * @lends mzero.create.webcamera
	 */
	var exports = {
		/**
		 * Represents a Request to capture an image.
		 * <p>
		 * Images are captured independently of the viewer state (hidden or displayed).
		 * </p>
		 *
		 * @name mzero.create.webcamera.CaptureImageRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.webcamera.CaptureImageResult
		 * @see mzero.create.webcamera.captureImage
		 */
		/**
		 * Data obtained on successful completion of {@link mzero.create.webcamera.CaptureImageRequest}
		 *
		 * @class mzero.create.webcamera.CaptureImageResult
		 * @property {String} Filename	Path to the stored image
		 * @see mzero.create.webcamera.CaptureImageRequest
		 * @see mzero.create.webcamera.captureImage
		 */
		
		/**
		 * Represents a request to capture an image and report it automatically to the management server
		 * associating it to a specific identifier
		 * @name mzero.create.webcamera.CaptureToReportImageRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.webcamera.captureToReport
		 */
		
		/**
		 * Represents a Request to display the viewer
		 * @name mzero.create.webcamera.ShowViewerRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.webcamera.showViewer
		 */
		/**
		 * Data obtained on successful completion of {@link mzero.create.webcamera.ShowViewerRequest}
		 *
		 * @class mzero.create.webcamera.ShowViewerResult
		 * @see mzero.create.webcamera.ShowViewerRequest
		 * @see mzero.create.webcamera.showViewer
		 */
		
		/**
		 * Represents a Request to hide the viewer
		 * @name mzero.create.webcamera.HideViewerRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.webcamera.hideViewer
		 */
		/**
		 * Data obtained on successful completion of {@link mzero.create.webcamera.HideViewerRequest}
		 *
		 * @class mzero.create.webcamera.HideViewerResult
		 * @see mzero.create.webcamera.HideViewerRequest
		 * @see mzero.create.webcamera.hideViewer
		 */
		
		/**
		 * @return {mzero.create.webcamera.CaptureImageRequest} Request ready to be started
		 * @see mzero.create.webcamera.CaptureImageResult
		 */
		captureImage: function() {
			var self = this;
			var request = service.createRequest("CaptureImage")
				.succeedsOn("CaptureImageSucceeded")
			;
			return request;
		},
		/**
		 * @return {mzero.create.webcamera.CaptureToReportImageRequest} Request ready to be started
		 */
		captureToReport: function(reportId) {
			var self = this;
			var request = service.createRequest("CaptureToReport", { ReportId: reportId })
				.succeedsOn("CaptureToReportSucceeded")
			;
			return request;
		},
		/**
		 * @return {mzero.create.webcamera.ShowViewerRequest} Request ready to be started
		 * @see mzero.create.webcamera.ShowViewerResult
		 */
		showViewer: function() {
			var self = this;
			var request = service.createRequest("ShowViewer")
				.succeedsOn("ShowViewerSucceeded")
			;
			return request;
		},
		/**
		 * @return {mzero.create.webcamera.HideViewerRequest} Request ready to be started
		 * @see mzero.create.webcamera.HideViewerResult
		 */
		hideViewer: function() {
			var self = this;
			var request = service.createRequest("HideViewer")
				.succeedsOn("HideViewerSucceeded")
			;
			return request;
		}
	};
	return exports;
})();
