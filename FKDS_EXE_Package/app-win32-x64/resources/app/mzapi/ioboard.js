/**
 * Interface to the I/O Board service provided by the platform
 *
 * @namespace ioboard
 * @memberof mzero.create
 */
mzero.create.ioboard = (function() {
	var relayService = mzero.create.MAPEnv.createService("RelayBoard");
	var inputService = mzero.create.MAPEnv.createService("InputBoard");
	
	function startPlatformMonitorSwitchRequest(switchName, eventHandlers) {
		var request = inputService.createRequest("ReceiveSwitchStates", { "SwitchName" : switchName }, {TagPersists: true})
			.withSignal("end", "CancelReceiveSwitchStates", 1)
			.firingEvents({ SwitchStateChanged: "SwitchState" })
			.succeedsOn("ReceiveSwitchStatesSucceeded")
			.start(eventHandlers);
		return request;
	}

	/**
	 * Represents a Request to monitor a switch using its configured name.
	 * @name MonitorSwitchRequest
	 * @class
	 * @memberof mzero.create.ioboard
	 * @extends mzero.create.Request
	 * @see mzero.create.ioboard.monitorSwitch
	 */
	/**
	 * Start the switch monitoring request, providing the callback handlers to be used.
	 *
	 * <p>
	 * <b>IMPORTANT:</b> In the existing implementation, the <tt>StateChange</tt> callback might be invoked very soon, <u>even before the <tt>start()</tt> call itself returns</u>.
	 * It is therefore better to avoid modifying objects right after the <tt>start()</tt> call if they are also modified in the <tt>SwitchState</tt> handler (otherwise you will
	 * find yourself debugging a non-existing problem).
	 * </p>
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.SwitchState {mzero.create.EventCallback} - Invoked as soon as the request starts (to notify you of the current state) and afterwards whenever the state changes.
	 *                                                                 The <tt>data</tt> parameter will be "On" or "Off" depending on the switch state, and "Unknown" if the state has not been determined yet.
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished
	 * @method
	 * @name mzero.create.ioboard.MonitorSwitchRequest#start
	 */
	/**
	 * Stops monitoring the switch
	 * @name end
	 * @method
	 * @instance
	 * @memberof mzero.create.ioboard.MonitorSwitchRequest
	 */

	/**
	 * Represents a Request to connect a relay to a specified position. If your application has an ongoing request on the same relay and a new one is issued, the old one will fail
	 * with an <code>Overriden</code> error code (see {@link mzero.create.ioboard.ConnectRelayRequest#start}, {@link mzero.create.ioboard.ProgramRelayPositionsRequest#start}).
	 * @name ConnectRelayRequest
	 * @class
	 * @memberof mzero.create.ioboard
	 * @extends mzero.create.Request
	 * @see mzero.create.ioboard.connectRelay
	 */
	/**
	 * Start the relay connection request, providing the callback handlers to be used.
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. The <code>err</code> values will be <code>Overriden</code> if the request finished because the client sent a newer connection or program before the current connection completed.
	 * @method
	 * @name mzero.create.ioboard.ConnectRelayRequest#start
	 */

	/**
	 * Describes how long a relay should hold a specific position
	 * @name ProgrammedRelayPosition
	 * @class
	 * @property {string} Name - Position name as specified in the platform configuration files
	 * @property {number} Duration - How long to hold the position (in milliseconds) before moving to the next one
	 * 
	 * @memberof mzero.create.ioboard
	 * @see mzero.create.ioboard.programRelay
	 */
	/**
	 * Represents a request to program a relay. If your application has an ongoing request on the same relay and a new one is issued, the old one will fail
	 * with an <code>Overriden</code> error code (see {@link mzero.create.ioboard.ConnectRelayRequest#start}, {@link mzero.create.ioboard.ProgramRelayPositionsRequest#start}).
	 *
	 * @name ProgramRelayPositionsRequest
	 * @class
	 * @memberof mzero.create.ioboard
	 * @see mzero.create.ioboard.programRelay
	 */
	/**
	 * Start the relay programming request, providing the callback handlers to be used.
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. The <code>err</code> values will be <code>Overriden</code> if the request finished because the client sent a newer connection or program before the current program completed.
	 * @method
	 * @name mzero.create.ioboard.ProgramRelayPositionsRequest#start
	 */

	// ts begins
	var SwitchState;
	(function (SwitchState) {
		SwitchState[SwitchState["Unknown"] = 0] = "Unknown";
		SwitchState[SwitchState["Off"] = 1] = "Off";
		SwitchState[SwitchState["On"] = 2] = "On";
	})(SwitchState || (SwitchState = {}));
	;

	var MonitorSetupPhase;
	(function (MonitorSetupPhase) {
		MonitorSetupPhase[MonitorSetupPhase["None"] = 0] = "None";
		MonitorSetupPhase[MonitorSetupPhase["UsingExisting"] = 1] = "UsingExisting";
		MonitorSetupPhase[MonitorSetupPhase["Activating"] = 2] = "Activating";
		MonitorSetupPhase[MonitorSetupPhase["Ready"] = 3] = "Ready";
		MonitorSetupPhase[MonitorSetupPhase["Deactivated"] = 4] = "Deactivated";
	})(MonitorSetupPhase || (MonitorSetupPhase = {}));
	;

	var MonitorSwitchRequest = (function () {
		function MonitorSwitchRequest(switchName) {
			this.started = false;
			this._logger = mzero.create.getLogger("MonitorSwitchRequest");
			this._setupPhase = 0 /* None */;
			this._switchName = switchName;
			this._switchState = this._previousSwitchState = 0 /* Unknown */;
			this._isFirstStatusChangeEvent = true;
			this._monitor = null;
		}
		MonitorSwitchRequest.prototype.start = function (eventHandlers) {
			this._eventHandlers = eventHandlers || {};
			this._monitor = monitoredSwitches.getInstanceFor(this._switchName);
			if (this._monitor.isActive()) {
				this.prepareToUseExistingMonitor();
			} else {
				this.prepareToWaitForMonitorSetUp();
			}
			this.started = true;
		};

		MonitorSwitchRequest.prototype.prepareToUseExistingMonitor = function () {
			// We only need to subscribe to its notification event
			this._setupPhase = 3 /* Ready */;
			var self = this;
			this._callbacks = {
	            StateChange: function (err, data) {
					self.fireStateChange(data);
	            }
        	};
	        this._monitor.startReceivingSwitchEvents(this);
				this.fireStateChange(this._monitor.currentSwitchState());
		};

		MonitorSwitchRequest.prototype.prepareToWaitForMonitorSetUp = function () {
			this._setupPhase = 2 /* Activating */;
			var self = this;
        	this._callbacks = {
				Connected: function (err, data) {
					self.completeSetup(err, data);
	            },
	            StateChange: function (err, data) {
					self.fireStateChange(data);
	            }
	        };
	        this._monitor.startReceivingSwitchEvents(this);
		};

		MonitorSwitchRequest.prototype.completeSetup = function (err, data) {
			if (err) {
				this._logger.info("Global switch monitor " + this._switchName + " could not be set up because of error=" + err);
				this._setupPhase = 4 /* Deactivated */;
				this.fireFinalEvent(err, null);
				return;
			}
			this._logger.info("Global switch monitor " + this._switchName + " is now available");
			this._setupPhase = 3 /* Ready */;
			this.fireStateChange(data);
		};

		MonitorSwitchRequest.prototype.fireStateChange = function (newState) {
			this._logger.debug("fireStateChange isfirst=" + this._isFirstStatusChangeEvent + " new=" + newState.toString() + " previous=" + this._previousSwitchState.toString());
			if (this._isFirstStatusChangeEvent || (newState != this._switchState)) {
				this._isFirstStatusChangeEvent = false;
				this._previousSwitchState = this._switchState;
				this._switchState = newState;
				var handler = this._eventHandlers["SwitchState"];
				if (handler) {
					handler(null, newState);
				}
			}
		};

		MonitorSwitchRequest.prototype.fireFinalEvent = function (err, data) {
			var handler = this._eventHandlers["Done"];
			if (handler) {
				handler(err, data);
			}
		};

	    MonitorSwitchRequest.prototype.end = function () {
	        if (this._setupPhase == 0 /* None */) {
	            this._logger.warn("Ignoring stopMonitor on request that has never been started (switch=" + this._switchName + ")");
	            return;
	        }
	        if (this._setupPhase == 4 /* Deactivated */) {
	            this._logger.warn("Ignoring stopMonitor on deactivated request (switch=" + this._switchName + ")");
	            return;
	        }
	        var phase = this._setupPhase;
	        this._setupPhase = 4 /* Deactivated */;
	
	        if (this._monitor) {
	            this._monitor.stopReceivingSwitchEvents(this);
	        }
	        if (phase == 2 /* Activating */) {
	            this.fireFinalEvent("Cancelled", null);
	        } else {
	            this.fireFinalEvent(null, null);
	        }
	    };
		return MonitorSwitchRequest;
	})();

	var MonitoredSwitchSet = (function () {
		function MonitoredSwitchSet() {
			this._instances = {};
		}
		MonitoredSwitchSet.prototype.getInstanceFor = function (switchName) {
			var canonicalSwitchName = switchName;
			var instance = this._instances[canonicalSwitchName];
			if (instance == null) {
            instance = new SharedSwitchMonitor(canonicalSwitchName);
				this._instances[canonicalSwitchName] = instance;
			}
			return instance;
		};

		MonitoredSwitchSet.prototype.removeMonitor = function (switchName) {
			delete this._instances[switchName]; // so that a future request retries the MAPLink connection from a clean slate (this will deactivate any bad callback we may have acquired)
		};
		return MonitoredSwitchSet;
	})();

	var monitoredSwitches = new MonitoredSwitchSet();

		/**
		 * The MAP service InputBoard service dispatches events to all interested parties but the messages have
		 * to flow through the STOMP broker on each notification. It is more efficient to provide a common
		 * entry at the API level
		 */
var SharedSwitchMonitor = (function () {
    /**
		 * @private
		 */
	    function SharedSwitchMonitor(canonicalSwitchName) {
				this._previousSwitchState = 0 /* Unknown */;
				this._currentSwitchState = 0 /* Unknown */;
		        this._objectsToCallBack = [];
		        this._logger = mzero.create.getLogger("SharedSwitchMonitor");
				this._canonicalSwitchName = canonicalSwitchName;
		}
	    SharedSwitchMonitor.prototype.isActive = function () {
				return (this._monitorSwitchRequest != null) && this._isSetUp;
		};
	
	    // This accessor is intended to be used only when initializing new MonitorSwitchRequest objects
	    SharedSwitchMonitor.prototype.currentSwitchState = function () {
				return this._currentSwitchState;
		};
	
	    SharedSwitchMonitor.prototype.startReceivingSwitchEvents = function (request) {
	        this._objectsToCallBack.push(request);
				if (this._monitorSwitchRequest == null) {
					// If no request already in progress, start a new one
					this.requestMapSwitchStates();
				}
		};
	
	    SharedSwitchMonitor.prototype.stopReceivingSwitchEvents = function (request) {
	        var found = false;
	        for (var i = 0; !found && (i < this._objectsToCallBack.length); i++) {
	            found = (request === this._objectsToCallBack[i]);
	        }
	        if (found) {
	            this._objectsToCallBack = this._objectsToCallBack.splice(i, 1);
	        }
	    };
	
	    SharedSwitchMonitor.prototype.requestMapSwitchStates = function () {
			this._isSetUp = false;
			var self = this;
			this._monitorSwitchRequest = startPlatformMonitorSwitchRequest(this._canonicalSwitchName, {
				SwitchState: function (err, data) {
					var state = data.State;
					if (self.isValidAndNew(state)) {
						self._logger.debug("New state=" + state + " in switch=" + self._canonicalSwitchName);
						self.fireStatusChange(state);
					}
				},
				Done: function (err, data) {
					if (err) {
						self.completeFailedSetUp(err);
					} else {
						self.completeSuccessfulSetUp(data);
					}
				}
			});
		};

	    SharedSwitchMonitor.prototype.isValidAndNew = function (switchState) {
			return (switchState != null) && (switchState != this._currentSwitchState);
		};
	
	    SharedSwitchMonitor.prototype.fireStatusChange = function (switchState) {
			this._previousSwitchState = this._currentSwitchState;
			this._currentSwitchState = switchState;
        	this._invokeCallbacks("StateChange", null, this._currentSwitchState);
		};

    	SharedSwitchMonitor.prototype.completeFailedSetUp = function (err) {
			this._logger.warn("Global switch monitor events for=" + this._canonicalSwitchName + " not set up because of error=" + err);
			if (this._isSetUp) {
				this._logger.error("Assumption violation: failed call after successful set up");
			}
        	this._invokeCallbacks("Connected", err, null);
			this._monitorSwitchRequest = null;
			this._isSetUp = false;
			monitoredSwitches.removeMonitor(this._canonicalSwitchName); // so that a future request retries the MAPLink connection from a clean slate (this will deactivate any bad listeners we may have acquired)
		};

    	SharedSwitchMonitor.prototype.completeSuccessfulSetUp = function (data) {
			this._logger.info("Global switch monitor is ready");
			if (this._isSetUp) {
				this._logger.error("Assumption violation: more success on a successfully set up monitor");
			}
			this._isSetUp = true;
			var state = data['State'];
			this.fireStatusChange(state); // dispatching to sharing listeners
		};

    	SharedSwitchMonitor.prototype._invokeCallbacks = function (callbackType, err, data) {
	        for (var i = 0; i < this._objectsToCallBack.length; i++) {
				var obj = this._objectsToCallBack[i];
				var callback = obj._callbacks[callbackType];
				if (callback) {
					callback(err, data);
				}
			}
		};
		return SharedSwitchMonitor;
	})();

	// ts ends

	/** @lends mzero.create.ioboard */
	var exports = {
		/**
		 * Monitor a switch identified by its name in the platform configuration files.
		 * @param {string} switchName - The name of the switch to monitor
		 * @return {mzero.create.ioboard.MonitorSwitchRequest} A request ready to be started
		 */
		monitorSwitch: function(switchName) {
			var result = new MonitorSwitchRequest(switchName);
			return result;
		},
		/**
		 * Connect a named relay to a symbolic position, leaving it there until further instructions.
		 * 
		 * @param {string} relayName - The name of the relay as specified in the platform configuration files
		 * @param {string} positionName - Name of the symbolic position as defined in the platform configuration files. Typical values are <tt>A</tt>,B<tt>A</tt>, etc.
		 * @return {mzero.create.ioboard.ConnectRelayRequest} A request ready to be started
		 */
		connectRelayPosition: function(relayName, positionName) {
			var request = relayService.createRequest("ConnectRelayPosition", { "RelayName" : relayName, "PositionName" : positionName })
				.succeedsOn("ConnectRelayPositionSucceeded")
			;
			return request;
		},
		/**
		 * Change a relay automatically between the given symbolic positions. Use this function when you know ahead of time how
		 * long to hold each position.
		 *
		 * @param {string} relayName - The name of the relay as specified in the platform configuration files
		 * @param {mzero.create.ioboard.ProgrammedRelayPosition[]} positions - The positions which the relay should take. They will be interpreted sequentially
		 * @return {mzero.create.ioboard.ProgramRelayPositionsRequest} A request ready to be started
		 */
		programRelay: function(relayName, positions) {
			var request = relayService.createRequest("ProgramRelayPositions", { "RelayName" : relayName, "Positions" : positions })
				.succeedsOn("ProgramRelayPositionsSucceeded")
			;
			return request;
		}
	};
	return exports;
})();
