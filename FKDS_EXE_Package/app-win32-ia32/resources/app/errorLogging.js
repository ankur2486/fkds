/**
 * Set up an admin email to send when exceptions aren't caught.
 */
var Winston = require('winston');
var emailAuth = require('./EmailAuth.json');
require('winston-email');

Winston.loggers.add('logger', {
    email: {
        from   : emailAuth.user,
        to     : emailAuth.adminEmails,
        service: 'Gmail',
        auth   : { user: emailAuth.user, pass: emailAuth.pass},
        handleExceptions: true,
        humanReadableUnhandledException: true
    }
});