/**
 * Holds various utility functions that get used throughout the application
 */
angular
    .module('keyDropApp')
    .service('Utils', ['$cookies', '$q', '$http', '$filter', function ($cookies, $q, $http, $filter) {
        function generateQRCode(data) {
            var typeNumber = 4;
            var errorCorrectionLevel = 'L';
            var qr = qrcode(typeNumber, errorCorrectionLevel);
            qr.addData(data);
            qr.make();
            return qr;
        }

        function qrToJpeg(qr) {
            var imgTag = qr.createImgTag();
            var binary = atob(imgTag.split(',')[1].split("\"")[0]);
            var array = [];
            for(var j = 0; j < binary.length; j++) {
                array.push(binary.charCodeAt(j));
            }
            return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
        }

        function createAWSName() {
            var name = new Date().toLocaleString() + "-" + new Date().getMilliseconds();
            return name.replace(new RegExp("(,|:|/| )", "g"), "-");
        }

        function uploadToAWS(file, filename) {
            var deferred = $q.defer();
            var bucket = new AWS.S3({params: {Bucket: 'gomotokeydropqr'}});
            var params = {Key: filename, ContentType: file.type, Body: file};
            bucket.upload(params, function (err, data) {
                if(err) {
                    console.log ("QR UPLOAD ERROR: " + err);
                    deferred.reject(err);
                }
                else {
                    var url = data.Location;
                    deferred.resolve(url);
                    console.log("Successfully uploaded qr code!");
                }
            });
            return deferred.promise;
        }

        function send_service_receipt_email(to, pricing, customer_name, customer_first_name, vehicle_string, current_datetime, services, url) {

            $http.get('components/email/service_receipt_email.html').then(function(response) {
                var template = _.template(response.data)({
                    price: pricing,
                    name: customer_name,
                    first_name: customer_first_name,
                    vehicle: vehicle_string,
                    date: current_datetime,
                    services: services,
                    qrurl: url
                });
                var from = 'Contact <keydropalert@gomoto.org>';
                var subject = "Services Tally";
                sendMail(to, from, subject, template);
            }).catch(function (err) {
                throw err;
            });
        }

        function saveQRCode(url) {
            $http.patch('http://localhost:5000/appointments/'+session_id, {
                qr_code: url
            }).then(function(response){
                //success
            }).catch(function(err) {
                console.log(err);
                throw err;
            });
        }

        function generate_and_send_qr_email(to, session_id, pricing, customer_name, customer_first_name, vehicle_string, current_datetime, services) {
            var qr = generateQRCode(session_id);
            var image = qrToJpeg(qr);
            var name = createAWSName();

            uploadToAWS(image, name).then(function(url){
                //manual services
                send_service_receipt_email(to, pricing, customer_name, customer_first_name, vehicle_string, current_datetime, services, url);
                saveQRCode(url);
            });
        }

        this.send_service_email = function(pageScope, to) {

        //grab the session information from the database
        if ($cookies.session_id) {
            var session_id = $cookies.session_id;
            $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {

                var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                current_datetime.toString();
                current_datetime += ' GMT';

                //customer object
                var customer = response.data['customer'];
                var customer_name = "";
                var customer_first_name = "";
                if (customer) {
                    customer_name = customer['first_name'] + " " + customer['last_name'];
                    customer_first_name = customer['first_name'];
                }

                //log object
                var log = logging_array;
                var log_string = "";
                for (var i = 0; i < log.length; i++) {
                    log_string += log[i]['page_name'];

                    if (i < log.length - 1) {
                        log_string += ", ";
                    }
                }

                //vehicle object
                var vehicle = response.data['vehicle'];
                var vehicle_string = "No Vehicle Found";
                if (vehicle) {
                    vehicle_string = vehicle['year'] + ' ' + vehicle['make'] + ' ' + vehicle['model'];
                    if (vehicle['series']) {
                        vehicle_string += ' ' + vehicle['series'];
                    }

                }

                var pricing = false;
                //manual services were chosen, use those
                // if ($cookies.manual_services_chosen) {
                    var manual_services = response.data['manual_services'];
                    generate_and_send_qr_email(to, session_id, pricing, customer_name, customer_first_name, vehicle_string, current_datetime, manual_services);
                // }
                //dms services were chosen, use those (NOTE: This code may not work as it is very outdated!)
                // else if ($cookies.services_chosen) {
                //     pricing = true;
                //
                //     //dms services
                //     var dms_services = response.data['services_confirmed'];
                //     if (dms_services) {
                //         var deferred = $q.defer();
                //         q_objects_call = [];
                //         //gather all the service calls
                //         for (var fg = 0; fg < dms_services.length; fg++) {
                //             q_objects_call.push($http.get('http://localhost:5000/services/' + dms_services[fg]));
                //         }
                //
                //         //set up the receipt email
                //         $q.all(q_objects_call)
                //             .then(
                //                 function (data) {
                //                     var qr = generateQRCode(session_id);
                //                     var image = qrToJpeg(qr);
                //                     var name = createAWSName();
                //
                //                     uploadToAWS(image, name).then(function(url){
                //                         send_service_receipt_email(to, pricing, customer_name, customer_first_name, vehicle_string, current_datetime, services, url);
                //                         saveQRCode(url);
                //                     });
                //
                //                 });
                //     }
                //
                //     // //upsell services
                //     var upsell_services_string = "";
                //     var upsell_services = response.data['services_recommended'];
                //     var upsell_service_objects = [];
                //     if (upsell_services)
                //     {
                //         //loop through the chosen services and calculate the total price for the services
                //         for (var i = 0; i < upsell_services.length; i++)
                //         {
                //             $http.get('http://localhost:5000/upsell_services/'+upsell_services[i]).then(function (response) {
                //                 upsell_service_objects.push(response.data);
                //             }).catch(function (err) {
                //                 throw err;
                //             });
                //         }
                //     }
                // }

                logging_array = response.data['logging_list'];

                logging_array.push({
                    "access_date": current_datetime,
                    "page_name": pageScope.page_name,
                    "url_path": pageScope.location_path
                });

                $http.patch('http://localhost:5000/appointments/' + session_id, {
                    logging_list: logging_array
                }).then(function (response) {
                    //success
                }).catch(function (err) {
                    console.log(err);
                    throw err;
                });
            }).catch(function (err) {
                throw err;
            });
        }
    }
}]);