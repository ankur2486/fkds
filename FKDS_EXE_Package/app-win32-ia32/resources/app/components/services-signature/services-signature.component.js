/**
 * Services component to load available services
 */
angular.
module('servicesSignature', ['ngCookies', 'signature']).
component('servicessignature', {
    templateUrl: 'components/services-signature/services-signature.html',
    controllerAs: "servicesSignatureCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
        function ServicesSignatureController($http, $cookies, InitIdle, $location, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "services_signature";
            self.location_path = $location.path();

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
            }

            if ($cookies.vehicle != null) {
                var vehicle = $cookies.vehicle;
                self.vehicle_name = vehicle.year + " " + vehicle.make + " " + vehicle.model;
                self.vin = vehicle.vin;
                self.mileage = vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //pull the chosen services if any are stored
            if ($cookies.manual_services_chosen) {
                self.manual_services_list = $cookies.manual_services_chosen;
                self.manual_services_string = "";
                for (var i = 0; i < self.manual_services_list.length; i++) {
                    self.manual_services_string += self.manual_services_list[i];

                    if (i < self.manual_services_list.length-1)
                    {
                        self.manual_services_string += ", ";
                    }
                }
            }

            self.backToService = function () {
              
                MyService.addPageTimer();
                MyService.reportScreen("services-manual-confirm", "User clicked on back button on the services signature page.");
                $location.path('/services-manual-confirm')
            };

            self.done = function () {
                MyService.addPageTimer();
               
                //log the page history to the appointment
            if ($cookies.session_id)
            {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                   logging_array.push({"access_date": current_datetime,
                            "page_name": self.page_name,
                       "url_path": self.location_path});

                        $http.patch('http://localhost:5000/appointments/' + session_id, {
                            logging_list: logging_array
                        }).then(function (response) {
                            //success
                        }).catch(function (err) {
                            console.log(err);
                            throw err;
                        });

                    }).catch(function (err) {
                        throw err;
                    });
                }
                MyService.reportScreen("keydrop", "User clicked on accept button on the services signature page.");
                $location.path('/keydrop');
            };
        }]
});
