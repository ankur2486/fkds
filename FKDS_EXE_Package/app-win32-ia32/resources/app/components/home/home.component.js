/**
 * Dummy component for the first module the user sees, the home component.
 */
angular.
module('home', ['ngCookies', 'keyDropApp']).
component('home', {
    templateUrl: 'components/home/home.html',
    controllerAs: "homeCtrl",
    controller: ['$cookies', '$location', '$http', '$filter', 'initIdle', 'MyService',
function ($cookies, $location, $http, $filter, InitIdle, MyService) {

    //delete all cookies
    delete $cookies['customer_dictionary'];
    delete $cookies['appointment_comments'];
    delete $cookies['vehicle'];
    delete $cookies['session_id'];
    delete $cookies['dummyLicense'];
    delete $cookies['services_chosen'];
    delete $cookies['manual_services_chosen'];
    delete $cookies['manual_services_comments'];
    delete $cookies['services_chosen'];
    delete $cookies['upsell_services_chosen'];
    delete $cookies['key_drop_appointment_id'];
    delete $cookies['admin_add_customer'];

    InitIdle.init();
    var self = this;
    self.page_name = "home";
    self.location_path = $location.path();
    MyService.disableTimer();
    $('body').removeClass('app-body-inner-bg');
    MyService.isShown = false;

    //Initialize key drop database table
    $http.get('http://localhost:5000/key_table').then(function(response) {
        var key_table = response.data['_items'];
        var key_slots = 20;

        //if key_table is empty, create the necessary rows
        if (key_table.length == 0)
        {
            for (var i = 0; i < key_slots; i++)
            {
                (function(p) {
                    $http.post('http://localhost:5000/key_table', {
                        'key_slot': p
                    }).catch(function (err) {
                        throw err;
                    });
                })(i+1);
            }
        }

    }).catch(function(err) {
        throw err;
    });

    self.change_page = function (data) {       
        
        if (data == 'drop') {
            MyService.tempPageName = "drop";
            MyService.initialize();
        }

        else if (data == 'pickup') {
            MyService.tempPageName = "pickup";
            MyService.initialize();
        }       

        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
        current_datetime.toString();
        current_datetime += ' GMT';

        $http.post('http://localhost:5000/appointments', {
           logging_list: [
               {
                   "access_date": current_datetime,
                   "page_name": self.page_name,
                   "url_path": self.location_path
               }
           ]
        }).then(function (response) {
           //success
           if (response.data['_id']) {
               $cookies.session_id = response.data['_id'];
           }

        }).catch(function (err) {
           console.log(err);
           throw err;
        });
        
    }
}]
});
