/**
 * More Info component to load phone/email page
 */
angular.
module('manualMoreInfo', ['ngCookies']).
component('manualmoreinfo', {
    templateUrl: 'components/manual-more-info/manual-more-info.html',
    controllerAs: "manualMoreInfoCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
        function ManualMoreInfoController($cookies, InitIdle, $location, $http, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "manual_more_info";
            self.location_path = $location.path();
            self.master = {};


            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'];

                self.form = {};
                self.form.phone = $cookies.customer_dictionary['phone'];
                self.form.email = $cookies.customer_dictionary['email'];
                self.form.contactPreference = $cookies.customer_dictionary['contact_preference'];
            }

        self.update = function (isValid, form) {
             MyService.addPageTimer();
            if (isValid) {
              
                self.master = angular.copy(form);
                    cust_email = "";
                    cust_phone = "";
                    cust_contact_preference = "";
                    if (self.master) {
                        cust_email = self.master['email'];
                        cust_phone = self.master['phone'];
                        cust_contact_preference = self.master['contactPreference'];
                    }

                    //save email and phone field to the record
                    if ($cookies.customer_dictionary) {
                        $cookies.customer_dictionary['email'] = cust_email;
                        $cookies.customer_dictionary['phone'] = cust_phone;
                        $cookies.customer_dictionary['contact_preference'] = cust_contact_preference;
                    }
                    else {
                        $cookies.customer_dictionary = {
                            "email": cust_email,
                            "phone": cust_phone,
                            "contact_preference": cust_contact_preference
                        }
                    }                

                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push({"access_date": current_datetime,
                           "page_name": self.page_name,
                           "url_path": self.location_path});

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array,
                           customer: {
                               email: cust_email,
                               phone: cust_phone,
                               contact_preference: cust_contact_preference
                           }
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           MyService.logInfoNError("error", "Error while saving customer data " + err);
                          
                           throw err;
                       });

                   }).catch(function(err) {
                       throw err;
                   });
                }

                MyService.reportScreen("manual-info-confirm", "User clicked on next button on the manual more info page.");
                $location.path('/manual-info-confirm');
            }
        };
    }]
});
