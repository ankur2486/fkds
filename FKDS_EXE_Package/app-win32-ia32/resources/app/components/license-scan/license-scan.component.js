/**
 * License Scan component to load license scan page
 */
angular.
module('licenseScan', ['ngCookies', 'keyDropApp']).
component('licensescan', {
    templateUrl: 'components/license-scan/license-scan.html',
    controllerAs: "licenseScanCtrl",

    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService', '$rootScope', '$scope',
function LicenseScanController($cookies, InitIdle, $location, $http, $filter, MyService, $rootScope, $scope) {
    InitIdle.init();
    var self = this;
    self.page_name = "license_scan";
    self.location_path = $location.path();
    self.isNextButtonDisabled = true;
    //random number between 0 and 2
    self.random_customer_index = Math.floor((Math.random() * 3));

    //dummy data for license
    self.dummyCustomer1 = {
        'first_name': 'Rob', 'last_name': 'Florig', 'street': '64 S. Front Street', 'city': 'Lexington',
        'state': 'VA', 'zipcode': '13245'
    };
    //dummy vehicle
    self.dummyVehicle1 = {
        image: "img/car-f150.png",
        year: 2013,
        make: "Ford",
        model: "F-150",
        vin: "K09L8Y6550IU",
        mileage: 75000
    };

    self.dummyCustomer2 = {
        'first_name': 'Courtney', 'last_name': 'Catanese', 'street': '672 N. Lane', 'city': 'Trenton',
        'state': 'NJ', 'zipcode': '16756'
    };
    self.dummyVehicle2 = {
        image: "img/car-escape.png",
        year: 2005,
        make: "Ford",
        model: "Escape",
        vin: "U9Y2D987T554",
        mileage: 125000
    };

    self.dummyCustomer3 = {
        'first_name': 'Matthew', 'last_name': 'Jones', 'street': '123 Street Road', 'city': 'Philadelphia',
        'state': 'PA', 'zipcode': '18104'
    };
    self.dummyVehicle3 = {
        image: "img/car-focus.png",
        year: 2010,
        make: "Ford",
        model: "Fusion",
        vin: "9GT5673E4R09",
        mileage: 100000
    };

    self.dummy_customer_array = [self.dummyCustomer1, self.dummyCustomer2, self.dummyCustomer3];
    self.dummy_vehicle_array = [self.dummyVehicle1, self.dummyVehicle2, self.dummyVehicle3];

    //pick a random customer from the dummy customers
    self.dummyCustomer = self.dummy_customer_array[self.random_customer_index];
    self.dummyVehicle = self.dummy_vehicle_array[self.random_customer_index];

    $cookies.dummyLicense = self.dummyCustomer;
    $cookies.vehicle = self.dummyVehicle;

    self.change_page = function () {
        //log the page history to the appointment
        if ($cookies.session_id) {
            session_id = $cookies.session_id;
            $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                logging_array = response.data['logging_list'];

                var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                current_datetime.toString();
                current_datetime += ' GMT';

                logging_array.push({
                    "access_date": current_datetime,
                    "page_name": self.page_name,
                    "url_path": self.location_path
                });

                $http.patch('http://localhost:5000/appointments/' + session_id, {
                    logging_list: logging_array
                }).then(function (response) {
                    //success
                }).catch(function (err) {
                    console.log(err);
                    throw err;
                });

            }).catch(function (err) {
                throw err;
            });
        }
    };

    var mzRequest = null;//mzero request object 
    self.devMode = MyService.devMode;

    self.licenseData = {};
    self.scanLicenseCount = 0;

    $('body').removeClass('app-body-inner-bg');

    self.goBack = function () {
        MyService.addPageTimer();
				MyService.reportScreen("license-or-manual", "User clicked on back button on license scan page.");
				$location.path('/license-or-manual');
    };

    self.showInvalidLicense = function (popupName, time) {
        MyService.addPageTimer();
        self.scanLicenseCount = self.scanLicenseCount + 1;
        if (self.scanLicenseCount == 3) {
            self.scanLicenseCount = 0;
            MyService.reportScreen("customer-form", "Show Unable To Read Popup and allow user to enter data manually.");
            MyService.showPopup("#unableToReadPopup", parseInt(MyService.pagesTimeOut.fkds_pagetimeout.unableToReadPopup.text));
            $location.path('/customer-form');
        }
        else {
            MyService.showPopup(popupName, time);
            self.mzeroLicenseScanStart();
        }
    };

    self.showUnableToReadPopup = function () {
                            MyService.reportScreen("customer-form", "Show Unable To Read Popup and allow user to enter data manually.");
                            MyService.showPopup("#unableToReadPopup", parseInt(MyService.pagesTimeOut.fkds_pagetimeout.unableToReadPopup.text));
                            $location.path('/customer-form');
                        }

    self.updateLicenseStatus = function () {
        MyService.addPageTimer();

                            //Update the license status for the appointment
                            if ($cookies.session_id) {
                                session_id = $cookies.session_id;
                                $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                                    logging_array = response.data['logging_list'];

                                    var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                                    current_datetime.toString();
                                    current_datetime += ' GMT';

                                    logging_array.push({
                                        "access_date": current_datetime,
                                        "page_name": self.page_name,
                                        "url_path": self.location_path
                                    });


                                }).catch(function (err) {
                                    throw err;
                                });

                                $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                                    $http.patch('http://localhost:5000/appointments/' + session_id, {
                                        license_status: "scanned",
                                        logging_list: logging_array
                                    }).then(function (response) {
                                        //success
                                    }).catch(function (err) {
                    MyService.logInfoNError("error", "Error while saving license_status data " + err);
                                        throw err;
                                    });

                                }).catch(function (err) {
                                    throw err;
                                });
        }

        $cookies.dummyLicense = self.licenseData;
        $location.path('/license-confirm');
        MyService.reportScreen("license-confirm", "Scanned valid License.");
    }

    self.mzeroLicenseScanStart = function () {
        //TODO: uncomment this once you get the config file loading
        MyService.addPageTimer();

        if (!MyService.devMode) {
            MyService.reportScreen("LicenseScanPage", "Waiting for scan");
            mzRequest = mzero.create.scanner.scanLicense();

            var eventHandlers = {
                Done: function (err, data) {
                    if (err) {
                        if (err.toLowerCase().indexOf('timeout') > -1) {
                            MyService.logInfoNError("error", "timeout of license scanner " + err);
                            self.showUnableToReadPopup();
                        }
                        else if (err.toLowerCase().indexOf('cancelled') > -1) {// cancelled scan function.

                        }
                        else if (err.toLowerCase().indexOf('serviceinuse') > -1) {
                            MyService.logInfoNError("error", "Error activating license scanner " + err);

                            self.showUnableToReadPopup();
                        }
                        else {
                            MyService.logInfoNError("error", "Error activating license scanner " + err);

                            self.showUnableToReadPopup();
                            }
                    } else if (data) {

                        //scanner will be started again if we are not reached to the maximum license allowed in following method.
                        if (data == null) {
                            MyService.logInfoNError("error", "Data is null.");
                            self.reportScreen("LicenseScanPage", "Invalid License Popup");
                            self.showInvalidLicense("#invalidLicensePopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.areYouTherePopupTimeout.text));
                        }
                        else {
                            var customerDict = {
                                "first_name": data.FirstName,
                                "last_name": data.LastName,
                                "phone": "",
                                "email": "",
                                "street": data.StreetAddress,
                                "city": data.City,
                                "state": data.StateName,
                                "zipcode": data.PostalCode,
                                "pin": ""
                            };
                            self.isNextButtonDisabled = false;
                            $('.manual-service-action a').removeAttr("disabled");
                            self.licenseData = customerDict;
                            MyService.logInfoNError("info", "licence data " + data);
                            self.updateLicenseStatus();
                        }
                    }
                }
            };

            mzRequest.start(eventHandlers);
            MyService.scannerTimeout = false;
            MyService.logInfoNError("info", "Scanner Started.");

        }
    };

    self.mzeroLicenseScanReadEnd = function () {

        if (!MyService.devMode) {
            if (mzRequest) {
                mzRequest.end();
                MyService.logInfoNError("info", "Scanner Stopped.");
            }
        }
    };

    self.mzeroLicenseScanStart();


    // DevMode 
    self.errorScan = function () {
        self.showUnableToReadPopup();
    };

    $scope.$on('$destroy', function () {
        self.mzeroLicenseScanReadEnd();
    });

    self.scanLicense = function () {

        MyService.addPageTimer();
        MyService.reportScreen("LicenseScanPage", "Waiting for scan");
        //dummy data for license
        dummyCustomer = {
            'first_name': 'TestCustomer', 'last_name': 'Customer', 'street': '123 Street Road', 'city': 'Place',
            'state': 'WY', 'zipcode': '12345'
        };
        var data = { "LicenseEntries": { "LastName": "TSTHUGINS", "FirstName": "LEGER", "MiddleName": "FELTON", "DateOfBirth": "1942-08-09", "eyes": "BLK", "Gender": "M", "height": "10'8\"", "StreetAddress": "2726 BOULDER CREST DR", "StreetAddress2": "", "City": "ALEXANDER CITY", "StateName": "AL", "PostalCode": "35010-7326", "Country": "USA", "id": "5226008", "issued": "1201-50-10", "expires": "2020-01-01", "filetype": "ANSI", "format": "15", "issuer": "360530", "state2": "Alabama", "st": "AL", "DAQ": "5226008", "DCS": "TSTHUGINS", "DDE": "", "DAC": "LEGER", "DDF": "", "DAD": "FELTON", "DDG": "", "DCA": "D", "DCB": "", "DCD": "", "DBD": "50101201", "DBB": "08091942", "DBA": "01012020", "DBC": "1", "DAU": "128 in", "DAY": "BLK", "DAG": "2726 BOULDER CREST DR", "DAI": "ALEXANDER CITY", "DAJ": "AL", "DAK": "350107326", "DCF": "", "DCG": "USA", "DCU": "", "DDA": "", "DDB": "", "DDD": "" }, "City": "ALEXANDER CITY", "Country": "USA", "DateOfBirth": "1942-08-08T18:30:00.000Z", "ExpirationDate": "2019-12-31T18:30:00.000Z", "FirstName": "LEGER", "Gender": "M", "IssuedDate": "1205-02-09T18:30:00.000Z", "LastName": "TSTHUGINS", "LicenseNumber": "5226008", "MiddleName": "FELTON", "PostalCode": "350107326", "StateCode": "AL", "StateName": "AL", "StreetAddress": "2726 BOULDER CREST DR", "StreetAddress2": "" }
        var customerDict = {
            "first_name": data.FirstName,
            "last_name": data.LastName,
            "phone": "",
            "email": "",
            "street": data.StreetAddress,
            "city": data.City,
            "state": data.StateName,
            "zipcode": data.PostalCode,
            "pin": ""
        };
        self.isNextButtonDisabled = false;
        $('.manual-service-action a').removeAttr("disabled");
        self.licenseData = customerDict;

       // self.updateLicenseStatus();
    }
}]
});
