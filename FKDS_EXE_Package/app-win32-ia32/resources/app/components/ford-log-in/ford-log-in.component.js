/**
 * Ford Log In component to load log in page
 */
angular.
module('fordLogIn', ['ngCookies']).
component('fordlogin', {
    templateUrl: 'components/ford-log-in/ford-log-in.html',
    controllerAs: "fordLogInCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
        function FordLogInController($cookies, InitIdle, $location, $http, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "ford_log_in";
        self.location_path = $location.path();
        self.isScannerActive = MyService.isScannerActive;

        if (!self.isScannerActive && !MyService.isShown) {
            MyService.showPopup("#errorLicenseServicePopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
            MyService.isShown = true;
        }
        //dummy data for ford pass
        self.dummyCustomer = {'first_name':'Todd','last_name':'Marcelle','street':'2335 N Van Buren Court','city':'Arlington',
            'state':'VA','zipcode':'22205'};
        $cookies.dummyLicense = self.dummyCustomer;
        $cookies.customer_dictionary = self.dummyCustomer;

        //dummy vehicle
        var data = {
            image: "img/car-edge.png",
            year: 2016,
            make: "Ford",
            model: "Edge",
            vin: "W3CDSE67890S",
            mileage: 30000
        };
        $cookies.vehicle = data;

        self.update = function (isValid, form) {
            if (isValid) {
                self.master = angular.copy(form);
               
                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push(
                           {"access_date": current_datetime,
                               "page_name": self.page_name,
                               "url_path": self.location_path});
                        $http.patch('http://localhost:5000/appointments/'+session_id, {
                            logging_list: logging_array,
                            customer: self.dummyCustomer
                        }).then(function(response){
                            //success
                        }).catch(function(err) {
                            MyService.logInfoNError("error", "Error while saving customer data " + err);                          
                            throw err;
                        });
                   }).catch(function(err) {
                       throw err;
                   });
                }
                MyService.reportScreen("services", "User clicked on sign in button on the ford login page.");
                $location.path('/services');
            }
        };

        self.backTohome = function () {
            MyService.clearSession("HomePage", "Session ended,User clicked on back button on the ford login page.");
        };

        self.change_page = function (data) {
            MyService.addPageTimer();
            if (data == 'scanLicense')
                MyService.reportScreen("license-scan", "User clicked on scan license link on the ford login page.");
            else if (data == 'manualEntry')
                MyService.reportScreen("customer-form", "User clicked on manual entry link on the ford login page.");
            
            //log the page history to the appointment
            if ($cookies.session_id)
            {
               session_id = $cookies.session_id;
               $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                   logging_array = response.data['logging_list'];

                   var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                   current_datetime.toString();
                   current_datetime += ' GMT';

                   logging_array.push({"access_date": current_datetime,
                                    "page_name": self.page_name,
                                    "url_path": self.location_path});

                   $http.patch('http://localhost:5000/appointments/'+session_id, {
                       logging_list: logging_array
                   }).then(function(response){
                       //success
                   }).catch(function(err) {
                       MyService.logInfoNError("error", "Error while saving logging data " + err);                      
                       throw err;
                   });

               }).catch(function(err) {
                   throw err;
               });
            }
        }
    }]
});
