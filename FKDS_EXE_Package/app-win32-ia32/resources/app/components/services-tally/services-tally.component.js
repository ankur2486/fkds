/**
 * Services component to load available services
 */
angular.
module('servicesTally', ['ngCookies', 'signature']).
component('servicestally', {
    templateUrl: 'components/services-tally/services-tally.html',
    controllerAs: "servicesTallyCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', '$q', 'MyService',
        function ServicesTallyController($http, $cookies, InitIdle, $location, $filter, $q, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "services_tally";
            self.location_path = $location.path();

            //Data initialization is temporary til integration with hardware
            self.customerDict = {
                "first_name": "Test",
                "last_name": "Customer",
                "phone": "123-123-1234",
                "email": "test@gomoto.com",
                "street": "123 Street Road",
                "city": "Nowhere",
                "state": "WY",
                "zipcode": "12345",
                "pin": "1234"
            };

            //pull the customer name if one is stored
            if ($cookies.customer_dictionary)
            {
                self.customerDict = $cookies.customer_dictionary;
            }

            if ($cookies.vehicle != null) {
                self.vehicle = $cookies.vehicle;
                self.vehicle_name = self.vehicle.year + " " + self.vehicle.make + " " + self.vehicle.model;
                self.vin = self.vehicle.vin;
                self.mileage = self.vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //pull the chosen services if any are stored
            self.services_list = [];
            if ($cookies.services_chosen)
            {
                self.services_list = $cookies.services_chosen;
            }

            //pull the chosen services if any are stored
            self.upsell_services_list = [];
            if ($cookies.upsell_services_chosen)
            {
                self.upsell_services_list = $cookies.upsell_services_chosen;
            }

            self.complete_total = 0;
            self.services = [];
            self.service_total = 0;

            //loop through the chosen services and calculate the total price for the services
            for (var i = 0; i < self.services_list.length; i++)
            {
                $http.get('http://localhost:5000/services/' + self.services_list[i]).then(function (response) {
                    if (response.data['price'])
                    {
                        self.service_total += response.data['price'];
                        self.complete_total += response.data['price'];
                    }
                    else
                    {
                        response.data['price'] = 0;
                    }

                    self.services.push(response.data);
                }).catch(function (err) {
                    console.log(err);
                    throw err;
                });
            }

            self.upsell_services = [];
            self.upsell_service_total = 0;

            //loop through the chosen services and calculate the total price for the upsells
            for (var i = 0; i < self.upsell_services_list.length; i++)
            {
                $http.get('http://localhost:5000/upsell_services/' + self.upsell_services_list[i]).then(function (response) {
                    if (response.data['price']) {
                        self.upsell_service_total += response.data['price'];
                        self.complete_total += response.data['price'];
                    }
                    else
                    {
                        response.data['price'] = 0;
                    }

                    self.upsell_services.push(response.data);

                }).catch(function (err) {
                    throw err;
                });
            }

            self.combined_services_list = [];
            self.combined_services_list = self.services_list.concat(self.upsell_services_list);

            self.combined_services = [];

            var deferred = $q.defer();
            q_objects_call = [];
            //gather all the service calls
            for (var fg = 0; fg < self.services_list.length; fg++)
            {
                q_objects_call.push($http.get('http://localhost:5000/services/' + self.services_list[fg]));
            }
            //gather all the upsell service calls
            for (var ufg = 0; ufg < self.upsell_services_list.length; ufg++)
            {
                q_objects_call.push($http.get('http://localhost:5000/upsell_services/' + self.upsell_services_list[ufg]));
            }

            //set up the receipt email
            $q.all(q_objects_call)
                .then(
                    function (data) {
                        for (var i = 0; i < data.length; i++)
                        {
                            self.combined_services.push(data[i]);
                        }
                    });

            self.backToService = function () {
                MyService.addPageTimer();
                MyService.reportScreen("appointment-comment", "User clicked on back button on the payment estimation page.");
                $location.path('/appointment-comment');
            };

            self.done = function () {
                MyService.addPageTimer();
                //log the page history to the appointment
                if ($cookies.session_id) {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                        logging_array.push({
                            "access_date": current_datetime,
                            "page_name": self.page_name,
                            "url_path": self.location_path
                        });

                        $http.patch('http://localhost:5000/appointments/' + session_id, {
                            logging_list: logging_array
                        }).then(function (response) {
                            //success
                        }).catch(function (err) {
                            console.log(err);
                            throw err;
                        });

                    }).catch(function (err) {
                        throw err;
                    });
                }
            };

            //send an email listing the services chosen with their price to the customer
            self.finalize = function () {
                MyService.addPageTimer();
                var service_message = "";
               
                for (var i = 0; i < self.services.length; i++)
                {
                    service_message += self.services[i]['name'] + ': $' + self.services[i]['price'];

                    if (self.services[i]['description'] != undefined)
                    {
                        service_message += ' (' + self.services[i]['description'] + ')';
                    }

                    if ( i != self.services.length-1 )
                    {
                        service_message += ", ";
                    }
                }

                var to = 'default@gomoto.org';
                var from = 'Contact <keydropalert@gomoto.org>';
                var subject = "Services Tally";
                var html_begin = "<b>Services chosen: </b>";
                var html_end = "<br><b>Grand Total: $</b>" + self.service_total;
                sendMail(to, from, subject, html_begin + service_message + html_end);

                MyService.reportScreen("keydrop", "User clicked on accept button on the payment estimation page.");
                $location.path('/keydrop');
            };
        }]
});
