/**
 * Payment Scan component to load payment scan page
 */
angular.
module('paymentScan', ['ngCookies', 'ngMaterial', 'signature']).
component('paymentscan', {
    templateUrl: 'components/payment-scan/payment-scan.html',
    controllerAs: "paymentScanCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService', '$rootScope',
        function PaymentScanController($cookies, InitIdle, $location, $http, $filter, MyService, $rootScope) {
            InitIdle.init();
            var self = this;
            self.page_name = "payment_scan";
            self.location_path = $location.path();
            var mzPaymentRequest = null;
            self.cancelCardTranFlag = false;
            self.cardConfirmed = false;
            self.disableCardDone = false;
            self.progressMessage = '';
            self.cancelCardTranFlag = false;
            self.CardPaymentResult = [];
            self.creditCardErrorMsg = '';
            self.devMode = MyService.devMode;

            $('body').removeClass('app-body-inner-bg');
            MyService.reportScreen("PaymentScanPage", "User clicked on pay now button on the payment summery page.");           

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
            }

            if($cookies.vehicle != null) {
                self.vehicle = $cookies.vehicle;
                self.vehicle_name = self.vehicle.year + " " + self.vehicle.make + " " + self.vehicle.model;
                self.vin = self.vehicle.vin;
                self.mileage = self.vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //pull the chosen services if any are stored
            self.services_list = [];
            if ($cookies.services_chosen) {
                self.services_list = $cookies.services_chosen;
            }

            self.services = [];
            self.service_total = 0;//default set

            //loop through the chosen services and calculate the total price for the services
            for (var i = 0; i < self.services_list.length; i++) {
                $http.get('http://localhost:5000/services/'+self.services_list[i]).then(function (response) {
                   self.services.push(response.data);
                   self.service_total += response.data['price'];
                }).catch(function (err) {
                   throw err;
                });
            }

            //pull the chosen manual services if any are stored
            self.manual_services_list = [];
            if ($cookies.manual_services_chosen) {
                self.manual_services_list = $cookies.manual_services_chosen;
            }

            //get the assigned service_total from the database
            if ($cookies.session_id) {
                session_id = $cookies.session_id;
                $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                    self.service_total = response['data']['service_total'];
                }).catch(function (err) {
                    throw err;
                });
            }

            self.send_payment_email = function send_payment_email(result){
                var to = result;
                var from = 'Contact <keydropalert@gomoto.org>';
                var subject = "Payment Summary";
                var html_message = "This is your generic keydrop email.";

                //grab the session information from the database
                if ($cookies.session_id)
                {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                        var current_date = $filter('date')(new Date(), 'EEE, dd MMM yyyy');
                        var current_time = $filter('date')(new Date(), 'h:mm:ss a');

                        $http.patch('http://localhost:5000/appointments/'+session_id, {
                            payment_status: 'paid'
                        }).then(function(response){
                            //success
                        }).catch(function(err) {
                            console.log(err);
                            throw err;
                        });

                        //customer object
                        var customer = response.data['customer'];
                        var customer_name = "No Customer Found";
                        var customer_first_name = "";
                        if ( customer )
                        {
                            customer_name = customer['first_name'] + " " + customer['last_name'];
                            customer_first_name = customer['first_name'];
                        }

                        //vehicle object
                        var vehicle = response.data['vehicle'];
                        var vehicle_string = "No Vehicle Found";
                        if (vehicle)
                        {
                            vehicle_string = vehicle['year'] + ' ' + vehicle['make'] + ' ' + vehicle['model'];
                            if (vehicle['series'])
                            {
                                vehicle_string += ' ' + vehicle['series'];
                            }

                        }

                        //send the email
                        $http.get('components/email/payment_email.html').then(function(response) {

                            var template = _.template(response.data)(
                                {name:customer_name,
                                    first_name:customer_first_name,
                                    vehicle:vehicle_string,
                                    date:current_date,
                                    time:current_time,
                                    total_payment: self.service_total,
                                    transaction_list: self.CardPaymentResult});

                            html_message = template;
                            sendMail(to, from, subject, html_message);

                        }).catch(function(err) {
                            throw err;
                        });


                    }).catch(function(err) {
                        throw err;
                    });
                }
            };

            self.change_page = function () {
                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push({"access_date": current_datetime,
                           "page_name": self.page_name,
                           "url_path": self.location_path});

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           console.log(err);
                           throw err;
                       });

                   }).catch(function(err) {
                       throw err;
                   });
                }

                //DEBUG
                //log the payment status to the appointment
                if ($cookies.session_id)
                {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                        $http.patch('http://localhost:5000/appointments/'+session_id, {
                            payment_status: 'paid'
                        }).then(function(response){
                            //success
                        }).catch(function(err) {
                            MyService.logInfoNError("error", "Error while saving payment_status " + err);                           
                            throw err;
                        });

                    }).catch(function(err) {
                        throw err;
                    });
                }

            };

            $("#paymentErrorPopup").on('hidden.bs.modal', function () {
                $rootScope.$apply(function () { $location.path('/payment-summary'); });
                MyService.reportScreen('PaymentSummaryPage', 'We are unable to complete your card transaction please try again');
            });

            $("#paymentSuccessPopup").on('hidden.bs.modal', function () {
                $rootScope.$apply(function () { $location.path('/keydrop-pickup'); });
                MyService.reportScreen('We are successfully completed your card transaction');
            });

            var CardTransactionResult = function (data) {
                this.ReceiptNumber = Date.now();
                this.AuthorizationNo = data.AuthorizationNo;
                this.StatusMessage = data.StatusMessage;
                this.TransactionType = data.TransactionType;
                this.MerchantName = data.MerchantName;
                this.PanMasked = data.PanMasked;
                this.MerchantIdMasked = data.MerchantIdMasked;
                this.TerminalIdMasked = data.TerminalIdMasked;
                this.CardScheme = data.CardScheme;
            };

            self.mzeroPaymentStart = function (amount) {
                MyService.addPageTimer();
                MyService.hideWaiting();
                MyService.disableTimer();

                if (!MyService.devMode) {

                    var payment = {
                        OrderNumber: Date.now(), // self.depositId() + self.receiptId() A unique (for each transaction) integer number which is created by the client application
                        Currency: "USD",
                        Amount: amount
                    };

                    mzPaymentRequest = mzero.create.payment.pay(payment); // IMPORTANT: the request object must exist so that closures can capture it
                    var eventHandlers = {
                        Confirm: function (err, data) {
                            MyService.disableTimer();
                            MyService.logInfoNError("info", "Confirmation data " + data);                            
                            if (data.PaymentTransaction.StatusMessage == "APPROVED") {                               
                                MyService.showWaiting('Processing payment', false);
                                mzPaymentRequest.commit(self.service_total);
                            }
                            else {
                                mzPaymentRequest.abort();
                            }
                        },
                        Done: function (err, data) {

                            MyService.addPageTimer();
                            MyService.hideWaiting();
                            mzPaymentRequest = null;

                            var errorMessage = "";
                            if (err)
                                errorMessage = err.toLowerCase();

                            if (data) {
                                MyService.logInfoNError("info", "Done with data " + data);

                                if (data.PaymentTransaction && data.PaymentTransaction.CardScheme != "")//todo
                                    //self.CardPaymentResult.push(new CardTransactionResult({ AuthorizationNo: data.PaymentTransaction.ApprovalCode, StatusMessage: data.PaymentTransaction.StatusMessage, TransactionType: data.PaymentTransaction.TransactionType, MerchantName: data.PaymentTransaction.MerchantName, PanMasked: data.PaymentTransaction.PanMasked, MerchantIdMasked: data.PaymentTransaction.MerchantIdMasked, TerminalIdMasked: data.PaymentTransaction.TerminalIdMasked, CardScheme: data.PaymentTransaction.CardScheme }))

                                if (data.PaymentTransaction && data.PaymentTransaction.StatusMessage.toLowerCase() == "confirm") {
                                    //send email for payment_receipt
                                    self.send_payment_email(self.customer_email);

                                    //log the payment status to the appointment
                                    if ($cookies.session_id)
                                    {
                                        session_id = $cookies.session_id;

                                        logging_array = response.data['logging_list'];

                                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                                        current_datetime.toString();
                                        current_datetime += ' GMT';

                                        logging_array.push({"access_date": current_datetime,
                                            "page_name": self.page_name,
                                            "url_path": self.location_path});

                                        $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                                            $http.patch('http://localhost:5000/appointments/'+session_id, {
                                                payment_status: 'paid',
                                                logging_list: logging_array
                                            }).then(function(response){
                                                //success
                                            }).catch(function(err) {
                                                MyService.logInfoNError("error", "error while saving payment_status and  logging_list" + err);                                              
                                                throw err;
                                            });

                                        }).catch(function(err) {
                                            throw err;
                                        });
                                    }

                                    MyService.showPopup("#paymentSuccessPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                                    return;
                                }
                                else if (errorMessage == "invalidvalue") {
                                    //show popup transaction failure.
                                    self.creditCardErrorMsg = "We are unable to complete your card transaction" + ". Entered amount is more than device allowed amount.";
                                    MyService.showPopup("#paymentErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));//todo create transactionFailPopup
                                    return;
                                }
                                else if (errorMessage == "ordernumbertoolarge".toLowerCase()) {
                                    self.creditCardErrorMsg = "Card payment is temporary unavailable. Please select other payment method.";
                                    MyService.showPopup("#paymentErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));//todo create transactionFailPopup 
                                    //self.tempPageName("payMethodPage");
                                    return;
                                }
                                else if (errorMessage == 'timeout') {
                                    console.log("card timeout");
                                    self.creditCardErrorMsg = "Your session timeout due to inactivity.";

                                    MyService.showPopup("#paymentErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup[0]._));//todo create transactionCancelledMsgPopup

                                    return;
                                }
                                else {
                                    self.creditCardErrorMsg = "We are unable to complete your card transaction";
                                    //show popup transaction failure.
                                    MyService.showPopup("#paymentErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));//todo create transactionFailPopup 
                                    return;
                                }
                            }

                            if (err) {
                                MyService.logInfoNError("error", "Done with error" + err);
                               
                                //show popup transaction failure.

                                if (errorMessage == 'servicenotavailable') {
                                    console.log("service not available");
                                }
                                else if (errorMessage == 'confirmationtimeout') {
                                    console.log("card confirmation timeout");
                                }
                                else if (errorMessage == 'timeout') {
                                    console.log("card timeout");
                                    MyService.showPopup("#transactionFailPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
                                }
                                else if (errorMessage == 'cancelled') {
                                    console.log("card request ended");
                                }
                                else if (errorMessage == 'serviceinuse') {
                                        MyService.showWaiting(errorMessage, true);
                                        setTimeout(function () { self.mzeroPaymentStart(self.service_total); }, MyService.pagesTimeOut.fkds_pagetimeout.mzeroRequestRetry.text);
                                }
                                else if (errorMessage == 'aborted') {
                                        MyService.showWaiting(errorMessage, true);
                                        setTimeout(function () { self.mzeroPaymentStart(self.service_total); }, MyService.pagesTimeOut.fkds_pagetimeout.mzeroRequestRetry.text);
                                }
                            }
                        }
                    };
                    mzPaymentRequest.start(eventHandlers);
                    MyService.logInfoNError("info", "mzero payment started");
                }
            };

            if (!self.devMode)
                self.mzeroPaymentStart(self.service_total);


            // DevMode 
            self.invalidSwap = function () {
                MyService.showPopup("#paymentErrorPopup", Number(MyService.pagesTimeOut.fkds_pagetimeout.errorMessagePopup.text));
            };

            self.validSwap = function () {
                //send email for payment_receipt
                self.send_payment_email(self.customer_email);
                MyService.addPageTimer();
                MyService.reportScreen("card swipe", "Waiting for card swipe");
                $location.path('/keydrop-pickup');
                self.CardPaymentResult.push(new CardTransactionResult({ AuthorizationNo: "BD62AC", StatusMessage: 'Confirm', TransactionType: 'SALE', MerchantName: ' Standard Test Merchant', PanMasked: ' ************0001', MerchantIdMasked: ' *****67897', TerminalIdMasked: '****0840', CardScheme: ' VISA' }));

                //log the page history to the appointment
                if ($cookies.session_id)
                {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                        logging_array.push({"access_date": current_datetime,
                            "page_name": self.page_name,
                            "url_path": self.location_path});

                        $http.patch('http://localhost:5000/appointments/'+session_id, {
                            logging_list: logging_array,
                            payment_status: 'paid'
                        }).then(function(response){
                            //success
                        }).catch(function(err) {
                            console.log(err);
                            throw err;
                        });

                    }).catch(function(err) {
                        throw err;
                    });
                }
            };

        }]
});
