/**
 * Manual Info Confirm component to load manual info confirmation page
 */
angular.
module('manualInfoConfirm', ['ngCookies']).
component('manualinfoconfirm', {
    templateUrl: 'components/manual-info-confirm/manual-info-confirm.html',
    controllerAs: "manualInfoConfirmCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
        function ManualInfoConfirmController($cookies, InitIdle, $location, $http, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "manual_info_confirm";
            self.location_path = $location.path();

            self.customerDict = {
                "first_name": "",
                "last_name": "",
                "phone": "",
                "email": "",
                "street": "",
                "city": "",
                "state": "",
                "zipcode": "",
                "pin": ""
            };

            if ($cookies.vehicle != null) {
                var vehicle = $cookies.vehicle;
                self.vehicle_name = vehicle.year + " " + vehicle.make + " " + vehicle.model;
                self.vin = vehicle.vin;
                self.mileage = vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //pull the customer if one is stored
            if ($cookies.customer_dictionary) {
                self.customerDict = $cookies.customer_dictionary;
            }

            self.change_page = function (data) {
                MyService.addPageTimer();
               
                //log the page history to the appointment
                if ($cookies.session_id) {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                        logging_array.push({
                            "access_date": current_datetime,
                           "page_name": self.page_name,
                            "url_path": self.location_path
                        });

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           console.log(err);
                           throw err;
                       });

                   }).catch(function(err) {
                       throw err;
                   });
                }

                if (data == 'serviceManual') {                 
                    if ($cookies.admin_add_customer) {
                        MyService.reportScreen("drilldown", "User clicked on correct button on the manual info confirm page.");
                        $location.path('/drilldown');
                    }
                    else {
                        MyService.reportScreen("pin-create-manual", "User clicked on correct button on the manual info confirm page.");
                        $location.path('/pin-create-manual');
                    }
                }
                else
                    MyService.reportScreen("customer-form", "User clicked on reenter button on the manual info confirm page.");

            }
        }]
});
