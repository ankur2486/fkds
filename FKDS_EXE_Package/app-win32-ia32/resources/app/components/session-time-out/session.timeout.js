﻿(function () {
    'use-strict'

    angular
        .module('keyDropApp')
        .controller('timeout', timeout);

    timeout.$inject = ['MyService'];
    function timeout(MyService) {
        var vm = this;
        vm.continue = function () {
            MyService.addPageTimer();
        }

        vm.abortSession = function (btnName) {
            var templateName = window.location.hash.split("/");
            var pageName = templateName[1] + "Page";
            if(btnName=='home') 
                MyService.clearSession(pageName, "Session ended, User click on "+ btnName +" button on " + pageName + ".");
            else
                MyService.clearSession(pageName, "Session ended, User click on " + btnName + " button on " + pageName + ".");
        }
    }
})();
