/**
 * Service Creation component to load service creation form
 */
angular.
module('serviceCreation', ['ngCookies']).
component('servicecreation', {
    templateUrl: 'components/service-creation/service-creation.html',
    controllerAs: "serviceCreationCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter','MyService',
        function ServiceCreationController($cookies, InitIdle, $location, $http, $filter, MyService) {
        InitIdle.init();
        var self = this;
        self.page_name = "service_creation";
        self.location_path = $location.path();

        self.service_form = {};
        self.service_added = false;
        self.service_failed = false;
        self.error_message = "";

        self.update = function(isValid, form) {
            MyService.addPageTimer();
            if (isValid) {
                form_copy = angular.copy(form);

                //log the page history to the appointment
                $http.post('http://localhost:5000/services/', {
                    name: form_copy['service_name'],
                    description: form_copy['service_description'],
                    price: form_copy['service_price'],
                    price_details: form_copy['service_price_details'],
                }).then(function(response){
                    //success
                    self.service_added = true;
                    self.service_form = {};

                    self.service_failed = false;
                    self.error_message = "";
                }).catch(function(err) {
                    MyService.logInfoNError("error", "Error while updating service info " + err);
                   
                    self.service_failed = true;

                    for(var key in err['data']['_issues']) {
                        if (err['data']['_issues'].hasOwnProperty(key)) {

                            if (self.error_message != "")
                            {
                                self.error_message += ", ";
                            }

                            self.error_message += key + " : " + err['data']['_issues'][key];
                        }
                    }

                    throw err;
                });
            }
        };
    }]
});
