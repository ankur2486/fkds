/**
 * Services Upsell component to load available upsell services
 */
angular.
module('servicesUpsell', ['ngCookies']).
component('servicesupsell', {
    templateUrl: 'components/services-upsell/services-upsell.html',
    controllerAs: "servicesUpsellCtrl",
    controller: ['$http', '$cookies', 'initIdle', '$location', '$filter', 'MyService',
        function ServicesUpsellController($http, $cookies, InitIdle, $location, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "services_upsell";
            self.location_path = $location.path();
            self.upsell_services_list = [];

            //pull the customer name if one is stored
            self.customer_name = "Visitor";
            self.customer_first_name = "";
            if ($cookies.customer_dictionary)
            {
                self.customer_name = $cookies.customer_dictionary['first_name'] + ' ' + $cookies.customer_dictionary['last_name'];
                self.customer_first_name = $cookies.customer_dictionary['first_name'];
            }

            if (self.customer_first_name != "")
            {
                //pull the upsell service choices from the database
                $http.get('http://localhost:5000/upsell_services?where={"name_filter": "' + self.customer_first_name + '"}').then(function (response) {
                    self.upsell_services = response.data['_items'];

                    if (self.upsell_services.length == 0)
                    {
                        //pull the service choices from the database
                        $http.get('http://localhost:5000/upsell_services').then(function (response) {
                            self.upsell_services = response.data['_items'];
                            setTimeout(function () {
                                $.fn.matchHeight._update();
                            }, 1);
                        }).catch(function (err) {
                            throw err;
                        });
                    }
                    else {
                        setTimeout(function () {
                            $.fn.matchHeight._update();
                        }, 1);
                    }
                }).catch(function (err) {
                    throw err;
                });
            }


            if ($cookies.vehicle != null) {
                self.vehicle = $cookies.vehicle;
                self.vehicle_name = self.vehicle.year + " " + self.vehicle.make + " " + self.vehicle.model;
                self.vin = self.vehicle.vin;
                self.mileage = self.vehicle.mileage;
            }
            else {
                self.vehicle_name = "No Vehicle Found"
            }

            //pull the chosen services if any are stored
            if ($cookies.upsell_services_chosen)
            {
                self.upsell_services_list = $cookies.upsell_services_chosen;
            }

            //checkbox function to see if a service is in the chosen list
            self.checkboxStatus = function (service) {
                return (self.upsell_services_list.indexOf(service) !== -1);
            };

            //add service to the list or remove it if it is already present
            self.update_choices = function (id) {
                if (self.upsell_services_list.includes(id))
                {
                    var index = self.upsell_services_list.indexOf(id);
                    self.upsell_services_list.splice(index, 1);
                }
                else
                {
                    self.upsell_services_list.push(id);
                }
            };

            self.backToService = function () {
                MyService.addPageTimer();
                MyService.reportScreen("services", "User clicked on back button on the add recommended services page.");
                $location.path('/services');
            };
            self.save_choices = function () {
                MyService.addPageTimer();
                $cookies.upsell_services_chosen = self.upsell_services_list;

                MyService.reportScreen("AppointmentCommentPage", "User clicked on next button on the add recommended services page.");
                //log the page history to the appointment
                if ($cookies.session_id)
                {
                    session_id = $cookies.session_id;
                    $http.get('http://localhost:5000/appointments/' + session_id).then(function (response) {
                        logging_array = response.data['logging_list'];

                        var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                        current_datetime.toString();
                        current_datetime += ' GMT';

                        logging_array.push({"access_date": current_datetime,
                            "page_name": self.page_name,
                            "url_path": self.location_path});

                        $http.patch('http://localhost:5000/appointments/' + session_id, {
                            logging_list: logging_array,
                            services_recommended: self.upsell_services_list
                        }).then(function (response) {
                            //success
                        }).catch(function (err) {
                            console.log(err);
                            throw err;
                        });

                    }).catch(function (err) {
                        throw err;
                    });
                }
                $location.path('/appointment-comment');
            };

        }]
});
