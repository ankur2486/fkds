/**
 * Pin Page component to load pin page
 */
angular.
module('pinPage', ['ngCookies']).
component('pinpage', {
    templateUrl: 'components/pin-page/pin-page.html',
    controllerAs: "pinPageCtrl",
    controller: ['$cookies', 'initIdle', '$location', '$http', '$filter', 'MyService',
        function PinPageController($cookies, InitIdle, $location, $http, $filter, MyService) {
            InitIdle.init();
            var self = this;
            self.page_name = "pin_page";
            self.location_path = $location.path();

            //pull the customer name if one is stored
            var validPin = "1234";
            self.customer_name = "Visitor";
            if ($cookies.customer_dictionary) {
                self.customer_name = $cookies.customer_dictionary['first_name'];
            }

            function correctPin() {
                if ($cookies.customer_dictionary) {
                    validPin = $cookies.customer_dictionary['pin'];
                }
                self.isCorrect = validPin == self.pin;
                return self.isCorrect;
            }

        self.update = function (isValid, form) {
            isValid = isValid && correctPin();
            //TODO: uncomment this once you get the config file loading
             MyService.addPageTimer();
            if (isValid) {
                self.master = angular.copy(form);
               
                //log the page history to the appointment
                if ($cookies.session_id)
                {
                   session_id = $cookies.session_id;
                   $http.get('http://localhost:5000/appointments/'+session_id).then(function(response) {
                       logging_array = response.data['logging_list'];

                       var current_datetime = $filter('date')(new Date(), 'EEE, dd MMM yyyy HH:mm:ss');
                       current_datetime.toString();
                       current_datetime += ' GMT';

                       logging_array.push(
                           {"access_date": current_datetime,
                               "page_name": self.page_name,
                               "url_path": self.location_path});

                       $http.patch('http://localhost:5000/appointments/'+session_id, {
                           logging_list: logging_array
                       }).then(function(response){
                           //success
                       }).catch(function(err) {
                           console.log(err);
                           throw err;
                       });

                       if (response.data['payment_status'] == 'paid')
                       {
                           MyService.reportScreen("keydrop-pickup", "User clicked on confirm button on the pin page and amount is paid.");
                           $location.path('/keydrop-pickup');
                       }
                       else
                       {
                           MyService.reportScreen("payment-scan", "User clicked on confirm button on the pin page and amount is unpaid.");
                           $location.path('/payment-scan');
                       }

                   }).catch(function(err) {
                       throw err;
                   });
                }
                   
                }
            };
        }]
});
