//keyDrop module
angular.module('home', []);
angular.module('services', []);
angular.module('drilldown', []);
angular.module('servicesSignature', []);
angular.module('fordLogIn', []);
angular.module('customerForm', []);
angular.module('licenseScan', []);
angular.module('licenseConfirm', []);
angular.module('moreInfo', []);
angular.module('servicesTally', []);
angular.module('servicesManual', []);
angular.module('servicesManualDescribe', []);
angular.module('servicesManualConfirm', []);
angular.module('keydrop', []);
angular.module('keydropConfirm', []);
angular.module('manualInfoConfirm', []);
angular.module('qrScan', []);
angular.module('pinPage', []);
angular.module('paymentSummary', []);
angular.module('paymentScan', []);
angular.module('pinCreate', []);
angular.module('appointmentComment', []);
angular.module('servicesUpsell', []);
angular.module('pinCreateManual', []);
angular.module('keydropPickup', []);
angular.module('serviceCreation', []);
angular.module('serviceCreationPinPage', []);
angular.module('dashboardSummary', []);
angular.module('outOfOrder', []);
angular.module('adminKeydropForm', []);
angular.module('licenseOrManual', []);
angular.module('adminReturnKey', []);
angular.module('manualMoreInfo', []);
angular.module('dashboardCustomerProfile', []);
var keyDropApp = angular.module('keyDropApp', ['ngRoute', 'ngMaterial', 'ngScrollbars', 'ngIdle', 'home', 'services',
    'drilldown', 'fordLogIn', 'customerForm',
    'licenseScan', 'licenseConfirm', 'moreInfo', 'servicesTally', 'servicesManual', 'servicesManualDescribe',
    'servicesManualConfirm', 'keydrop', 'keydropConfirm', 'servicesSignature', 'manualInfoConfirm', 'qrScan',
    'pinPage', 'paymentSummary', 'paymentScan', 'pinCreate', 'appointmentComment', 'servicesUpsell',
    'pinCreateManual', 'keydropPickup', 'serviceCreation', 'serviceCreationPinPage', 'dashboardSummary', 'outOfOrder',
    'adminKeydropForm', 'licenseOrManual', 'adminReturnKey', 'manualMoreInfo', 'dashboardCustomerProfile']);

var ErrorStackParser = require("error-stack-parser");

// route logic for what path loads what page
keyDropApp.config(function($routeProvider, $provide, KeepaliveProvider, IdleProvider, $httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    //$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    IdleProvider.idle(900);
    IdleProvider.timeout(1);

    $provide.decorator("$exceptionHandler", function ($delegate) {
        var emailAuth = require("./EmailAuth.json");
        return function (exception, cause) {
            var message = "";
            try {
            var stackTrace = ErrorStackParser.parse(exception).toString();
            stackTrace = stackTrace.replace(new RegExp(",", 'g'), ",</li><br><li>");
            stackTrace = stackTrace.replace(new RegExp("@", 'g'), " at <br>");
                message += exception.toString() + "<br>Cause: " + cause + "<br><ul><li>" + stackTrace + "</li></ul>";
            }
            catch(err) {
                message += "<pre>" + JSON.stringify(exception, null, 2) + "</pre>";
            }
            sendMail(emailAuth.adminEmails, emailAuth.user, "Error Log - " + exception.toString(), message);
            $delegate(exception, cause);
        };
    });
    
    $routeProvider

    // logic for the home page
    .when('/home', {
        template: '<home></home>'
    })

    // logic for the services page
    .when('/services', {
        template: '<services></services>'
    })

    // logic for the drilldown page
    .when('/drilldown', {
        template: '<drilldown></drilldown>'
    })

    // logic for the signature page
    .when('/services-signature', {
        template: '<servicesSignature></servicesSignature>'
    })

    // logic for the ford log in page
    .when('/ford-log-in', {
        template: '<fordLogIn></fordLogIn>'
    })

    // logic for the customer form page
    .when('/customer-form', {
        template: '<customerForm></customerForm>'
    })

    // logic for the license scan page
    .when('/license-scan', {
        template: '<licenseScan></licenseScan>'
    })

    // logic for the license confirm page
    .when('/license-confirm', {
        template: '<licenseConfirm></licenseConfirm>'
    })

    // logic for the more info (email,phone) page
    .when('/more-info', {
        template: '<moreInfo></moreInfo>'
    })

    // logic for the services tally page
    .when('/services-tally', {
        template: '<servicesTally></servicesTally>'
    })

    // logic for the manual services page
    .when('/services-manual', {
        template: '<servicesManual></servicesManual>'
    })

    // logic for the manual services describe page
    .when('/services-manual-describe', {
        template: '<servicesManualDescribe></servicesManualDescribe>'
    })

    // logic for the manual services confirm page
    .when('/services-manual-confirm', {
        template: '<servicesManualConfirm></servicesManualConfirm>'
    })

    // logic for the keydrop page
    .when('/keydrop', {
        template: '<keydrop></keydrop>'
    })

    // logic for the keydrop confirm page
    .when('/keydrop-confirm', {
        template: '<keydropConfirm></keydropConfirm>'
    })

    // logic for the manual info confirm page
    .when('/manual-info-confirm', {
        template: '<manualInfoConfirm></manualInfoConfirm>'
    })

    // logic for the qr scan page
    .when('/qr-scan', {
        template: '<qrScan></qrScan>'
    })

    // logic for the pin page
    .when('/pin-page', {
        template: '<pinPage></pinPage>'
    })

    // logic for the payment summary page
    .when('/payment-summary', {
        template: '<paymentSummary></paymentSummary>'
    })

    // logic for the payment scan page
    .when('/payment-scan', {
        template: '<paymentScan></paymentScan>'
    })

    // logic for the pin create page
    .when('/pin-create', {
        template: '<pinCreate></pinCreate>'
    })

    // logic for the appointment comment page
    .when('/appointment-comment', {
        template: '<appointmentComment></appointmentComment>'
    })

    // logic for the services upsell page
    .when('/services-upsell', {
        template: '<servicesUpsell></servicesUpsell>'
    })

    // logic for the pin create manual page
    .when('/pin-create-manual', {
        template: '<pinCreateManual></pinCreateManual>'
    })

    // logic for the keydrop pickup page
    .when('/keydrop-pickup', {
        template: '<keydropPickup></keydropPickup>'
    })

    // logic for the service creation page
    .when('/service-creation', {
        template : '<serviceCreation></serviceCreation>'
    })

    // logic for the service creation pin page
    .when('/service-creation-pin-page', {
        template : '<serviceCreationPinPage></serviceCreationPinPage>'
    })

    // logic for the dashboard summary page
    .when('/dashboard-summary', {
        template : '<dashboardSummary></dashboardSummary>'
    })

     // logic for the out of order page
    .when('/outoforder', {
        template: '<outOfOrder></outOfOrder>'
    })

    // logic for the admin keydrop form page
    .when('/admin-keydrop-form', {
        template: '<adminKeydropForm></adminKeydropForm>'
    })

    // logic for the license or manual page
    .when('/license-or-manual', {
        template: '<licenseOrManual></licenseOrManual>'
    })

    // logic for the admin return key page
    .when('/admin-return-key', {
        template: '<adminReturnKey></adminReturnKey>'
    })

    // logic for the manual track more info (email,phone) page
    .when('/manual-more-info', {
        template: '<manualMoreInfo></manualMoreInfo>'
    })

    // logic for the customer profile page
    .when('/dashboard-customer-profile', {
        template: '<dashboardCustomerProfile></dashboardCustomerProfile>'
    })

    .otherwise('/home');
});

keyDropApp.run(['Idle', function (Idle) {
    Idle.watch();
}]);