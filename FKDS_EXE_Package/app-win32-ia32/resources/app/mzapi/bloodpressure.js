/**
 * Interface to the blood pressure measurement service provided by the platform
 * @namespace bloodpressure
 * @memberof mzero.create
 */
mzero.create.bloodpressure = (function() {
	
	var service = mzero.create.MAPEnv.createService("BloodPressure");
	
	/** @lends mzero.create.bloodpressure */
	var exports = {
		/**
		 * Represents a Request to measure blood pressure.
		 * @name mzero.create.bloodpressure.MeasureBloodPressureRequest
		 * @class
		 * @extends mzero.create.Request
		 * @fires CurrentCuffPressure
		 * @see mzero.create.bloodpressure.measure
		 * @see mzero.create.bloodpressure.CurrentBloodPressureMeasurement
		 * @see mzero.create.bloodpressure.MeasureBloodPressureResult
		 */
		
		/**
		 * Data obtained on the <tt>CurrentCuffPressure</tt> event during execution
		 * of {@link mzero.create.bloodpressure.MeasureBloodPressureRequest}
		 *
		 * @class mzero.create.bloodpressure.CurrentBloodPressureMeasurement
		 * @property {number} CurrentCuffPressure	Systolic pressure currently reported by the device, in mmHg
		 * @see mzero.create.bloodpressure.measure
		 * @see mzero.create.bloodpressure.MeasureBloodPressureRequest
		 * @see mzero.create.bloodpressure.MeasureBloodPressureResult
		 */
		
		/**
		 * Data obtained on successful completion of {@link mzero.create.bloodpressure.MeasureBloodPressureRequest}, which
		 * contains organized information about the platform status
		 *
		 * @class mzero.create.bloodpressure.MeasureBloodPressureResult
		 * @property {number} SystolicBloodPressure Systolic value in mmHg
		 * @property {number} DiastolicBloodPressure Diastolic value in mmHg
		 * @property {number} HeartRate Heart Rate in beats per minute
		 * @property {number} MeanArterialPressure	Mean Arterial Pressure (MAP) in mmHg
		 * @property {string} BPStatus Device-dependent status information
		 * @see mzero.create.bloodpressure.measure
		 * @see mzero.create.bloodpressure.MeasureBloodPressureRequest
		 */
		
		/**
		 * Start the blood measurement request, providing the callback handlers to be used.
		 *
		 * @param eventHandlers {Object} - Event handlers
		 * @param eventHandlers.CurrentCuffPressure {mzero.create.EventCallback} - Invoked as the measurement progresses, whenever partial data is available; see {@link mzero.create.bloodpressure.CurrentBloodPressureMeasurement}
		 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished
		 * @method
		 * @name mzero.create.bloodpressure.MeasureBloodPressureRequest#start
		 */
		
		/**
		 * Call <tt>end()</tt> to indicate that the request should finish as soon as possible, interrupting the operation of the device. This
		 * hurries the end of the request and, if successful, you will get a <tt>Cancelled</tt> error on the <tt>Done</tt> handler.
		 *
		 * <p>Note that, because the requests run asynchronously, after calling this function your request might still succeed or fail with
		 * a reason other than <tt>Cancelled</tt>: the operation might have actually completed by the time you called <tt>end()</tt>.</p>
		 *
		 * @method
		 * @name mzero.create.bloodpressure.MeasureBloodPressureRequest#end
		 */
		 
		/**
		 * Measure blood pressure.
		 *
		 * @param {Number} weight - Weight of the person, which is used internally to determine which measuring method should be used. Whether the number represents kilograms, pounds or some other unit depends on the device configuration
		 *
		 * @return {mzero.create.bloodpressure.MeasureBloodPressureRequest} An asynchronous request to measure blood pressure, ready to be started
		 */
		measure: function(weightInKgs) {
			var self = this;
			var request = service.createRequest("MeasureBloodPressure", { Weight: weightInKgs } )
				.withSignal("end", "CancelMeasureBloodPressure", 0)
				.firingEvents({ BloodPressureProgressCurrentValue:"CurrentCuffPressure" })
				.succeedsOn("MeasureBloodPressureSucceeded")
			;
			return request;
		}
	};
	return exports;
})();
