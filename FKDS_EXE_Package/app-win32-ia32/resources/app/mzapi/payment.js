/**
 * Interface to the MZero Payment service provided by the platform
 *
 * @namespace payment
 * @memberof mzero.create
 */
mzero.create.payment = (function() {
	var defaultCurrency = "USD";
	var service = mzero.create.MAPEnv.createService("MzeroPay");
	
	function createSimplePayRequest(payment, reportData) {
		if(!payment.Currency) payment.Currency = defaultCurrency;
		var payParams = {
			OrderNumber: payment.OrderNumber, Amount: payment.Amount, Currency: payment.Currency,
			KioskId: mzero.create.MAPEnv.PlatformData.MachineId, ReportData: reportData
		};
		var request = service.createRequest("Payment", payParams)
			.succeedsOn("PaymentAccepted")
		;
		return request;
	}
	
	function createSimpleCommitRequest(transactionRef, amount, reportData) {
		var command = {
			TransactionReference: transactionRef,
			Amount: amount,
			ReportData: reportData
		};
		var request = service.createRequest("CommitPayment", command)
			.succeedsOn("PaymentAccepted")
		;
		return request;
	}
	
	function createSimpleCancelRequest(transactionRef, reportData) {
		var command = {
			TransactionReference: transactionRef,
			ReportData: reportData
		};
		var request = service.createRequest("CancelPayment", command)
			.succeedsOn("PaymentAccepted")
		;
		return request;
	}
	
	function convertTransactionTime(obj) {
		var transactionTime = obj.TransactionTime;
		if(transactionTime) {
			obj.TransactionTime = new Date(transactionTime);
		}
		return obj;
	}
	
	/**
	 * @name mzero.create.payment.PaymentParams
	 * @see mzero.create.payment.pay
	 * @class
	 * @property {String} OrderNumber	- A unique (for each transaction) integer number which is created by the client application
	 * @property {String} Currency	- Optional: one of <code>USD</code> (the default) or <code>CAD</code>.
	 * @property {number} Amount	- The amount to obtain authorization for
	 * @property {mzero.create.APIReport} APIReport	- Optional: application data to include in an API report
	 */
	 
	/**
	 * @class mzero.create.payment.PaymentTransaction
	 * @property {string} StatusMessage
	 * @property {string} AuthReference
	 * @property {string} ApprovalCode
	 * @property {string} TransactionId
	 * @property {Date} TransactionTime
     * @property {boolean} SignatureVerificationRequired
     * @property {boolean} SignatureCaptured
     * @property {string} CardEaseReference
     * @property {string} CardHash
     * @property {string} CardReference     
     * @property {string} TotalAmount
     * @property {string} PanMasked 
     * @property {string} ExpiryDate
     * @property {string} MerchantName
     * @property {string} CardScheme
     * @property {string} TransactionSource
     * @property {string} TransactionType
     * @property {string} CardholderVerification     
     * @property {string} MerchantIdMasked
     * @property {string} TerminalIdMasked     
	 */
	
	/**
	 * Once started, the request will be sent to the platform and, if approved, you will have the option between aborting the
	 * payment and committing it for a different amount (as long as it is smaller or equal to the one you were authorized for).
	 * @class mzero.create.payment.PaymentRequest
	 * @property {mzero.create.payment.PaymentTransaction} PaymentTransaction	Data obtained during authorization (available after the <tt>Confirm</tt> event handler provided in {@link mzero.create.payment.PaymentRequest#start} is called back)
	 * @property {boolean} Started	the start() method has been called on the request
	 * @property {boolean} ConfirmationDemanded	- The platform has successfully authorized the payment, and asked the client application to confirm or abort it.
	 * @property {boolean} ConfirmationResolved	- The client application replied to the <code>Confirm</code> confirmation demand by using either {@link mzero.create.payment.PaymentRequest#commit} or {@link mzero.create.payment.PaymentRequest#abort}; see {@link mzero.create.payment.PaymentRequest#start}
	 * @property {mzero.create.payment.PaymentResult} Result	Final operation result, available when the 'Done' event handler is invoked
	 * @property {number} AuthorizedAmount	The amount provided in the original call to {@link mzero.create.payment.pay}
	 * @property {number} CommittedAmount	The amount provided when {@link mzero.create.payment.PaymentRequest#commit} was called
	 */
	/**
	 * A payment request handled by MZeroPay
	 * @param {mzero.create.payment.PaymentParams} params - Payment parameters
	 * @private
	 */
	function PaymentRequest(params) {
		this.payment = params;
		this.AuthorizedAmount = params.Amount;
		this._confirmHandler = null;
		this._doneHandler = null;
	}
	/**
	 * Start the payment request, providing the callback handlers to be used.
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.Confirm {mzero.create.EventCallback}	- Invoked once the result of the payment auhorization is known. After this event, you can invoke either {@link mzero.create.payment.PaymentRequest.commit} or {@link mzero.create.payment.PaymentRequest.abort} to decide the fate of the payment
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished
	 * @method
	 * @name mzero.create.payment.PaymentRequest#start
	 */
	PaymentRequest.prototype.start = function(eventHandlers) {
		if(eventHandlers) {
			this._confirmHandler = eventHandlers.Confirm;
			this._doneHandler = eventHandlers.Done;
		}
		if(!this._confirmHandler) {
			throw new mzero.create.MAPException("PaymentRequest requires a 'Confirm' handler");
		}
		this._reportData = mzero.create.MAPEnv._buildReportData(this.payment.APIReport);
		var myself = this;
		createSimplePayRequest(this.payment, this._reportData).start(
			{
				Done: function(err, data) {
					if(err) {
						myself._doneHandler(err, data);
						return;
					}
					myself.PaymentTransaction = convertTransactionTime(data.PaymentTransaction);
					myself.ConfirmationDemanded = true;
					myself._confirmHandler(err, data);
				}
			}
		);
		this.Started = true;
	}
	/**
	 * Once the platform has asked for payment confirmation (by invoking the <tt>Confirm</tt> event handler you provided in {@link mzero.create.payment.PaymentRequest#start}),
	 * use this function to commit the payment, indicating the amount you wish to charge the user.
	 *
	 * @method
	 * @name mzero.create.payment.PaymentRequest#commit
	 * @param {number} amount - The amount to actually charge
	 * @see mzero.create.payment.PaymentRequest#start
	 * @see mzero.create.payment.PaymentRequest#abort
	 */
	PaymentRequest.prototype.commit = function(amount) {
		this.checkSecondStageSanity("commit");
		this.CommittedAmount = amount;
		var myself = this;
		createSimpleCommitRequest(myself.PaymentTransaction.AuthReference, amount, this._reportData).start(
			{
				Done: function(err, data) {
					if(!myself._doneHandler) return;
					myself.Result = convertTransactionTime(data.PaymentTransaction);
					myself._doneHandler(err, data); // TODO: Some postprocessing might be needed
				}
			}
		);
		this.ConfirmationResolved = true;
	};
	/**
	 * Once the platform has asked for payment confirmation (by invoking the <tt>Confirm</tt> event handler you provided in {@link mzero.create.payment.PaymentRequest#start}),
	 * use this function to abort the payment.
	 *
	 * <p>If the payment is successfully aborted after calling this function, the request's <tt>Done</tt> handler will be invoked with an <tt>Aborted</tt> error code.
	 * </p>
	 *
	 * @method
	 * @name mzero.create.payment.PaymentRequest#commit
	 * @see mzero.create.payment.PaymentRequest#start
	 */
	PaymentRequest.prototype.abort = function() {
		this.checkSecondStageSanity("abort");
		this.Aborting = true;
		var myself = this;
		createSimpleCancelRequest(this.PaymentTransaction.AuthReference, this._reportData).start(
			{
				Done: function(err, data) {
					if(!myself._doneHandler) return;
					if(err) {
						// An error occurred during cancellation
						myself.Result = convertTransactionTime(data.PaymentTransaction);
						myself._doneHandler(err, data); 
					} else {
						// Successful cancellation
						myself._doneHandler("Aborted", data);
					}
				}
			}
		);
		this.ConfirmationResolved = true;
	};
	PaymentRequest.prototype.checkSecondStageSanity = function(label) {
		if(!this.Started) {
			throw new mzero.create.MAPException("Cannot " + label + " a payment request that has not been started");
		}
		if(!this.ConfirmationDemanded) {
			throw new mzero.create.MAPException("Cannot " + label + " a payment request that has not been confirmed by the platform");
		}
		if(this.ConfirmationResolved) {
			throw new mzero.create.MAPException("Cannot " + label + " a payment request whose confirmation has been resolved");
		}
	}
	
	/**
	 * @class mzero.create.payment.PaymentResult
	 * @property {String} AuthorizationReference
	 */
	
	/**
	 * @lends mzero.create.payment
	 */
	var exports = {
		/**
		 * The pay function returns a Request object that controls the payment process.
		 *
		 * @param {mzero.create.payment.PaymentParams} params - Parameters
		 * @return {mzero.create.payment.PaymentRequest} Request ready to be started
		 * @see mzero.create.payment.PaymentResult
		 */
		pay: function(params) {
			return new PaymentRequest(params);
		}
	};
	return exports;
})();
