/**
 * Interface to the Cash service provided by the platform
 *
 * @namespace cash
 * @memberof mzero.create
 */
mzero.create.cash = (function() {
	var defaultCurrency = "USD";
	var MAPEnv = mzero.create.MAPEnv;
	var service = MAPEnv.createService("Cash");
	
	/**
	 * <p>
	 * A request to initiate a cash deposit operation. As other requests, it is
	 * initiated when you call its {@link mzero.create.cash.CheckStockRequest#start} method.
	 * </p>
	 *
	 * @extends mzero.create.Request
	 * @see mzero.create.cash.checkStock
	 * @see mzero.create.cash.CheckStockResult
	 * @class mzero.create.cash.CheckStockRequest
	 */
	
	/**
	 * Start the check stock request, providing the callback handlers to be used.
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. In case of error, the <tt>err</tt> value will indicate the reason (such as <tt>HardwareError</tt>). If successful, the <tt>data</tt> contains the result.
	 * @method
	 * @name mzero.create.cash.CheckStockRequest#start
	 */
	
	/**
	 * @class mzero.create.cash.CheckStockResult
	 * @property {mzero.create.cash.ProcessedStockItems} CashBox - Data on the contents of the cash box device
	 * @property {mzero.create.cash.ProcessedStockItems} Stock - Data on the contents of the stock dispensing device
	 * @property {number} TotalAmount - Total amount stored on the cash devices
	 */
	 
	/**
	 * @class mzero.create.cash.StockItemData
	 * @see mzero.create.cash.ProcessedStockItems
	 * @see mzero.create.cash.CheckStockResult
	 * @see mzero.create.cash.CashReturnedData
	 * @property {string} Name - Human readable description of the item
	 * @property {string} Type - One of <code>Bill</code>, <code>Coin</code>
	 * @property {number} Value - Denominational value of each stock item in this category
	 * @property {number} Count - Number of items of the specified value
	 */
	
	/**
	 * Processes an array of {@link mzero.create.cash.StockItemData} objects and provides simpler methods to access
	 * the information contained in it.
	 *
	 * @see mzero.create.cash.ProcessedStockItems
	 * @see mzero.create.cash.CheckStockResult
	 * @class mzero.create.cash.ProcessedStockItems
	 * @property {number} ItemCount - Total number of items (bills, coins) contained in the device
	 * @property {number} Amount - Total value of the items contained in the device
	 * @property {Object.<number, number>} DenominationMap - Associative array that maps a denomination value to the number of items of that value contained in the device. you might want to use {@link mzero.create.cash.ProcessedStockItems#itemQuantityInDenomination} instead.
	 */
	function ProcessedStockItems(stockItems/*: Array<StockItemData>*/)
	{
		this.RawStockItems = (stockItems != null)? stockItems : [];
		this._organize();
	}
	
	ProcessedStockItems.prototype._organize = function()
	{
		var itemCount = 0;
		var amount = 0;
		var digestedItems = this;
		this.DenominationMap = { };
		this.RawStockItems.forEach(function(stockItem, index, array) {
			itemCount += stockItem.Count;
			amount += stockItem.Value * stockItem.Count;
			digestedItems._countItemInItsDenomination(stockItem);
		});
		this.ItemCount = itemCount;
		this.Amount = amount;
	}
	
	function standardizeDenomination(d) {
		var result = parseFloat(d).toString();
		var decimalPoint = result.indexOf('.');
		if(decimalPoint < 0) {
			return result;
		}
		result = parseFloat(d).toFixed(2);
		return result;
	}
	
	ProcessedStockItems.prototype._countItemInItsDenomination = function(stockItem/*:StockItemData*/)
	{
		var key = standardizeDenomination(stockItem.Value);
		var currentDenominationCount = this.DenominationMap[key];
		if (currentDenominationCount == null) {
			currentDenominationCount = stockItem.Count;
		} else {
			currentDenominationCount += stockItem.Count;
		}
		this.DenominationMap[key] = currentDenominationCount;
	}
	
	/**
	 * Convenience function to query the denomination map.
	 *
	 * @param	denomination {number} - Denominational value being inquired
	 * @return If the specified denomination is available in the device, returns the number of items in the denomintation; otherwise, it returns 0 (zero)
	 * @method
	 * @name mzero.create.cash.ProcessedStockItems#itemQuantityInDenomination
	 */
	ProcessedStockItems.prototype.itemQuantityInDenomination = function(denomination/*:Number*/)
	{
		var key = denomination;
		var denominationCount = this.DenominationMap[key];
		return (denominationCount != null)? denominationCount : 0;
	}
	
	/**
	 * The policies supported by a deposit operation.
	 * <dl>
	 * <dt><code>Default</code></dt>
	 * <dd>The default behavior: the kiosk ensures that it has enough funds to both refund and give change for a deposit (except when the kiosk is
	 * in the single bill acceptor configuration).</dd>
	 * <dt><code>AvoidOverpayment</code></dt>
	 * <dd>The kiosk avoids exceeding the target deposit amount and returns notes that exceed the deposit amount</dd>
	 * </dl>
	 * @typedef {('Default'|'AvoidOverpayment')} mzero.create.cash.SimpleDepositPolicy
	 * @see mzero.create.cash.DepositOptions
	 * @see mzero.create.cash.DepositParams
	 */
	/**
	 * @name mzero.create.cash.DepositOptions
	 * @see mzero.create.cash.DepositParams
	 * @class
	 * @property {mzero.create.cash.SimpleDepositPolicy[]} Policies	- Optional array specifying the policies to apply during the deposit
	 */
	/**
	 * @name mzero.create.cash.DepositParams
	 * @see mzero.create.cash.deposit
	 * @class
	 * @property {String} Currency	- Optional: specify one of <code>USD</code> (the default) or <code>CAD</code>
	 * @property {number} Amount	- The desired deposit amount
	 * @property {mzero.create.cash.DepositOptions} Options	- Optional parameters for the deposit
	 * @property {mzero.create.APIReport} APIReport	- Optional: application data to include in an API report
	 */
	 
	/**
	 * <p>
	 * A request to initiate a cash deposit operation. As other requests, it is
	 * initiated when you call its {@link mzero.create.cash.DepositRequest#start} method.
	 * </p>
	 * 
	 * <p>The main points to consider when implementing a deposit operation are:</p>
	 * 
	 * <ul>
	 * <li>
	 * When a deposit starts, the devices will start accepting cash from the
	 * user and later on they will stop doing so: either the deposit amount
	 * requested was reached, the user stopped depositing cash and the deposit
	 * timeout was reached, or some other condition happened (for example, if
	 * a device can only hold 15 bills, acceptance will stop at that point).
	 * </li>
	 * 
	 * <li>
	 * <p>
	 * If cash acceptance stops normally (the requested amount was reached or surpassed,
	 * or the user stopped inserting cash), or an error occurred after the user has tendered
	 * some money, you will get a <code>Confirm</code>
	 * event informing you on the amount inserted . Depending on the circumstances, this could
	 * be more or less money than the amount you requested and you need to tell the platform
	 * how to proceed next: after the <code>Confirm</code> event, call either <code>commit()</code> to accept the cash, or
	 * <code>abort()</code> to initiate a refund.
	 * </p>
	 * </li>
	 * 
	 * <li>
	 * <p>
	 * If cash acceptances stops immediately because of an error such as a hardware problem or low cash funds, and no
	 * money has been taken in from the user, you will not
	 * get the <code>Confirm</code> event; instead, you will get the <code>Done</code> event directly.
	 * </p>
	 * </li>
	 * 
	 * <li>
	 * When the platform finishes a deposit request, the <code>Done</code> event will give
	 * you the definitive information on the amount deposited and taken from the user:
	 * <ul>
	 * <li>If the <code>err</code> value is null, there will be <code>DepositData</code> in the
	 * <code>data</code> value and you need to verify that the deposited amount matches your expectations because
	 * the user might have inserted less cash than you requested. For example, one way this can happen is when you
	 * use <code>end()</code> to move the deposit to the confirmation phase.
	 * </li>
	 * 
	 * <li>If the <code>err</code> value is NOT null, you still need to look for <code>DepositData</code> in the
	 * <code>data</code> value: it could be that you decided to <code>abort()</code> a deposit but the user could not
	 * be refunded in full and you will get transaction information because money is owed to the user. In this case,
	 * your application will have to credit the user.
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * 
	 * <p>
	 * To sum it up, deposit is a complicated operation because it interacts with the user and various hardware devices
	 * which can fail in different ways during the process:
	 * <ul>
	 * <li>If it is known early on that the operation cannot be completed, then your <code>Done</code> handler will be
	 * called right away with an error code (i.e. there will be no request for confirmation).
	 * </li>
	 * <li>
	 * If it happens later, then you get an error back if no money went into the
	 * device, but if money went in partially or fully, then you get a
	 * transaction back to reflect what actually happened.
	 * </li>
	 * <li>
	 * Even if you decide to abort a deposit, it is possible that the money cannot be
	 * returned. Therefore, even if you know that <code>abort()</code> was called on a
	 * deposit request, when it is DONE you still need to check if it contains
	 * <code>DepositData</code> information.
	 * </li>
	 * </ul>
	 * </p>
	 *
	 * @class mzero.create.cash.DepositRequest
	 * @property {boolean} ConfirmationResolved	- The client application replied to the <code>Confirm</code> confirmation demand by using either {@link mzero.create.cash.DepositRequest#commit} or {@link mzero.create.cash.DepositRequest#abort}; see {@link mzero.create.cash.DepositRequest#start}
	 * @property {string} ConfirmedDecision - When <code>ConfirmationResolved</code> is <code>true</code>, <code>ConfirmedDecision</code> is one of <code>Commit</code>, <code>Abort</code> depending on what function was called on the request
	 */
	
	/**
	 * Start the deposit request, providing the callback handlers to be used.
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.Accepting {mzero.create.EventCallback} - Fired when the service is ready to take cash from the user. This stage can end when <code>Confirm</code> or <code>Done</code> are fired
	 * @param eventHandlers.ProcessingCash {mzero.create.EventCallback} - Fired when the service processes a cash item tendered by the user; it might not be able to take more cash items until the <code>Accepting</code> event is fired again.
	 * @param eventHandlers.CashIn {mzero.create.EventCallback} - Fired when a bill or coin has been processed and is being stored in a cash device. The <code>data</code> value describes the item and it is of type {@link mzero.create.cash.CashInData}
	 * @param eventHandlers.BillRefused {mzero.create.EventCallback} - Fired when a bill or object has been returned from the acceptor device because it cannot be recognized as a valid cash item
	 * @param eventHandlers.CashReturned {mzero.create.EventCallback} - Fired when an bill or coin inserted into the machine cannot be taken (likely when the service cannot guarantee that the whole deposit can be refunded or that change can be given). The <code>data</code> value describes the item and it is of type {@link mzero.create.cash.CashReturnedData}
	 * @param eventHandlers.Confirm {mzero.create.EventCallback} - Fired when the application needs to make a decision regarding the deposit; the <code>data</code> value is of type {@link mzero.create.cash.ConfirmDepositData}. At any time after this event, you can decide to either keep the amount already tendered (calling {@link mzero.create.cash.DepositRequest#commit}) or refund the user (calling {@link mzero.create.cash.DepositRequest#abort}); the Cash service will keep waiting for your decision.
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. In case of error, the <tt>err</tt> value will indicate the reason (such as <tt>InsufficientFunds</tt> if there is not enough cash in the machine to guarantee that the user can be refunded or receive change). In any case, if <tt>data</tt> is not null, it is of type {@link mzero.create.cash.DepositResult} and contains transaction information if the kiosk kept money that belongs to the user.
	 * @method
	 * @name mzero.create.cash.DepositRequest#start
	 */
	
	/**
	 * Information provided in a <code>CashIn</code> event callback when an accepted cash item (bill or coin) is being taken.
	 * @class mzero.create.cash.CashInData
	 * @property {number} DepositedAmount - Running amount of the deposit so far
	 * @property {mzero.create.cash.StockItemData} StockItem - Information about the item being taken
	 */
	
	/**
	 * Information provided in a <code>CashReturned</code> event callback when a cash item (bill or coin) tendered by the user cannot be taken.
	 * @class mzero.create.cash.CashReturnedData
	 * @property {Array.<string>} Reasons - Reasons why the cash item cannot be accepted. It can contain <code>CannotRefundDeposit</code>, <code>CannotDispenseChange</code> or both.
	 * @property {Array.<mzero.create.cash.StockItemData>} AcceptableDenominations - The denominations which can be accepted instead at this time
	 * @see mzero.create.cash.DepositRequest#start
	 */
	
	/**
	 * End a deposit by moving it to its confirmation stage, during which you will get
	 * an opportunity to abort() or commit() the amount collected so far. Use this function
	 * if you need to terminate the ongoing deposit sooner that it otherwise would.
	 * 
	 * <p>Note that because of the asynchronous nature of the platform, the operation might have already
	 * either finished or reached the confirmation stage at the time of this call. If you get the
	 * <code>Confirm</code>, you still need to make a decision regarding the outcome of the deposit.</p>
	 * 
	 * @see mzero.create.cash.DepositRequest#start
	 * @method
	 * @name mzero.create.cash.DepositRequest#end
	 */
	
	/**
	 * @class mzero.create.cash.ConfirmDepositData
	 * @property {number} Amount - Amount actually tendered by the user during the deposit. It might be 0 (zero) if the user did not tender any cash items.
	 * @see mzero.create.cash.DepositRequest#start
	 */
	
	/**
	 * @class mzero.create.cash.ConfirmWithdrawData
	 * @property {number} Amount - Amount available for withdrawal.
	 * @see mzero.create.cash.WithdrawRequest#start
	 */
	
	/**
	 * Once the <code>Confirm</code> event has been fired on the request, call this function to keep the tendered funds and start dispensing change to the user.
	 * <p><b>IMPORTANT</b>: Note that the change dispensal process might still fail, resulting on the machine owing money to the user. To identify this problem, check the
	 * final transaction result that you will get when processing the <code>Done</code> event.
	 * @see mzero.create.cash.DepositRequest#start
	 * @see mzero.create.cash.DepositRequest#abort
	 * @method
	 * @name mzero.create.cash.DepositRequest#commit
	 */
	
	/**
	 * Once the <code>Confirm</code> event has been fired on the request, call this function to return the tendered funds to the user.
	 * <p><b>IMPORTANT</b>: Note that the refund process might still fail, resulting on the machine owing money to the user. To identify this problem, check the
	 * final transaction result that you will get when processing the <code>Done</code> event.
	 * @see mzero.create.cash.DepositRequest#start
	 * @see mzero.create.cash.DepositRequest#commit
	 * @method
	 * @name mzero.create.cash.DepositRequest#abort
	 */
	
	/**
	 * @class mzero.create.cash.DepositResult
	 * @property {mzero.create.cash.CashTransactionLog} Transaction - Reported information about the cash transaction
	 * @see mzero.create.cash.deposit
	 */
	
	/**
	 * @class mzero.create.cash.CashTransactionLog
	 * @property {string} Operation - Name of the platform operation performed: <code>Deposit</code>, <code>Withdraw</code>, etc
	 * @property {string} CreditType - One of <code>Credit</code>, <code>Debit</code>, <code>Info</code>
	 * @property {number} Amount - Total value of the operation
	 * @property {string} TransactionId - System identification under which the transaction has been recorded
	 * @see mzero.create.cash.DepositResult
	 * @see mzero.create.cash.WithdrawResult
	 */
	
	/**
	 * @name mzero.create.cash.WithdrawParams
	 * @see mzero.create.cash.withdraw
	 * @class
	 * @property {String} Currency	- Optional: specify one of <code>USD</code> (the default) or <code>CAD</code>
	 * @property {number} Amount	The amount to withdraw
	 * @property {mzero.create.APIReport} APIReport	- Optional: application data to include in an API report
	 */
	
	/**
	 * <p>
	 * A request to initiate a cash withdrawal operation. As other requests, it is
	 * initiated when you call its {@link mzero.create.cash.WithdrawRequest#start} method.
	 * </p>
	 * @class mzero.create.cash.WithdrawRequest
	 * @property {boolean} ConfirmationResolved	- The client application replied to the <code>Confirm</code> confirmation demand by using either {@link mzero.create.cash.WithdrawRequest#commit} or {@link mzero.create.cash.WithdrawRequest#abort}; see {@link mzero.create.cash.WithdrawRequest#start}
	 * @property {string} ConfirmedDecision - When <code>ConfirmationResolved</code> is <code>true</code>, <code>ConfirmedDecision</code> is one of <code>Commit</code>, <code>Abort</code> depending on what function was called on the request
	 */
	
	/**
	 * Start the withdraw request, providing the callback handlers to be used.
	 *
	 * @param eventHandlers {Object} - Event handlers
	 * @param eventHandlers.Confirm {mzero.create.EventCallback} - Fired when the application needs to make a decision regarding the withdrawal; the <code>data</code> value is of type {@link mzero.create.cash.ConfirmWithdrawData}. At any time after this event, you can decide to either proceed with the withdrawal (calling {@link mzero.create.cash.WithdrawRequest#commit}) or abort it (calling {@link mzero.create.cash.WithdrawRequest#abort}); the Cash service will keep waiting for your decision.
	 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. In case of error, the <tt>err</tt> value will indicate an error reason. In any case, if <tt>data</tt> is not null, it is of type {@link mzero.create.cash.WithdrawResult} and contains transaction information if the kiosk kept money that belongs to the user.
	 * @method
	 * @name mzero.create.cash.WithdrawRequest#start
	 */
	
	/**
	 * Once the <code>Confirm</code> event has been fired on the request, call this function to dispense the funds being withdrawn.
	 * <p><b>IMPORTANT</b>: Note that the withdraw process might still fail. To identify this problem, check the
	 * final transaction result that you will get when processing the <code>Done</code> event.
	 * @see mzero.create.cash.WithdrawRequest#start
	 * @see mzero.create.cash.WithdrawRequest#abort
	 * @method
	 * @name mzero.create.cash.WithdrawRequest#commit
	 */
	
	/**
	 * Once the <code>Confirm</code> event has been fired on the request, call this function to terminate the request without dispensing any funds.
	 *
	 * @see mzero.create.cash.WithdrawRequest#start
	 * @see mzero.create.cash.WithdrawRequest#commit
	 * @method
	 * @name mzero.create.cash.WithdrawRequest#abort
	 */
	
	/**
	 * @class mzero.create.cash.WithdrawResult
	 * @property {mzero.create.cash.CashTransactionLog} Transaction - Reported information about the cash transaction
	 * @see mzero.create.cash.withdraw
	 */
	
	/**
	 * @lends mzero.create.cash
	 */
	var exports = {
		/**
		 * The check stock function creates a Request object that controls the process.
		 *
		 * @return {mzero.create.cash.CheckStockRequest} Request ready to be started
		 * @see mzero.create.cash.CheckStockResult
		 */
		checkStock: function() {
			var code = defaultCurrency;
			var request = service.createRequest("CheckStock", { "Currency" : code } )
				.succeedsOn("StockDetails")
				.transformResultUsing(function(data) {
					var result = { };
					result.CashBox = new ProcessedStockItems(data.CashBoxItems);
					result.Stock = new ProcessedStockItems(data.StockItems);
					result.TotalAmount = data.StockAmount + data.CashBoxAmount;
					return result;
				})
			;
			return request;
		},
		
		/**
		 * The deposit function returns a Request object that controls the process.
		 *
		 * @param {mzero.create.cash.DepositParams} params - Parameters
		 * @return {mzero.create.cash.DepositRequest} Request ready to be started
		 * @see mzero.create.cash.DepositRequest
		 * @see mzero.create.cash.DepositResult
		 */
		deposit: function(params) {
			var currency = params.Currency || defaultCurrency;
			var options = params.Options || {};
			var request = service.createRequest("Deposit", { Currency: currency, ReportData: MAPEnv._buildReportData(params.APIReport), Amount: params.Amount, Options: options })
				.withSignal("end", function() {
					request._sendSignal(["EndDeposit", { }]);
				}, 0)
				.withSignal("commit", function() {
					if(request.ConfirmationResolved) {
						throw new Error("Cannot commit() a deposit that has been resolved");
					}
					request._sendSignal(["CommitDeposit", { "Currency" : currency }]);
					request.ConfirmationResolved = true;
					request.ConfirmedDecision = "Commit";
				}, 0)
				.withSignal("abort", function() {
					if(request.ConfirmationResolved) {
						throw new Error("Cannot abort() a deposit that has been resolved");
					}
					request._sendSignal(["CancelDeposit", { "Currency" : currency }]);
					request.ConfirmationResolved = true;
					request.ConfirmedDecision = "Abort";
				}, 0)
				.firingEvents({
					"AcceptingCash" : "Accepting",
					"CashBillRefused" : "CashRefused",
					"CashIn" : "CashIn",
					"CashReturned" : "CashReturned",
					"ProcessingCash" : "ProcessingCash"
				})
				.convertingResponsesToEvents({
					"ConfirmDeposit" : "Confirm",
					"EndDeposit" : ''
				})
				.convertingErrorCodes( { "Cancelled" : "Aborted" } )
				.succeedsOn("DepositSucceeded")
			;
			request.ConfirmationResolved = undefined;
			request.ConfirmedDecision = undefined;
			return request;
		},

		/**
		 * The withdraw function returns a Request object that controls the process
		 *
		 * @param {mzero.create.cash.WithdrawParams} params - Parameters
		 * @return {mzero.create.cash.WithdrawRequest} Request ready to be started
		 * @see mzero.create.cash.WithdrawRequest
		 * @see mzero.create.cash.WithdrawResult
		 */
		withdraw: function(params) {
			var currency = params.Currency || defaultCurrency;
			var request = service.createRequest("Withdraw", { Currency: currency, ReportData: MAPEnv._buildReportData(params.APIReport), Amount: params.Amount })
				.withSignal("commit", function() {
					if(request.ConfirmationResolved) {
						throw new Error("Cannot commit() a withdraw that has been resolved");
					}
					request._sendSignal(["CommitWithdraw", { "Currency" : currency }]);
					request.ConfirmationResolved = true;
					request.ConfirmedDecision = "Commit";
				}, 0)
				.withSignal("abort", function() {
					if(request.ConfirmationResolved) {
						throw new Error("Cannot abort() a withdraw that has been resolved");
					}
					request._sendSignal(["CancelWithdraw", { }]);
					request.ConfirmationResolved = true;
					request.ConfirmedDecision = "Abort";
				}, 0)
				.convertingResponsesToEvents({
					"ConfirmWithdraw" : "Confirm"
				})
				.convertingErrorCodes( { "WithdrawCancelled" : "Aborted" } )
				.succeedsOn("WithdrawSucceeded")
			;
			request.ConfirmationResolved = undefined;
			request.ConfirmedDecision = undefined;
			return request;
		}
	};
	return exports;
})();
