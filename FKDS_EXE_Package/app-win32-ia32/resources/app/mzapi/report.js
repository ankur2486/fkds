/**
 * Interface to the Report service provided by platform, which lets you create custom reports
 * @namespace report
 * @memberof mzero.create
 */
mzero.create.report = (function() {
	var service = mzero.create.MAPEnv.createService("Report");
	function padTwoDigits(n)
	{
		return (n < 10)? '0' + n : n;
	}
	
	function formatLocalTimestamp(date/*Date*/)/*String*/
	{
		if(!date) return null;
		var result = date.getFullYear() +
        		'-' + padTwoDigits(date.getMonth() + 1) +
        		'-' + padTwoDigits(date.getDate()) +
        		' ' + padTwoDigits(date.getHours()) +
        		':' + padTwoDigits(date.getMinutes()) +
        		':' + padTwoDigits(date.getSeconds()) +
        		'.' + (date.getMilliseconds() / 1000).toFixed(3).slice(2, 5)
        	;
		return result;
	}
	

	function convertToKeyValueArray(keysValues) {
		if(!keysValues) return null;
		var result = [];
		Object.getOwnPropertyNames(keysValues).forEach(function(element,index,array) {
			var value = keysValues[element];
			if(value && Object.prototype.toString.call(value) === '[object Date]') {
				result.push( { "Key" : element, "Value" : formatLocalTimestamp(value) } );
			} else {
				result.push( { "Key" : element, "Value" : value } );
			}
		});
		return result;
	};
	/** @lends mzero.create.report */
	var exports = {
		/**
		 * Represents a Request to send a custom report to the management server
		 * @name mzero.create.report.CustomReportRequest
		 * @class
		 * @extends mzero.create.Request
		 */
		/**
		 * Sends a custom report to the management server.
		 * <p>
		 * Currently the name and data structure need to be defined in advance. The reported data ends up stored in a SQL table called "dynamic_" + reportName and the keys
		 * in the report become the column names. Some key names are reserved (<code>kiosk_id</code>, <code>report_time</code>, <code>processed_time</code>) and should not
		 * be used from your code; the platform fills them in as needed.
		 * </p>
		 *
		 * @param {String} reportName - Name of the report (must be already defined in your customer account)
		 * @param {Object} keysValues - Mapping containing the key/value pairs to be reported, according to the definition in your customer account
		 * @return {mzero.create.report.CustomReportRequest}
		 */
		send: function(reportName, keysValues) {
			var keyValueArray = convertToKeyValueArray(keysValues);
			var request = service.createRequest("SendReport", { ReportName: reportName, Entries: keyValueArray })
				.succeedsOn("SendReportSucceeded")
			;
			return request;
		},
		/**
		 * Represents a Request to announce a change in a property managed by this application
		 * @name mzero.create.report.AnnouncePropertyChangeRequest
		 * @class
		 * @extends mzero.create.Request
		 */
		/**
		 * Announce a change in the value of a global property maintained by your application
		 * @param {String} Property name
		 * @param {Object} New value, which will be converted to a string for announcement
		 * @return {mzero.create.report.AnnouncePropertyChangeRequest}
		 */
		announcePropertyChange: function(propertyName, propertyValue) {
			var request = service.createRequest("AnnounceReportPropertyChange", { Name: propertyName, Value : propertyValue && propertyValue.toString() || null })
				.succeedsOn("AnnounceReportPropertyChangeSucceeded")
			;
			return request;
		},
		"_formatLocalTimestamp" : formatLocalTimestamp,
		"_receiveProperties" : function(properties) {
			var request = service.createRequest("ReceiveReportPropertyChanges", { Patterns: properties }, { TagPersists: true })
				.succeedsOn("ReceiveReportPropertyChangesSucceeded")
				.withSignal("end", "ReceiveReportPropertyChanges", 0)
				.firingEvents({ ReportPropertyChangeEvent: "PropertyChange" })
			;
			return request;
		}
	};
	return exports;
})();
