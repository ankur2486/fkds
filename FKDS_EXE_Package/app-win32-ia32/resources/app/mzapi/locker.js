/**
 * Interface to the locker service functions provided by the platform
 * @namespace locker
 * @memberof mzero.create
 */
mzero.create.locker = (function() {
	
	var MAPEnv = mzero.create.MAPEnv;
	var service = mzero.create.MAPEnv.createService("Locker");
	
	/** @lends mzero.create.locker */
	var exports = {
		/**
		 * Represents a Request to check all the lockers and their current state
		 * @name mzero.create.locker.CheckLockersRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.locker.checkLockers
		 * @see mzero.create.locker.CheckLockersResult
		 */
		/**
		 * Start a check lockers request, providing the callback handlers to be used.
		 *
		 * @param eventHandlers {Object} - Event handlers
		 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. If successful, the <code>err</code> parameter will be null and the <code>data</code> parameter will be of type {@link mzero.create.locker.CheckLockersResult}; otherwise, <code>err</code> will contain a string explaining the reason (such as <code>HardwareFailure</code>).
		 * @method
		 * @name mzero.create.locker.CheckLockersRequest#start
		 */
		/**
		 * @name mzero.create.locker.CheckLockersResult
		 * @class
		 * @property {mzero.create.locker.LockerEntry[]} Lockers - Array containing the state and id of each locker installed on the machine.
		 * @see mzero.create.locker.checkLockers
		 */
		/**
		 * @name mzero.create.locker.LockerEntry
		 * @class
		 * @property {int} Id - Uniquely identifies the locker in the machine
		 * @property {string} Status - One of <code>Unknown</code>, <code>Available</code>, <code>Taken</code>
		 * @see mzero.create.locker.CheckLockersResult
		 * @see mzero.create.locker.checkLockers
		 */
		/**
		 * Check state of all lockers installed on the machine
		 *
		 * @return {mzero.create.locker.CheckLockersRequest} An asynchronous request, ready to be started
		 * @see mzero.create.locker.CheckLockersResult
		 */
		checkLockers: function() {
			var request = service.createRequest("CheckLockers", { } )
				.succeedsOn("CheckLockersSucceeded")
			;
			return request;
		},

		/**
		 * Parameters to a store operation
		 * @name mzero.create.locker.StoreParams
		 * @class
		 * @property {int} Locker - Id of the locker where the user can store their items
		 * @property {mzero.create.APIReport} APIReport	- Optional: application data to include in the API report; if not specified, default data will be provided
		 * @see mzero.create.locker.store
		 */
		
		/**
		 * Represents a Request to store item(s) in a locker
		 * @name mzero.create.locker.StoreRequest
		 * @class
		 * @extends mzero.create.Request
		 * @property {int} Locker - Id of the locker where the user stores their items during this request
		 * @fires Storing
		 * @see mzero.create.locker.store
		 */
		
		/**
		 * Start a store request, providing the callback handlers to be used.
		 *
		 * @param eventHandlers {Object} - Event handlers
		 * @param eventHandlers.Storing {mzero.create.EventCallback} - Invoked once the locker is waiting for the item to be stored; for example, after any installed locks have been opened
		 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. If successful, the <code>err</code> parameter will be null; otherwise it will contain a string explaining the reason (such as <code>Timeout</code>).
		 * @method
		 * @name mzero.create.locker.StoreRequest#start
		 */
		
		/**
		 * Call <tt>end()</tt> to indicate that the request should finish as soon as possible, interrupting the current wait. This
		 * hurries the end of the request and, if successful, you will get a <tt>Cancelled</tt> error on the <tt>Done</tt> handler.
		 *
		 * <p>Note that, because the requests run asynchronously, after calling this function your request might still succeed or fail with
		 * a reason other than <tt>Cancelled</tt>: the operation might have actually completed by the time you called <tt>end()</tt>.</p>
		 *
		 * @method
		 * @name mzero.create.locker.StoreRequest#end
		 */
		 
		/**
		 * Allow the user to store item(s) in a designated locker
		 *
		 * @param {mzero.create.locker.StoreParams} params - Parameters
		 *
		 * @return {mzero.create.locker.StoreRequest} An asynchronous request, ready to be started
		 */
		store: function(params) {
			var lockerId = params.Locker;
			var apiReport = params.APIReport;
			var request = service.createRequest("Store", { Locker: lockerId, "ReportData": MAPEnv._buildReportData(apiReport) } )
				.withSignal("end", function() {
					request._sendSignal(["CancelStore", { Locker : lockerId }]);
				}, 0)
				.firingEvents({ "StoringIn" : "Storing" })
				.succeedsOn("StoreSucceeded")
			;
			request.Locker = lockerId;
			return request;
		},

		/**
		 * Parameters to a retrieval operation
		 * @name mzero.create.locker.RetrieveParams
		 * @class
		 * @property {int} Locker - Id of the locker whose contents are to be retrieved
		 * @property {mzero.create.APIReport} APIReport	- Optional: application data to include in the API report; if not specified, default data will be provided
		 * @see mzero.create.locker.retrieve
		 */
		
		/**
		 * Represents a Request to retrieve item(s) from a locker
		 * @name mzero.create.locker.RetrieveRequest
		 * @class
		 * @extends mzero.create.Request
		 * @property {int} Locker - Id of the locker whose contents will be retrieved during this request
		 * @fires Retrieving
		 * @see mzero.create.locker.retrieve
		 */
		
		/**
		 * Start a retrieval request, providing the callback handlers to be used.
		 *
		 * @param eventHandlers {Object} - Event handlers
		 * @param eventHandlers.Retrieving {mzero.create.EventCallback} - Invoked when the locker is ready for retrieval
		 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. If successful, the <code>err</code> parameter will be null; otherwise it will contain a string explaining the reason (such as <code>Timeout</code>).
		 * @method
		 * @name mzero.create.locker.RetrieveRequest#start
		 */
		
		/**
		 * Retrieve item(s) from a locker
		 *
		 * @param {mzero.create.locker.RetrieveParams} params - Parameters
		 *
		 * @return {mzero.create.locker.RetrieveRequest} An asynchronous request, ready to be started
		 */
		retrieve: function(params) {
			var lockerId = params.Locker;
			var apiReport = params.APIReport;
			var request = service.createRequest("Retrieve", { Locker: lockerId, "ReportData": MAPEnv._buildReportData(apiReport) } )
				.firingEvents({ "RetrievingFrom" : "Retrieving" })
				.succeedsOn("RetrieveSucceeded")
			;
			request.Locker = lockerId;
			return request;
		},
	};
	return exports;
})();
