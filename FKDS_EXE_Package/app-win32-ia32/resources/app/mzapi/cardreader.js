/**
 * Interface to the CardReader service provided by the platform
 * @namespace cardreader
 * @memberof mzero.create
 */
mzero.create.cardreader = (function() {
	
	var service = mzero.create.MAPEnv.createService("CardReader");
	/** @lends mzero.create.cardreader */
	var exports = {
		/**
		 * Represents a Request to read a magnetic stripe card
		 * @name mzero.create.cardreader.ReadMagneticStripeRequest
		 * @class
		 * @extends mzero.create.Request
		 * @see mzero.create.cardreader.readMagneticStripe
		 * @see mzero.create.cardreader.ReadMagneticStripeResult
		 */
		
		/**
		 * Data obtained on successful completion of {@link mzero.create.cardreader.ReadMagneticStripeRequest}
		 *
		 * @class mzero.create.cardreader.ReadMagneticStripeResult
		 * @property {Object} Info
		 * @property {String} Info.CardType
		 * @property {String} Info.PAN
		 * @property {String} Info.CVV
		 * @property {Object} Info.CardExpiry
		 * @property {String} Info.CardExpiry.Month	Two digit month number
		 * @property {String} Info.CardExpiry.Year	Two digit year
		 * @property {String} Info.LastName	Last name encoded in the card, if recognized
		 * @property {String} Info.FirstName First name encoded in the card, if recognized
		 * @property {Array.<String>} Tracks Array containing the tracks found in the card
		 * @see mzero.create.cardreader.ReadMagneticStripeRequest
		 */
		 
		/**
		 * Start the card read request, providing the callback handlers to be used.
		 *
		 * @param eventHandlers {Object} - Event handlers
		 * @param eventHandlers.Done {mzero.create.EventCallback} - The request has finished. In case of error, the <tt>err</tt> value will indicate the reason (such as <tt>Timeout</tt> when the user does not swipe a card).
		 * @method
		 * @name mzero.create.cardreader.ReadMagneticStripeRequest#start
		 */
		
		/**
		 * Call <tt>end()</tt> to indicate that the request should finish as soon as possible, interrupting the operation of the device. This
		 * hurries the end of the request and, if successful, you will get a <tt>Cancelled</tt> error on the <tt>Done</tt> handler.
		 *
		 * <p>Note that, because the requests run asynchronously, after calling this function your request might still succeed or fail with
		 * a reason other than <tt>Cancelled</tt>: the operation might have actually completed by the time you called <tt>end()</tt>.</p>
		 *
		 * @method
		 * @name mzero.create.cardreader.ReadMagneticStripeRequest#end
		 */
		 
		/**
		 * Activates the card reader device until the user swipes a card or a timeout occurs.
		 *
		 * @return {mzero.create.cardreader.ReadMagneticStripeRequest} Request ready to be started
		 * @see mzero.create.cardreader.ReadMagneticStripeResult
		 */
		readMagneticStripe: function() {
			var self = this;
			var request = service.createRequest("ReadMagstripeCard")
				.withSignal("end", "CancelReadMagstripeCard", 0)
				.succeedsOn("ReadMagstripeCardSucceeded")
			;
			return request;
		}
	};
	return exports;
})();
