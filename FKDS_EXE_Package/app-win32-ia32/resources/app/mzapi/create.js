var mzero = (function() {
	var createjs = (function() {
		/**
		 * @namespace mzero.create
		 * @version 00.09.47
		 */
		var API_ID = "MZCREATEJS 00.09.47"; // please keep API_ID and @version synched until a release tool does it automatically!
		var componentsNS = {};

		var _nullLoggingPlugin = (function() {
			var _defaultNullLogger = {
				trace: function(m) {},
				debug: function(m) {},
				info: function(m) {},
				warn: function(m) {},
				error: function(m) {},
				fatal: function(m) {}
			};
			return {
				initialize: function(nullLogging, options) { },
				configureLogger: function(logger, options) { },
				getNullLogger: function() { return _defaultNullLogger; },
				getNamedLogger: function(name) { return _defaultNullLogger; }
			};
		})();
		var _loggingPlugin;
		var _useNullAPILogging;

		function _getAPILogger(name) {
			if(_useNullAPILogging) {
				return _loggingPlugin.getNullLogger();
			} else {
				return _loggingPlugin.getNamedLogger('API' + (name? '.' + name : ''));
			}
		}
		function _getClientLogger(name) {
			if(_useNullAPILogging) {
				return _loggingPlugin.getNullLogger();
			} else {
				return _loggingPlugin.getNamedLogger('API.Client' + (name? '.' + name : ''));
			}
		}


		/**
		 * Custom data provided by the application, meant to contain business logic data associated to a call on the APIs that support it.
		 *
		 * <p>The exact meaning of each property below is left to the application to decide: the descriptions stated below are only suggestions.</p>
		 *
		 * @name mzero.create.APIReport
		 * @class
		 * @property {string} CustomId1 - Optional string maybe identifying an entity related to the application itself; it could be a custom machine/terminal identifier, token, etc.
		 * @property {string} CustomRef1 - Optional string maybe referencing an entity external to the application itself. For example, on cash deposits, it could be the account number to which the deposit should be credited to.
		 * @property {string} Operation	- Optional string maybe representing the <em>larger</em> application state or context in which the API call is performed; for example, for a cash deposit, it could be used to indicate restrictions or conditions such as "Cash only customer", "Card/cash payment", etc.
		 * @property {string} State - Optional string maybe representing the <em>specific</em> application state or context during which the API call is performed; for example, for a cash deposit, it could be a payment concept such as "Monthly payment", "Annual renewal", etc.
		 */
		
		/**
		 * @name mzero.create.InitializationOptions
		 * @class
		 * @param {mzero.plugins.logging.LoggingOptions} Logging - Logging options: if <tt>null</tt>, the null logger will be used.
		 * @param {mzero.create.PlatformConnectionOptions} Connection - Parameters to use when connecting to to the platform
		 * @param {mzero.create.AppData} AppData - Optional: application data that the API will use when reporting to the management server
		 * @param {string[]} PropertyNames - Optional: the global property names which this application is interested on. When used, those properties will be available for reading in the {@link mzero.create.MAPEnvironment#GlobalProperties} dictionary once the connection to the platform has been established, where their values will be updated transparently to your application.
		 * @see mzero.create.MAPEnvironment#initialize
		 */
		
		/**
		 * @name mzero.create.PlatformConnectionOptions
		 * @class
		 * @param {String} Url - Location of the messaging broker where the platform will be searched for
		 * @param {String} Login - User id under which the connection should be attempted
		 * @param {String} Passcode - The password used to authenticate the login
		 * @see mzero.create.InitializationOptions
		 */
		
		/**
		 * Custom data related to the client application itself, provided by the application and used on the API calls that support it.
		 * @name mzero.create.AppData
		 * @class
		 * @property {string} SoftwareId	- Optional string which identifies the application version itself. For example, "GymMemberships 1.0.2"
		 * @property {string} CustomMachineId1	- Optional string which identifies the machine the software runs on under the application's business logic. Typically it's a "station number", "gas pump" number, "business unit" identification, etc.
		 * @see mzero.create.MAPEnvironment
		 * @see mzero.create.InitializationOptions
		 */
		
		/**
		 * The platform connection event handlers are callback functions which the API invokes to notify you about the state of the platform connection.
		 * @class mzero.create.PlatformConnectionEventHandlers
		 * @property {mzero.create.PlatformConnectionCallback} Done - The function to be invoked once the state of the initial connection attempt is known
		 * @property {mzero.create.PlatformDisconnectionCallback} Disconnected - This function will be invoked when the platform becomes unresponsive after an initial successful connection (that is, the <code>Done</code> callback was invoked successfully previously).
		 * @see mzero.create.MAPEnvironment#initialize
		 */
		
		/**
		 * A callback function of this kind will be invoked once when the platform is known to be connected.
		 * @callback mzero.create.PlatformConnectionCallback
		 * @param {String} err - When the connection fails, its value indicates the kind of error that caused it. In case of success, it is <tt>null</tt>.
		 * @param {mzero.create.PlatformData} data - Current platform data when the connection succeeded.
		 * @see mzero.create.PlatformConnectionEventHandlers
		 * @see mzero.create.MAPEnvironment#initialize
		 */
		/**
		 * A callback function of this kind will be invoked once when a connection to the platform was established successfully but fails afterwards.
		 * In these cases, all currently running requests will fail and new ones 
		 * @callback mzero.create.PlatformDisconnectionCallback
		 * @param {String} err - When the connection fails, its value indicates the kind of error that caused it. In case of success, it is <tt>null</tt>.
		 * @param {Object} data - Not currently in use
		 * @see mzero.create.PlatformConnectionEventHandlers
		 * @see mzero.create.MAPEnvironment#initialize
		 */


		/**
		 * Contains information corresponding to the platform and supporting APIs.
		 * @class mzero.create.PlatformData
		 * @property {string} MachineId - Identifier of the device running this application, as configured in the platform. It will be available once the connection has been established.
		 * @property {string} ClientAPI - This API's version
		 * @see mzero.create.MAPEnvironment
		 * @see mzero.create.MAPEnvironment#platformData
		 */
		function PlatformData(data) {
			this.MachineId = data? data.KioskId : null;
			this.ClientAPI = API_ID;
		}
		 
		/**
		 * MAPEnvironment manages the connection to the platform and is used to initialize your application.
		 * There is a single instance of this class, defined in the {@link mzero.create} namespace.
		 *
		 * @class mzero.create.MAPEnvironment
		 * @property {mzero.create.AppData} AppData - Data provided by your application when it calls {@link mzero.create.MAPEnvironment#initialize}. If the property is updated afterwards, the new values will be used on the next opportunity.
		 * @property {mzero.create.PlatformData} PlatformData - Data provided by the platform when the connection is established, which identifies the machine your application is running on
		 * @property {Object.<string,string>} GlobalProperties - Global properties which this application uses (available and updated transparently once the platform connection has been established). To use this feature, you need to specify the names of the properties you are interested on when the platform is initialized (see the initialization options in {@link mzero.create.MAPEnvironment#initialize}).
		 */
		function MAPEnvironment() {
			this._mapLink = new MAPLink(this);
			this._initialized = false;
			this._platformData = new PlatformData();
			var myself = this;
			Object.defineProperty(this, 'PlatformData', { configurable: false, get: function() { return myself._platformData; } } );
		}
		/**
		 * Returns true if the software is currently connected to the platform
		 * @return {boolean}
		 * @method mzero.create.MAPEnvironment#connected
		 */
		MAPEnvironment.prototype.connected = function() {
			return this._mapLink.connected();
		}
		/**
		 * @return {mzero.create.PlatformData} Platform data currently available
		 * @method mzero.create.MAPEnvironment#platformData
		 */
		MAPEnvironment.prototype.platformData = function() {
			return this._platformData;
		}
		/**
		 * Initialize the connection to the platform and call the specified callback function once as soon as the
		 * platform is known to be ready (which means that it could be called right away when the connection is
		 * already established).
		 *
		 * @param {mzero.create.InitializationOptions} options - Initialization options
		 * @param {mzero.create.PlatformConnectionEventHandlers} eventHandlers - Event handlers for the connection attempt(s)
		 * @name mzero.create.MAPEnvironment#initialize
		 * @method
		 */
		MAPEnvironment.prototype.initialize = function(options, eventHandlers) {
			var doneCallback = eventHandlers.Done;
			var disconnectedCallback = eventHandlers.Disconnected;
			if(this._mapLink.connected()) {
				this._disconnectedCallback = disconnectedCallback;
				return doneCallback(null, this._platformData);
			}
			if(!options) {
				throw new MAPException("No options specified for initialization");
			}
			var myself = this;
			if(options.PropertyNames && options.PropertyNames.length) {
				doneCallback = function(err, data) {
					if(!err) {
						myself._receivePropertiesAfterConnection(err, data, options.PropertyNames, eventHandlers.Done);
					} else {
						eventHandlers.Done && eventHandlers.Done(err, data);
					}
				}
			}
			this._setupLogging(options.Logging)
			this.logger.info("Initializing MAPEnvironment " + API_ID);
			this.AppData = options.AppData || { };
			this._updateSoftwareIdIfNeeded();
			this._disconnectedCallback = disconnectedCallback;
			var result = this._mapLink.connect(options.Connection, doneCallback);
			this._initialized = true;
			return result;
		};
		MAPEnvironment.prototype._setupLogging = function(options) {
			_loggingPlugin = (mzero && mzero.plugins && mzero.plugins.logging)? mzero.plugins.logging : _nullLoggingPlugin;
			_useNullAPILogging = options? false : true;
			_loggingPlugin.initialize(_useNullAPILogging, options);
			var logger = _getAPILogger(null);
			_loggingPlugin.configureLogger(logger, options);
			this.logger = logger;
		};
		/**
		 * Get a logger object for API clients; the type object depends on the logging plugin selected.
		 * <p>Make sure to call this function <em>after</em> the API has been initialized by <code>MAPEnvironment.initialize()</code>.</p>
		 * @param {string} name - Optional: a possibly hierarchical name to be used when creating a client logger.
		 * @name mzero.create.MAPEnvironment#getLogger
		 * @method
		 * @see mzero.create.MAPEnvironment#initialize
		 */
		MAPEnvironment.prototype.getLogger = function(name) {
			return _getClientLogger(name);
		};
		MAPEnvironment.prototype._updateSoftwareIdIfNeeded = function() {
			var t = this._getOrCreateSoftwareIdWhenNeeded();
			if(t == this._softwareIdBase) return;
			this._createNewStdSoftwareId(t);
		}
		MAPEnvironment.prototype._getOrCreateSoftwareIdWhenNeeded = function() {
			if(!this.AppData) this.AppData = {};
			var softwareId = this.AppData.SoftwareId;
			if(!softwareId) softwareId = this.AppData.SoftwareId = window.location.toString();
			return softwareId;
		};
		MAPEnvironment.prototype._createNewStdSoftwareId = function(baseSoftwareId) {
			this._softwareIdBase = baseSoftwareId;
			var fullSoftwareId = baseSoftwareId + ":" + API_ID;
			var max = 50;
			if(baseSoftwareId.length <= max) {
				this._stdSoftwareId = baseSoftwareId;
			} else {
				var prefix = "...";
				this._stdSoftwareId = prefix + baseSoftwareId.substring(baseSoftwareId.length - (max - prefix.length));
			}
		};
		/**
		 * Indicates if initialization has started or not
		 * @method mzero.create.MAPEnvironment#isInitialized
		 * @return {boolean}
		 * @see mzero.create.MAPEnvironment#initialize
		 */
		MAPEnvironment.prototype.isInitialized = function() {
			return this._initialized;
		};
		/**
		 * @protected
		 */
		MAPEnvironment.prototype.createService = function(serviceName) {
			return this._mapLink._createService(serviceName);
		}
		MAPEnvironment.prototype._completePlatformConnection = function(data) {
			this._platformData = new PlatformData(data);
		}
		MAPEnvironment.prototype._updateSession = function(sid) {
			this._currentSessionId = sid;
		}
		MAPEnvironment.prototype._buildReportData = function(apiReportData) {
			var result = {};
			result.CustomId1 = apiReportData? apiReportData.CustomId1 : null;
			this._updateSoftwareIdIfNeeded();
			result.SoftwareId = this._stdSoftwareId;
			result.SessionId = this._currentSessionId;
			result.CustomRef1 = apiReportData? apiReportData.CustomRef1 : null;
			result.ReferenceId = null; // forced by deprecation
			result.Operation = apiReportData? apiReportData.Operation : null;
			result.State = apiReportData? apiReportData.State : null;
			return result;
		}
		MAPEnvironment.prototype._receivePropertiesAfterConnection = function(platformConnectionErr, platformConnectionData, properties, finalConnectionCallback) {
			var myself = this;
			var handlers = {
				Done : function(err, data) {
					if(err) {
						myself.logger.error("Global property reception failed");
					} else {
						myself.GlobalProperties = data.Properties;
					}
					finalConnectionCallback && finalConnectionCallback(platformConnectionErr, platformConnectionData);
				},
				PropertyChange : function(err, data) {
					var properties = myself.GlobalProperties;
					if(!properties) {
						properties = {};
						myself.GlobalProperties = properties;
					}
					properties[data.Name] = data.Value;
				},
				_PersistenceBroken: function(err, data) {
					// TODO
				}
			};
			mzero.create.report._receiveProperties(properties).start(handlers);
		}
		MAPEnvironment.prototype._notifyDisconnection = function(err, data) {
			this._disconnectedCallback && this._disconnectedCallback(err, data)
		}
		
		var theMAPEnv;

		// Based on https://gist.github.com/LeverOne/1308368
		// (I think a and b were declared as parameters only to make the code even shorter)
		var UUID = function(){var a, b;for(b=a='';a++<36;b+=a*51&52?(a^15?8^Math.random()*(a^20?16:4):4).toString(16):'-');return b};

		function TagCreator() {
			this.counter = 1;
		}
		TagCreator.prototype.nextTag = function() {
			var result = this.counter++;
			return result.toString();
		};

		var ignoreMapping = '';
		var _minimumHeartbeatWait = 1.25*60*1000;

		function PlatformHeartbeatEstimator(logger, heartbeatMissingNotifier) {
			this.logger = logger;
			this.heartbeatMissingNotifier = heartbeatMissingNotifier;
			var currentTime = new Date().value;
			this._lastHeartbeatTime = currentTime;
			this._estimatedMaxLength = 0;
		}
		PlatformHeartbeatEstimator.prototype.update = function() {
			var currentTime = new Date().value;
			var currentLength = currentTime - this._lastHeartbeatTime;
			if(currentLength > this._estimatedMaxLength) {
				this._estimatedMaxLength = currentLength;
			}
			var proposedWait = 1.5 * this._estimatedMaxLength;
			var heartbeatWait = (proposedWait > _minimumHeartbeatWait)? proposedWait : _minimumHeartbeatWait;
			if(this._timerId) {
				window.clearTimeout(this._timerId);
			}
			this.logger.debug('Next heartbeat wait ' + heartbeatWait);
			var myself = this;
			this._timerId = window.setTimeout(function() {
				myself.heartbeatMissingNotifier(heartbeatWait);
			}, heartbeatWait);
		}
		/**
		 * @class
		 */
		function MAPLink(mapEnv) {
			this._mapEnv = mapEnv;
			this._uniquishId = UUID();
			this._clientId = "jsc." + this._uniquishId;
			this._localQueue = "/queue/" + this._clientId;
			this._stompClient = null;
			this._initialized = false;
			this._mapConnected = false;
			this._tagCreator = new TagCreator();
			this._requestRegistry = new RequestRegistry(this);
			this._servicesByName = {};
			this._connectionCallbacks = [];
			this._connectionState = 'NotConnected';
		}
		
		MAPLink.prototype.logger = null;
		
		MAPLink.prototype.initialized = function() { return this._initialized; }
		MAPLink.prototype.connected = function() { return this._mapConnected; }
		MAPLink.prototype.connect = function(options, callback) {
			if(this._connectionState != "NotConnected") {
				callback("ReconnectionNotSupported", null);
				return;
			}
			this.logger = _getAPILogger('Link');
			this._requestRegistry.initialize();
			this._connectionCallbacks.push(callback);
			if(this._connectionCallbacks.length != 1) {
				this.logger.info("Already waiting for connection outcome");
				return;
			}
			this.logger.info("Initiating stomp connection");
			this._stompClient = Stomp.client(options.Url);
			this._stompClient.heartbeat = { outgoing: 0,  incoming: 0 };
			var headers = {
				login: options.Login,
				passcode: options.Passcode,
				"client-id": "ID:stompLayer",
				"accept-version": "1.0,1.1"
			};
			this._connectionState = "BrokerConnection";
			var myself = this;
			this._stompClient.connect(headers,
				function() { myself._onStompConnected(); },
				function() { myself._onStompError(); }
			);
			this._initialized = true;
		};
		MAPLink.prototype._notifyConnectionCallbacks = function(err, data) {
			var connected = (err == null);
			var myself = this;
			this.logger.info("Notifying connection callbacks: connected=" + connected + " state=" + this._connectionState + " err=" + err);
			this._connectionCallbacks.forEach(function(callback, index, array) {
				callback(err, data);
			});
			this._connectionCallbacks = [];
		};
		MAPLink.prototype._onStompConnected = function() {
			var myself = this;
			this.logger.debug("stomp connected");
			var subscriptionHeaders = { id: "sub-" + this._uniquishId };
			this.localQueueSubscription = this._stompClient.subscribe(
				this._localQueue,
				function(message) {
					myself._onMAPFrameReceived(message);
				},
				subscriptionHeaders
			);
			var connectToMAPRequest = JSON.stringify(
				{
					"Service" : "Map",
					"Command": "Connect",
					"Data" : { "ClientId": this._clientId }
				}
			);
			this.logger.info("Platform connection request=" + connectToMAPRequest + " len=" + connectToMAPRequest.length);
			this._stompClient.send(
				"/queue/map", 
				{
					"reply-to": this._localQueue,
					"timestamp" : (new Date()).getTime(),
					"priority" : 5,
					"NMSXDeliveryMode" : true,
					"persistent" : true
				},
				connectToMAPRequest
			);
			this._connectionState = "QueueSetup";
			var MaxPlatformConnectionWait = 90*1000;
			var myself = this;
			this._platformConnectionTimer = window.setTimeout(function() {
				myself._abandonPlatformConnection();
			}, MaxPlatformConnectionWait);
		};
		MAPLink.prototype._abandonPlatformConnection = function() {
			this._connectionAttemptAbandoned = true;
			this._notifyConnectionCallbacks("PlatformSynchError", null);
		};
		MAPLink.prototype._onStompError = function() {
			if(this._connectionState == "PlatformConnected") {
				this.logger.error("The platform connection has been lost");
				// Heartbeat monitoring can detect this condition; do we need to be more specific?
				// TODO: confirm the conditions upon which the error happened; that determines what we should do here
				// TODO?: this._connectionState = "Disconnected";
				// TODO?: this._disconnectionReason = "PlatformConnectionLost";
				// TODO?: this._notifyDisconnection("PlatformConnectionLost");
			} else if(this._connectionState == "QueueSetup") {
				// Can happen if the broker shuts down once we're connected to it but before the platform responds
				this._notifyConnectionCallbacks("PlatformSynchError", null);
			} else {
				this._notifyConnectionCallbacks("BrokerConnectionError", null);
			}
		};
		MAPLink.prototype._onMAPFrameReceived = function(frame) {
			if(this._connectionAttemptAbandoned) {
				this.logger.info("Frame will be ignored because we have abandoned this connection");
				return;
			}
			var frameBodyObject;
			try {
				frameBodyObject = JSON.parse(frame.body);
				// this.logger.debug("Parsed " + frame.body);
				// this.logger.debug("Result= " + frameBodyObject);
			} catch(e) {
				this.logger.error("Error when parsing received frame: " + e + " on " + frame);
				return;
			}
			var serviceName = frameBodyObject["Service"];
			if(serviceName == "Map") {
				this._handleMAPFrame(frame, frameBodyObject);
			} else {
				this._dispatch(serviceName, frameBodyObject);
			}
		};
		MAPLink.prototype._handleMAPFrame = function(frame, frameBodyObject) {
			var mapCommand = frameBodyObject.Command;
			var mapResponse = frameBodyObject.Response;
			
			if(mapCommand == "Connect") {
				if(mapResponse == "Connected") {
					this._initMAPSession(frame, frameBodyObject);
				} else {
					this.logger.debug("Unknown MAP Connect response: " + mapResponse);
				}
			} else if(mapCommand == "Heartbeat") {
				//this.logger.debug("Replying to MAP heartbeat");
				this._heartbeatEstimator.update();
				this._sendToMAPSession("Map", mapCommand, {}, "HeartbeatResponse");
			} else if(mapCommand == "ServiceCommand" && mapResponse == "ServiceNotAvailable") {
				// The tag is invalid in this case
				this._failRequestWhenServiceNotAvailable(frameBodyObject["Data"]);
			} else {
				this.logger.debug("Unknown MAP command: " + mapCommand);
			}
		};
		MAPLink.prototype._initMAPSession = function(initialFrame, initialFrameObject) {
			window.clearTimeout(this._platformConnectionTimer);
			this._platformConnectionTimer = undefined;
			this._mapSessionQueue = initialFrame.headers["reply-to"];
			var data = initialFrameObject.Data;
			this.logger.info("MAP session queue=" + this._mapSessionQueue + ", kioskId=" + data.KioskId + ", " + data.SessionId);
			this._mapConnected = true;
			this._mapEnv._completePlatformConnection(data);
			this._connectionState = "PlatformConnected";
			this._disconnectionReason = undefined;
			var myself = this;
			this._heartbeatEstimator = new PlatformHeartbeatEstimator(this.logger, function(expiredWait) {
				myself._onPlatformHeartbeatMissed(expiredWait);
			});
			this._notifyConnectionCallbacks(null, this._mapEnv.platformData());
		};
		MAPLink.prototype._onPlatformHeartbeatMissed = function(elapsedInterval) {
			this.logger.error("Platform heartbeat disappeared");
			this._disconnectionReason = 'PlatformUnresponsive';
			var pendingRequests = this._requestRegistry._unregisterAll();
			pendingRequests.forEach(function(request, index, array) {
				request._firePlatformUnresponsive();
			});
			this._notifyDisconnection('PlatformUnresponsive', null);
		};
		MAPLink.prototype._sendToMAPSession = function(serviceName, command, msgData, mapResponse) {
			if(!msgData) {
				msgData = {};
			}
			var tag = msgData.Tag;
			if(this._disconnectionReason) {
				this._scheduleFailureResponse(tag, serviceName, command, this._disconnectionReason);
				return;
			}
			var mapMsg = {
				"Service" : serviceName,
				"Command": command,
				"Data" : msgData
			};
			if(mapResponse) {
				mapMsg["Response"] = mapResponse;
			}
			var mapMsgString = JSON.stringify(mapMsg);
			this._stompClient.send(
				this._mapSessionQueue, 
				{
					"reply-to": this._localQueue,
					"timestamp" : (new Date()).getTime(),
					"priority" : 5,
					"NMSXDeliveryMode" : true,
					"persistent" : true
				},
				mapMsgString
			);
			if(tag) {
				this.logger.debug("Sent: " + serviceName + "." + command + " tag=" + tag);
			} else {
				this.logger.debug("Sent: " + serviceName + "." + command);
			}
		};
		MAPLink.prototype._scheduleFailureResponse = function(tag, serviceName, command, response) {
			var myself = this;
			window.setTimeout(function() {
				myself._completeFailureResponse(tag, serviceName, command, response);
			}, 0);
		};
		MAPLink.prototype._completeFailureResponse = function(tag, serviceName, command, response) {
			var request = tag? this._requestRegistry._findByTag(tag) : this._findByServiceCommand(service, command);
			if(!request) {
				this.logger.warn("Could not schedule response " + _response + " because no request was found: tag=" + tag + " service=" + serviceName + " command=" + command);
				return;
			}
			request._firePlatformUnresponsive();
		};
		MAPLink.prototype._failRequestWhenServiceNotAvailable = function(msgData) {
			if(!msgData) {
				msgData = {};
			}
			var service = msgData["AttemptedService"];
			var command = msgData["AttemptedCommand"];
			var response = msgData["AttemptedResponse"];
			var request = this._requestRegistry._findByServiceCommand(service, command);
			if(!request) {
				this.logger.debug("No request found for attempted service.command: " + service + "." + command);
				return;
			}
			request._fireServiceNotAvailable();
		}
		MAPLink.prototype._createService = function(serviceName) {
			if(this._servicesByName.hasOwnProperty(serviceName)) {
				throw new MAPException("Service already exists: " + serviceName);
			}
			var service = new Service(this, serviceName);
			this._servicesByName[serviceName] = service;
			return service;
		};
		MAPLink.prototype._dispatch= function(serviceName, msgBody) {
			var evt = msgBody["Event"];
			var data = msgBody["Data"];
			var tag = data["Tag"];
			if(!tag) {
				this.logger.debug("Received untagged service frame: " + JSON.stringify(msgBody));
				return;
			}
			var request = this._requestRegistry._findByTag(tag);
			if(!request) {
				this.logger.debug("Received service frame with unknown tag=" + tag);
				return;
			}
			if(serviceName != request.service.name) { // Sanity check
				this.logger.warn("Tagged service frame does not match context for tag=" + tag);
				return;
			}
			if(evt != null) {
				this.logger.info("Dispatching event=" + evt + " tag=" + tag);
				request._dispatchEvent(evt, data);
			} else {
				var response = msgBody["Response"];
				var mappedEvent = (response && request._responseEvents)? request._responseEvents[response] : null;
				if(mappedEvent) {
					if(mappedEvent == ignoreMapping) {
						this.logger.info("Ignoring response=" + response + " tag=" + tag);
						return;
					}
					this.logger.info("Dispatching response=" + response + " as event=" + mappedEvent + " tag=" + tag);
					request._dispatchMappedEvent(response, mappedEvent, data);
				} else {
					this.logger.info("Dispatching response=" + response + " tag=" + tag);
					request._dispatchResponse(response, data);
				}
			}
		};
		MAPLink.prototype._createRequest = function(service, requestName, opt_contents, behavior) {
			var tag = this._tagCreator.nextTag();
			var result = new Request(service, requestName, tag, opt_contents, behavior);
			this._requestRegistry.register(result);
			return result;
		};
		MAPLink.prototype._releaseRequest = function(request) {
			this._requestRegistry.unregister(request);
			this.logger.debug("Released request tag=" + request.tag);
		};
		MAPLink.prototype._notifyDisconnection = function(err, data) {
			this._mapEnv._notifyDisconnection(err, data);
		};

		/**
		 * An MZero Create service represents a platform service
		 * @class mzero.create.Service
		 */
		function Service(mapLink, name) {
			this._mapLink = mapLink;
			this.name = name;
			this._requests = {};
		}
		/**
		 * Creates a request associated to this service
		 * @param {string} requestName - Platform name
		 * @param {object} opt_contents	- Request parameters
		 * @param {object} behavior - Platform behavior settings
		 * @protected
		 */
		Service.prototype.createRequest = function(requestName, opt_params, behavior) {
			var result = this._mapLink._createRequest(this, requestName, opt_params, behavior);
			this._requests[result.tag] = result;
			return result;
		};
		Service.prototype._sendRequest = function(request) {
			return this._mapLink._sendToMAPSession(this.name, request.name, request.contents);
		};
		Service.prototype._sendRequestSignal = function(signal) {
			this._mapLink._sendToMAPSession(this.name, signal.name, signal.contents);
		};
		Service.prototype._releaseRequest = function(request) {
			this._mapLink._releaseRequest(request);
			delete this._requests[request.tag];
		};

		/**
		 * A function to be invoked in response to an MZero Create event.
		 *
		 * <p>The execution of a <code>Request</code> can generate different events identified by strings
		 * (such as "Progress", "Done", etc.), and for each one the API will look for a callback function to invoke.</p>
		 *
		 * <p>You specify the callback functions for each event you are interested on when the request is started.</p>
		 *
		 * @public
		 * @callback mzero.create.EventCallback
		 * @param {String} err - In case of the <tt>Done</tt> event, <tt>err</tt> is null in case of success; otherwise, it indicates which error caused the request to fail.
		 * @param {Object} data - Contains any data object provided by the event
		 * @example <caption>Defining event callback functions and starting a request</caption>
		 * var eventHandlers = {
		 *     CurrentCuffPressure: function(err, data) {
		 *         currentDemoController.showData("cuff pressure", data);
		 *     },
		 *     Done: function(err, data) {
		 *         if(err) {
		 *             // The request failed, and err describes the reason
		 *         } else {
		 *             // The request finished successfully and data contains the result (if any)
		 *         }
		 *     }
		 * };
		 * // Create and start a measurement request on the blood pressure service:
		 * var request = mzero.create.bloodpressure.measure(weight).start(eventHandlers);
		 *
		 */
		
		/**
		 * A request is an operation performed asynchronously by the API through the platform services.
		 *
		 * <p>
		 * Rather than creating these objects yourself, you obtain them by using a service function and initiate them
		 * by calling their <tt>start()</tt> method, providing callback functions for the event names you are interested on.
		 * </p>
		 *
		 * @class mzero.create.Request
		 * @fires Done	All Request objects fire the <tt>Done</tt> event when their execution completes.
		 * @see mzero.create.EventCallback
		 *
		 * @property {Boolean} Started - true when the request has been started
		 * @property {Object} Result - Once the request has finished, contains its final result (if any). The actual contents of the result object depend on the type of request.
		 * @property {String} Error - If the request has finished in error, contains the error that terminated it
		 * @see mzero.create.Request#start
		 */
		function Request(service, name, tag, opt_contents, behavior) {
			this.logger = _getAPILogger('Request');
			this.service = service;
			this.name = name;
			this.tag = tag;
			this.contents = this._copy1(opt_contents);
			this.contents["Tag"] = tag.toString();
			this.Started = false;
			this.Done = false;
			this.Error = null;
			this.result = null;
			this._configureBehaviors(behavior);
		}
		Request.prototype._configureBehaviors = function(behavior) {
			this._tagPersists = behavior && behavior["TagPersists"];
		};
		/**
		 * Initiates a request, specifying which functions should handle the events fired during execution.
		 * <p>Each kind of request fires different event names, but they all at least generate the <tt>Done</tt> event.</p>
		 *
		 * @param {Object} eventHandlers - An object that maps event names to the corresponding callback functions you want to use.
		 * @param {mzero.create.EventCallback} eventHandlers.Done - Typically you would at least define a handler for the <tt>Done</tt> event (since you usually want
		 *                                                          to know when the request finished and what its result was).
		 * @public
		 * @name start
		 * @method
		 * @memberof mzero.create.Request
		 * @instance
		 */
		Request.prototype.start = function(eventHandlers) {
			this._eventCounters = {};
			this._eventHandlers = eventHandlers;
			this.service._sendRequest(this);
			this.Started = true;
			return this;
		};
		/**
		 * Indicates if the request has received the specified event
		 */
		Request.prototype.gotEvent = function(eventName) {
			return (this._eventCounters[eventName] || 0) != 0;
		};
		Request.prototype._reservedNames = {"Tag":true, "CustomReportData":true};
		Request.prototype._copy1 = function(obj) {
			var result = {};
			if(obj) {
				for(var i in obj) {
					if(this._reservedNames[i]) {
						throw new Error("Using reserved property name: ");
					}
					result[i] = obj[i];
				}
			}
			return result;
		};
		Request.prototype._definitionSanityCheck = function() {
			if(this.Started) {
				throw new Error("Cannot define things on a started request");
			}
			if(this.Done) {
				throw new Error("Cannot define things on a completed request");
			}
		};
		/**
		 * Specifies the status code which indicates that the request completed successfully
		 * @protected
		 */
		Request.prototype.succeedsOn = function(responseName) {
			this._definitionSanityCheck();
			this._successfulResponse = responseName;
			return this;
		};
		Request.prototype.convertingErrorCodes = function(errorCodeMapping) {
			this._errorCodeMapping = errorCodeMapping;
			return this;
		};
		/**
		 * Specifies the events which are fired while the request is active
		 * @protected
		 * @param {Object} eventMapping - A mapping to translate platform event codes to the MZero Create event name which will be sent to the request
		 */
		Request.prototype.firingEvents = function(eventMapping) {
			this._definitionSanityCheck();
			this._events = eventMapping;
			return this;
		}
		/**
		 * Some of the responses generated by the platform are managed better as events. This function
		 * lets you specify which ones
		 */
		Request.prototype.convertingResponsesToEvents = function(responseMapping) {
			this._definitionSanityCheck();
			this._responseEvents = responseMapping;
			return this;
		}
		/**
		 * Specifies custom information to be included when the request is reported to the management server
		 * @todo refine functionality
		 * @protected
		 */
		Request.prototype.withReportData = function(data) { // TODO: refine
			this._definitionSanityCheck();
			this.contents["CustomReportData"] = data;
			return this;
		};
		/**
		 * Specifies a signal that can be sent during the request
		 * @param {String} signalFunctionName - Name under which the signal will be available to the user
		 * @param {(String|Function)} signalSpecification - If a string, this represents the name of the platform signal to send and the platform will create the appropriate function.
		 * 	                                                If a function, you can specify its actions yourself
		 * @param {Number}[0] num_opt_args - So far, it is an optional argument that helps to understand visually how to call the signal function
		 * @protected
		 */
		Request.prototype.withSignal = function(signalFunctionName, signalSpecification, num_opt_args) {
			this._definitionSanityCheck();
			if(this._reservedNames.hasOwnProperty(signalFunctionName)) {
				throw new Error("Signal cannot have reserved name: " + signalFunctionName);
			}
			if(!signalSpecification) {
				throw new Error("No specification provided for signal: " + signalFunctionName);
			}
			var myself = this;
			if(num_opt_args === undefined) { // So far, it's an optional argument that helps to understand visually how to call the signal function
				num_opt_args = 0;
			}
			var specificationType = typeof(signalSpecification);
			if(specificationType == "string") {
				this[signalFunctionName] = function() {
					var args = Array.prototype.slice.call(arguments);
					args.unshift(signalSpecification);
					myself._sendSignal(args);
				};
			} else if(specificationType == "function") {
				this[signalFunctionName] = signalSpecification;
			} else {
				throw new Error("Cannot understand specification type '" + specificationType + "' when creating signal: " + signalFunctionName);
			}
			return this;
		};
		/**
		 * Specify functions to transform certain event data objects into different forms
		 */
		Request.prototype.transformEventDataUsing = function(transforms) {
			this._definitionSanityCheck();
			this._eventDataTransforms = transforms;
			return this;
		};
		/**
		 * Specifies a result transformation to be executed on the data received from the platform
		 * @protected
		 */
		Request.prototype.transformResultUsing = function(func) {
			this._definitionSanityCheck();
			this._transformResult = func;
			return this;
		}
		Request.prototype._ignoringResponses = function(responses) { // TODO: to be used for platform calls that send responses when they should be signals
			var responseMapSet = undefined;
			if(responses) {
				responseMapSet = {};
				responses.forEach(function(elem, index, array) {
					responseMapSet[elem] = true;
				});
			}
			this._responsesToIgnore = responseMapSet;
			return this;
		};
		Request.prototype._dispatchResponse = function(responseName, data) {
			var success = (this._successfulResponse == responseName);
			var error = data["Error"];
			if(success) {
				if(this._transformResult) {
					data = this._transformResult(data);
				}
				this._fireComplete(null, data);
			} else if(this._shouldIgnoreResponse(responseName)) {
				this.logger.debug("Ignoring by specification: " + responseName);
			} else {
				if(error == null) {
					this.logger.warn("Response has no error reason");
					error = "UnknownError";
				} else {
					error = (this._errorCodeMapping && this._errorCodeMapping[error]) || error;
				}
				this._fireComplete(error, data);
			}
		};
		Request.prototype._dispatchEvent = function(platformEventName, data) {
			if(!this._events) {
				this.logger.warn("Event received but the request has no definitions: " + platformEventName);
				return;
			}
			var mappedEventName = this._events[platformEventName];
			if(!mappedEventName) {
				this.logger.warn("Event received but there are no mappings for it: " + platformEventName);
				return;
			}
			this._dispatchMappedEvent(platformEventName, mappedEventName, data);
		};
		Request.prototype._dispatchMappedEvent = function(platformEventName, apiEventName, data) {
			var eventCounter = this._eventCounters[apiEventName] || 0;
			this._eventCounters[apiEventName] = ++eventCounter;
			if(!this._eventHandlers) {
				this.logger.warn("Event received but no handlers are defined");
				return;
			}
			var dataTransformFunction = this._eventDataTransforms && this._eventDataTransforms[apiEventName];
			if(dataTransformFunction) {
				data = dataTransformFunction(data);
			}
			var callback = this._eventHandlers[apiEventName];
			if(!callback) {
				this.logger.warn("Platform event received but it has no handler: " + platformEventName);
				return;
			}
			try {
				callback(null, data);
			} catch(e) {
				this.logger.warn("Caught exception while invoking event callback; platform event=" + platformEventName + "; exception=" + e);
			}
		};
		Request.prototype._fireComplete = function(err, data) {
			// "Persistent" requests now receive two "completion" notifications: Done and _PersistenceBroken
			var handlerName;
			if(this.Done && this._tagPersisted) {
				handlerName = '_PersistenceBroken';
				this._PersistenceBroken = true;
			} else {
				handlerName = 'Done';
			}
			var callback = this._eventHandlers[handlerName];
			this.Done = true;
			this.Error = err;
			this.Result = data;
			if(!this._eventHandlers) {
				this.logger.warn("Request is done but no one is interested");
				return;
			}
			if(!callback) {
				this.logger.trace("Request has no '" + handlerName + "' handler defined");
			} else {
				try {
					callback(err, data);
				} catch(e) {
					this.logger.warn("Caught exception while invoking completion callback; err=" + err + "; exception=" + e);
				}
			}
			if(this._tagPersists) {
				if(!err) {
					this._tagPersisted = true;
					this._PersistenceBroken = false;
					return;
				}
				this.logger.info("Tag " + this.tag + " would have persisted if the request had not failed");
			}
			this.service._releaseRequest(this);
		};
		Request.prototype._sendSignal = function(signalArgs) {
			var signal = new Signal(this, signalArgs); // TODO: create a mechanism to receive the names/values of any parameters needed by this signal
			this.service._sendRequestSignal(signal);
		};
		Request.prototype._shouldIgnoreResponse = function(response) {
			if(!response || !this._responsesToIgnore) return false;
			return this._responsesToIgnore[response];
		}
		Request.prototype._fireServiceNotAvailable = function() {
			this._fireComplete("ServiceNotAvailable", null);
		};
		Request.prototype._firePlatformUnresponsive = function() {
			this._fireComplete("PlatformUnresponsive", null);
		}

		/**
		 * A Signal is an operation on the platform which does not have a direct response.
		 * @protected
		 * @class
		 */
		function Signal(request, signalArgs)
		{
			this.name = signalArgs.shift();
			var params = signalArgs[0]? signalArgs[0] : { };
			params["Tag"] = request.tag;
			this.contents = params;
		}

		/**
		 * @private
		 */
		function MAPException(msg) {
			this.message = msg;
			this.toString = function() {
				return this.message;
			}
		}

		function RequestRegistry(mapLink) {
			this._mapLink = mapLink;
			this._requestsByTag = {};
		}
		RequestRegistry.prototype.initialize = function() {
			this.logger = _getAPILogger('ReqReg');
		}
		RequestRegistry.prototype.register = function(request) {
			if(!this._mapLink.initialized()) {
				throw new MAPException("PlatformNotInitialized");
			}
			if(!this._mapLink.connected()) {
				throw new MAPException("PlatformNotYetAvailable");
			}
			var tag = request.tag;
			if(!tag) {
				throw new MAPException("NonTaggedRequest");
			}
			if(this._requestsByTag.hasOwnProperty(tag)) {
				throw new MAPException("TagAlreadyAssigned");
			}
			this._requestsByTag[tag] = request;
		};
		RequestRegistry.prototype.unregister = function(request) {
			var tag = request.tag;
			return this._unregisterByTag(tag);
		};
		RequestRegistry.prototype._unregisterByTag = function(tag) {
			if(tag == null) {
				this.logger.warn("No tag");
			} else if(!this._requestsByTag.hasOwnProperty(tag)) {
				this.logger.warn("No need to unregister a non-registered request");
			} else {
				delete this._requestsByTag[tag];
			}
		};
		RequestRegistry.prototype._findByTag = function(tag) {
			var result = this._requestsByTag[tag];
			return result;
		};
		RequestRegistry.prototype._findByServiceCommand = function(service, command) {
			var result = null;
			for(var tag in this._requestsByTag) {
				var request = this._requestsByTag[tag];
				if((request.service.name == service) && (request.name == command)) {
					result = request;
					break;
				}
			}
			return result;
		};
		RequestRegistry.prototype._unregisterAll = function() {
			var result = [];
			for(var tag in this._requestsByTag) {
				result.push(this._requestsByTag[tag]);
			}
			this._requestsByTag = {};
			return result;
		};
		
		theMAPEnv = new MAPEnvironment();
		
		/**
		 * @lends mzero.create
		 */
		var exports = {
			API: API_ID,
			/**
			 * @memberof mzero.create
			 * @namespace
			 */
			components: componentsNS,
			/**
			 * @property {mzero.create.MAPEnvironment} MAPEnv Singleton used for platform initialization and general queries
			 */
			MAPEnv : theMAPEnv,
			MAPException : MAPException,
			_getAPILogger: function(name) {
				return _getAPILogger(name);
			},
			getLogger : function(name) {
				return theMAPEnv.getLogger(name);
			}
		};
		return exports;
	}());
	/**
	 * @namespace mzero
	 */
	var exports = {
		"create" : createjs
	};
	return exports;
}());
